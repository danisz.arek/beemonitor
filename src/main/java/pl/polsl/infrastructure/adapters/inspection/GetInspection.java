package pl.polsl.infrastructure.adapters.inspection;

import org.springframework.stereotype.Component;
import pl.polsl.domain.honeycollection.HoneyCollection;
import pl.polsl.domain.honeycollection.service.IGetHoneyCollection;
import pl.polsl.domain.inspection.Inspection;
import pl.polsl.domain.inspection.service.IGetInspection;
import pl.polsl.infrastructure.adapters.honeycollection.HoneyCollectionEntity;
import pl.polsl.infrastructure.adapters.honeycollection.HoneyCollectionRepository;
import pl.polsl.infrastructure.collection.IMapTo;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class GetInspection implements IGetInspection {

    private final InspectionRepository inspectionRepository;
    private final IMapTo<Inspection, InspectionEntity> iMapTo;

    public GetInspection(InspectionRepository inspectionRepository, IMapTo<Inspection, InspectionEntity> iMapTo) {
        this.inspectionRepository = inspectionRepository;
        this.iMapTo = iMapTo;
    }


    @Override
    public Inspection byId(Integer id) {
        InspectionEntity entity = this.inspectionRepository.findById(id).orElse(null);

        if (null == entity) {
            return null;
        }

        return iMapTo.toDto(entity);
    }

    @Override
    public List<Inspection> getAll() {
        Iterable<InspectionEntity> inspectionEntities = inspectionRepository.findAll();
        List<Inspection> output = new ArrayList<>();
        inspectionEntities.forEach(entity -> output.add(iMapTo.toDto(entity)));

        return output;
    }

    @Override
    public List<Inspection> findAllByBeehiveId(Integer id) {
        Iterable<InspectionEntity> inspectionEntities = inspectionRepository.findAllByBeehiveId(id);
        List<Inspection> output = new ArrayList<>();
        inspectionEntities.forEach(entity -> output.add(iMapTo.toDto(entity)));

        return output;
    }

    @Override
    public List<Inspection> getUnassigned() {
        Iterable<InspectionEntity> inspectionEntities = inspectionRepository.findAllByBeehiveIdIsNull();
        List<Inspection> output = new ArrayList<>();
        inspectionEntities.forEach(entity -> output.add(iMapTo.toDto(entity)));

        return output;
    }

    @Override
    public Inspection findByBeehiveId(Integer beehiveId) {

        Optional<InspectionEntity> userDetailsEntities = inspectionRepository.findByBeehiveId(beehiveId);
        return iMapTo.toDto(userDetailsEntities.orElse(null));
    }
}

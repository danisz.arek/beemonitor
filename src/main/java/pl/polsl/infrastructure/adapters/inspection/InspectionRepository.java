package pl.polsl.infrastructure.adapters.inspection;

import org.springframework.data.repository.CrudRepository;
import pl.polsl.infrastructure.adapters.honeycollection.HoneyCollectionEntity;

import java.util.List;
import java.util.Optional;

public interface InspectionRepository extends CrudRepository<InspectionEntity, Integer> {

    Optional<InspectionEntity> findById(Integer id);

    List<InspectionEntity> findAllByBeehiveId(Integer userId);

    List<InspectionEntity> findAllByBeehiveIdIsNull();

    Optional<InspectionEntity> findByBeehiveId(Integer id);
}

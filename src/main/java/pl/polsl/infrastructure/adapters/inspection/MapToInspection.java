package pl.polsl.infrastructure.adapters.inspection;

import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pl.polsl.domain.honeycollection.HoneyCollection;
import pl.polsl.domain.inspection.Inspection;
import pl.polsl.infrastructure.adapters.honeycollection.HoneyCollectionEntity;
import pl.polsl.infrastructure.collection.IMapTo;

@Component
public class MapToInspection implements IMapTo<Inspection, InspectionEntity> {

    public InspectionEntity toEntity(Inspection dto, @Nullable InspectionEntity entity) {
        if (entity == null) {
            entity = new InspectionEntity();
            entity.setId(dto.getId());
        }

        entity.setNotes(dto.getNotes());
        entity.setFeeding(dto.isFeeding());
        entity.setQueen(dto.isQueen());
        entity.setInspectionDate(dto.getInspectionDate());
        entity.setBeehiveId(dto.getBeehiveId());
        entity.setTreatment(dto.isTreatment());

        return entity;
    }

    @Override
    public Inspection toDto(@Nullable InspectionEntity entity) {
        if (entity == null)
            return null;

        Inspection dto = new Inspection();
        dto.setId(entity.getId());
        dto.setNotes(entity.getNotes());
        dto.setFeeding(entity.isFeeding());
        dto.setQueen(entity.isQueen());
        dto.setInspectionDate(entity.getInspectionDate());
        dto.setBeehiveId(entity.getBeehiveId());
        dto.setTreatment(entity.isTreatment());

        return dto;
    }

}

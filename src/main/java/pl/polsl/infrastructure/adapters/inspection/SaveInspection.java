package pl.polsl.infrastructure.adapters.inspection;

import org.springframework.stereotype.Component;
import pl.polsl.domain.inspection.Inspection;
import pl.polsl.domain.inspection.exception.NoSuchInspection;
import pl.polsl.domain.inspection.service.ISaveInspection;
import pl.polsl.infrastructure.collection.IMapTo;

import java.util.Optional;

@Component
public class SaveInspection implements ISaveInspection {

    private InspectionRepository inspectionRepository;
    private IMapTo<Inspection, InspectionEntity> iMapTo;

    public SaveInspection(InspectionRepository inspectionRepository, IMapTo<Inspection, InspectionEntity> iMapTo) {
        this.inspectionRepository = inspectionRepository;
        this.iMapTo = iMapTo;
    }

    @Override
    public void save(Inspection inspection) {
        inspectionRepository.save(iMapTo.toEntity(inspection, null));
    }

    @Override
    public void update(Integer id, Inspection inspection) throws NoSuchInspection {
        Optional<InspectionEntity> entity = this.inspectionRepository.findById(id);

        entity.orElseThrow(NoSuchInspection::new);

        this.inspectionRepository.save(iMapTo.toEntity(inspection, entity.get()));
    }

    @Override
    public void delete(Integer id) {
        this.inspectionRepository.deleteById(id);
    }


}

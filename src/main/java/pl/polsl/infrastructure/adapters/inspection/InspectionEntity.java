package pl.polsl.infrastructure.adapters.inspection;

import javax.persistence.*;

@Table
@Entity(name = "inspection")
public class InspectionEntity {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "inspection_date",
            length = 20,
            nullable = false
    )
    private String inspectionDate;

    @Column(name = "feeding",
            nullable = false
    )
    private boolean feeding;

    @Column(name = "queen",
            nullable = false
    )
    private boolean queen;

    @Column(name = "treatment",
            nullable = false
    )
    private boolean treatment;

    @Column(name = "notes",
            nullable = false
    )
    private String notes;

    @Column(name = "beehive_id")
    private Integer beehiveId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getInspectionDate() {
        return inspectionDate;
    }

    public void setInspectionDate(String inspectionDate) {
        this.inspectionDate = inspectionDate;
    }

    public boolean isFeeding() {
        return feeding;
    }

    public void setFeeding(boolean feeding) {
        this.feeding = feeding;
    }

    public boolean isQueen() {
        return queen;
    }

    public void setQueen(boolean queen) {
        this.queen = queen;
    }

    public boolean isTreatment() {
        return treatment;
    }

    public void setTreatment(boolean treatment) {
        this.treatment = treatment;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Integer getBeehiveId() {
        return beehiveId;
    }

    public void setBeehiveId(Integer beehiveId) {
        this.beehiveId = beehiveId;
    }
}

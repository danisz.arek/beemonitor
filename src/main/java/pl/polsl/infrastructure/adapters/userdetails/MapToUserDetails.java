package pl.polsl.infrastructure.adapters.userdetails;

import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pl.polsl.domain.identity.service.IUserService;
import pl.polsl.domain.userdetails.UserDetails;
import pl.polsl.infrastructure.collection.IMapTo;
import pl.polsl.infrastructure.identity.repository.IUserRepository;

@Component
public class MapToUserDetails implements IMapTo<UserDetails, UserDetailsEntity> {

    private final IUserRepository iUserRepository;
    private final IUserService iUserService;


    public MapToUserDetails(IUserRepository iUserRepository, IUserService iUserService) {
        this.iUserRepository = iUserRepository;
        this.iUserService = iUserService;
    }

    public UserDetailsEntity toEntity(UserDetails dto, @Nullable UserDetailsEntity entity) {

        if (entity == null)
            entity = new UserDetailsEntity();

        if (dto.getUser() != null) {
            entity.setId(dto.getId());
//            entity.setUserEntity(iUserRepository.findById(dto.getUser().getId().getId()).orElse(null));
            entity.setUserEntity(iUserRepository.findOneByUsername(dto.getUser().getUsername().getUsername()));
            entity.setBeekeperName(dto.getBeekeperName());
            entity.setBeekeperSurname(dto.getBeekeperSurname());
            entity.setEmail(dto.getEmail());
            entity.setPostCode(dto.getPostCode());
            entity.setCity(dto.getCity());
            entity.setRoad(dto.getRoad());
            entity.setRoadNr(dto.getRoadNr());
        }

        return entity;
    }

    @Override
    public UserDetails toDto(@Nullable UserDetailsEntity entity) {

        if (entity == null)
            return null;

        UserDetails dto = new UserDetails();
        dto.setId(entity.getId());
        dto.setUser(iUserService.findOneByUsername(entity.getUserEntity().getUsername()));
//        dto.setUser(iUserService.byId(entity.getId()));
        dto.setBeekeperName(entity.getBeekeperName());
        dto.setBeekeperSurname(entity.getBeekeperSurname());
        dto.setEmail(entity.getEmail());
        dto.setPostCode(entity.getPostCode());
        dto.setCity(entity.getCity());
        dto.setRoad(entity.getRoad());
        dto.setRoadNr(entity.getRoadNr());

        return dto;
    }

}

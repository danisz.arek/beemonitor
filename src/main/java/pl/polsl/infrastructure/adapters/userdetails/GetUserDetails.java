package pl.polsl.infrastructure.adapters.userdetails;

import org.springframework.stereotype.Component;
import pl.polsl.domain.identity.service.IUserService;
import pl.polsl.domain.userdetails.UserDetails;
import pl.polsl.domain.userdetails.services.IGetUserDetails;
import pl.polsl.infrastructure.collection.IMapTo;
import pl.polsl.infrastructure.identity.repository.IUserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class GetUserDetails implements IGetUserDetails {

    private final UserDetailsRepository userDetailsRepository;
    private final IMapTo<UserDetails, UserDetailsEntity> iMapTo;
    private IUserRepository iUserRepository;
    private IUserService iUserService;

    public GetUserDetails(UserDetailsRepository userDetailsRepository, IMapTo<UserDetails, UserDetailsEntity> iMapTo) {
        this.userDetailsRepository = userDetailsRepository;
        this.iMapTo = iMapTo;
    }

    @Override
    public UserDetails byId(Integer id) {
        UserDetailsEntity entity = this.userDetailsRepository.findById(id).orElse(null);

        if (null == entity) {
            return null;
        }

        return iMapTo.toDto(entity);
    }

    @Override
    public List<UserDetails> getAll() {
        Iterable<UserDetailsEntity> userDetailsEntities = userDetailsRepository.findAll();
        List<UserDetails> output = new ArrayList<>();
        userDetailsEntities.forEach(entity -> output.add(iMapTo.toDto(entity)));

        return output;
    }

    @Override
    public UserDetails findByUserEntityId(Integer id) {

        Optional<UserDetailsEntity> userDetailsEntities = userDetailsRepository.findByUserEntityId(id);
        return iMapTo.toDto(userDetailsEntities.orElse(null));
    }

    @Override
    public List<UserDetails> findAllByUserEntityId(Integer id) {

        Iterable<UserDetailsEntity> userDetailsEntities = userDetailsRepository.findAllByUserEntityId(id);
        List<UserDetails> output = new ArrayList<>();
        userDetailsEntities.forEach(entity -> output.add(iMapTo.toDto(entity)));

        return output;
    }
}

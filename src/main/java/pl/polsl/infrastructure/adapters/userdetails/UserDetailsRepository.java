package pl.polsl.infrastructure.adapters.userdetails;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UserDetailsRepository extends CrudRepository<UserDetailsEntity, Integer> {
    Optional<UserDetailsEntity> findById(Integer id);

    Optional<UserDetailsEntity> findByUserEntityId(Integer userId);

    List<UserDetailsEntity> findAllByUserEntityId(Integer userId);
}

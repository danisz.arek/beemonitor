package pl.polsl.infrastructure.adapters.userdetails;

import pl.polsl.infrastructure.identity.entity.UserEntity;

import javax.persistence.*;

@Table
@Entity(name = "user_details")
public class UserDetailsEntity {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "beekeper_name",
            length = 20,
            nullable = false
    )
    private String beekeperName;

    @Column(name = "beekeper_surname",
            length = 20,
            nullable = false
    )
    private String beekeperSurname;

    @Column(name = "email",
            length = 50,
            nullable = false
    )
    private String email;

    @Column(name = "post_code",
            length = 9,
            nullable = false
    )
    private String postCode;

    @Column(name = "city",
            length = 20,
            nullable = false
    )
    private String city;

    @Column(name = "road",
            length = 20,
            nullable = false
    )
    private String road;

    @Column(name = "road_nr",
            length = 10,
            nullable = false
    )
    private String roadNr;

    @OneToOne
    private UserEntity userEntity;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBeekeperName() {
        return beekeperName;
    }

    public void setBeekeperName(String beekeperName) {
        this.beekeperName = beekeperName;
    }

    public String getBeekeperSurname() {
        return beekeperSurname;
    }

    public void setBeekeperSurname(String beekeperSurname) {
        this.beekeperSurname = beekeperSurname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRoad() {
        return road;
    }

    public void setRoad(String road) {
        this.road = road;
    }

    public String getRoadNr() {
        return roadNr;
    }

    public void setRoadNr(String roadNr) {
        this.roadNr = roadNr;
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(UserEntity userEntity) {
        this.userEntity = userEntity;
    }
}

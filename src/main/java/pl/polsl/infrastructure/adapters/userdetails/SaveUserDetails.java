package pl.polsl.infrastructure.adapters.userdetails;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import pl.polsl.domain.identity.service.IUserService;
import pl.polsl.domain.userdetails.UserDetails;
import pl.polsl.domain.userdetails.exception.NoSuchDetail;
import pl.polsl.domain.userdetails.services.IGetUserDetails;
import pl.polsl.domain.userdetails.services.ISaveUserDetails;
import pl.polsl.infrastructure.collection.IMapTo;
import pl.polsl.infrastructure.identity.repository.IUserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class SaveUserDetails implements ISaveUserDetails {

    private UserDetailsRepository userDetailsRepository;
    private IMapTo<UserDetails, UserDetailsEntity> iMapTo;
    private IUserService iUserService;


    public SaveUserDetails(UserDetailsRepository userDetailsRepository, IMapTo<UserDetails, UserDetailsEntity> iMapTo,
                           IUserService iUserService) {
        this.userDetailsRepository = userDetailsRepository;
        this.iMapTo = iMapTo;
        this.iUserService = iUserService;
    }

    @Override
    public ResponseEntity<HttpStatus> save(UserDetails userDetails) {

        List <UserDetailsEntity> list = new ArrayList<>();

        try {
            list = userDetailsRepository.findAllByUserEntityId(userDetails.getUser().getId().getId());
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (list.size() >= 1) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } else {
            userDetailsRepository.save(iMapTo.toEntity(userDetails, null));
            return new ResponseEntity<>(HttpStatus.OK);
        }
    }

    @Override
    public void updateByUser(Integer id, UserDetails userDetails) throws NoSuchDetail {
        Optional<UserDetailsEntity> entity = this.userDetailsRepository.findByUserEntityId(id);

        entity.orElseThrow(NoSuchDetail::new);

        userDetails.setUser(iUserService.findOneByUsername(entity.get().getUserEntity().getUsername()));
        this.userDetailsRepository.save(iMapTo.toEntity(userDetails, entity.get()));
    }

    @Override
    public void delete(Integer id) {
        this.userDetailsRepository.deleteById(id);
    }

    @Override
    public void deleteByUser(Integer userId) {

        List<UserDetailsEntity> detailsToDelete = userDetailsRepository.findAllByUserEntityId(userId);

        for (UserDetailsEntity detailToDelete : detailsToDelete)
            this.userDetailsRepository.deleteById(detailToDelete.getId());
    }
}

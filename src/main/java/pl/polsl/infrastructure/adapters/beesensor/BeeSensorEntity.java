package pl.polsl.infrastructure.adapters.beesensor;


import pl.polsl.infrastructure.adapters.beehive.BeehiveEntity;
import pl.polsl.infrastructure.adapters.beesensordata.BeeSensorDataEntity;

import javax.persistence.*;
import java.util.Set;

@Table
@Entity(name = "beesensor")
public class BeeSensorEntity {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "phone_no",
            length = 20,
            nullable = false
    )
    private String phoneNo;

    @OneToOne
    BeehiveEntity beehive;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "beeSensorId")
    private Set<BeeSensorDataEntity> beeSensorDataEntities;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }


    public Set<BeeSensorDataEntity> getBeeSensorDataEntities() {
        return beeSensorDataEntities;
    }

    public void setBeeSensorDataEntities(Set<BeeSensorDataEntity> beeSensorDataEntities) {
        this.beeSensorDataEntities = beeSensorDataEntities;
    }

    public BeehiveEntity getBeehiveEntity() {
        return beehive;
    }

    public void setBeehiveEntity(BeehiveEntity beehiveEntity) {
        this.beehive = beehiveEntity;
    }
}

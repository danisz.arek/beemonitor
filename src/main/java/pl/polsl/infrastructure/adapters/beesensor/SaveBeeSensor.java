package pl.polsl.infrastructure.adapters.beesensor;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import pl.polsl.domain.beesensor.BeeSensor;
import pl.polsl.domain.beesensor.exception.NoSuchBeeSensor;
import pl.polsl.domain.beesensor.service.ISaveBeeSensor;
import pl.polsl.domain.beesensor_data.BeeSensorData;
import pl.polsl.domain.beesensor_data.service.IGetBeeSensorData;
import pl.polsl.domain.beesensor_data.service.ISaveBeeSensorData;
import pl.polsl.infrastructure.collection.IMapTo;

import java.util.List;
import java.util.Optional;

@Service
public class SaveBeeSensor implements ISaveBeeSensor {

    private BeeSensorRepository beeSensorRepository;
    private IMapTo<BeeSensor, BeeSensorEntity> iMapTo;
    private ISaveBeeSensorData iSaveBeeSensorData;
    private IGetBeeSensorData iGetBeeSensorData;

    public SaveBeeSensor(BeeSensorRepository beeSensorRepository, IMapTo<BeeSensor, BeeSensorEntity> iMapTo,
                         ISaveBeeSensorData iSaveBeeSensorData, IGetBeeSensorData iGetBeeSensorData) {
        this.beeSensorRepository = beeSensorRepository;
        this.iMapTo = iMapTo;
        this.iSaveBeeSensorData = iSaveBeeSensorData;
        this.iGetBeeSensorData = iGetBeeSensorData;
    }

    @Override
    public ResponseEntity<HttpStatus> save(BeeSensor beeSensor) {

        List<BeeSensorEntity> beeSensors = beeSensorRepository.findAllByBeehiveId(beeSensor.getBeehive().getId());

        if (beeSensors.size() >= 1 ) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } else {
            beeSensorRepository.save(iMapTo.toEntity(beeSensor, null));
            return new ResponseEntity<>(HttpStatus.OK);
        }
    }

    @Override
    public void update(Integer id, BeeSensor beeSensor) throws NoSuchBeeSensor {
        Optional<BeeSensorEntity> entity = this.beeSensorRepository.findById(id);

        entity.orElseThrow(NoSuchBeeSensor::new);

        this.beeSensorRepository.save(iMapTo.toEntity(beeSensor, entity.get()));
    }

    @Override
    public void delete(Integer id) {

        List<BeeSensorData> beeSensorsDataToDelete = this.iGetBeeSensorData.findAllByBeeSensorId(id);

        for (BeeSensorData beeSensorDataToDelete : beeSensorsDataToDelete)
            iSaveBeeSensorData.delete(beeSensorDataToDelete.getId());

        this.beeSensorRepository.deleteById(id);
    }


}

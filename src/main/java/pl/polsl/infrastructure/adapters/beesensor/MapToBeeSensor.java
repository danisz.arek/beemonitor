package pl.polsl.infrastructure.adapters.beesensor;

import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pl.polsl.domain.beehive.service.IGetBeehive;
import pl.polsl.domain.beehive.service.ISaveBeehive;
import pl.polsl.domain.beesensor.BeeSensor;
import pl.polsl.domain.honeycollection.HoneyCollection;
import pl.polsl.infrastructure.adapters.beehive.BeehiveRepository;
import pl.polsl.infrastructure.adapters.honeycollection.HoneyCollectionEntity;
import pl.polsl.infrastructure.collection.IMapTo;

@Component
public class MapToBeeSensor implements IMapTo<BeeSensor, BeeSensorEntity> {

    private BeehiveRepository beehiveRepository;
    private IGetBeehive iGetBeehive;

    public MapToBeeSensor(BeehiveRepository beehiveRepository,
                          IGetBeehive iGetBeehive) {

        this.beehiveRepository = beehiveRepository;
        this.iGetBeehive = iGetBeehive;
    }

    public BeeSensorEntity toEntity(BeeSensor dto, @Nullable BeeSensorEntity entity) {
        if (entity == null) {
            entity = new BeeSensorEntity();
            entity.setId(dto.getId());
        }

        entity.setPhoneNo(dto.getPhoneNo());
        entity.setBeehiveEntity(beehiveRepository.findById(dto.getBeehive().getId()).orElse(null));

        return entity;
    }

    @Override
    public BeeSensor toDto(@Nullable BeeSensorEntity entity) {
        if (entity == null)
            return null;

        BeeSensor dto = new BeeSensor();
        dto.setId(entity.getId());
        dto.setPhoneNo(entity.getPhoneNo());
        dto.setBeehive(iGetBeehive.byId(entity.getId()));

        return dto;
    }

}

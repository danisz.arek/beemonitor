package pl.polsl.infrastructure.adapters.beesensor;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface BeeSensorRepository extends CrudRepository<BeeSensorEntity, Integer> {
    Optional<BeeSensorEntity> findById(Integer id);

    List<BeeSensorEntity> findAllByBeehiveId(Integer userId);

    Optional<BeeSensorEntity> findByBeehiveId(Integer id);

    Optional<BeeSensorEntity> findByPhoneNo(String phoneNo);
}

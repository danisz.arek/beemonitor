package pl.polsl.infrastructure.adapters.beesensor;

import org.springframework.stereotype.Component;
import pl.polsl.domain.beesensor.BeeSensor;
import pl.polsl.domain.beesensor.service.IGetBeeSensor;
import pl.polsl.domain.honeycollection.HoneyCollection;
import pl.polsl.domain.honeycollection.service.IGetHoneyCollection;
import pl.polsl.infrastructure.adapters.honeycollection.HoneyCollectionEntity;
import pl.polsl.infrastructure.adapters.honeycollection.HoneyCollectionRepository;
import pl.polsl.infrastructure.collection.IMapTo;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class GetBeeSensor implements IGetBeeSensor {

    private final BeeSensorRepository beeSensorRepository;
    private final IMapTo<BeeSensor, BeeSensorEntity> iMapTo;

    public GetBeeSensor(BeeSensorRepository beeSensorRepository, IMapTo<BeeSensor, BeeSensorEntity> iMapTo) {
        this.beeSensorRepository = beeSensorRepository;
        this.iMapTo = iMapTo;
    }


    @Override
    public BeeSensor byId(Integer id) {
        BeeSensorEntity entity = this.beeSensorRepository.findById(id).orElse(null);

        if (null == entity) {
            return null;
        }

        return iMapTo.toDto(entity);
    }

    @Override
    public List<BeeSensor> getAll() {
        Iterable<BeeSensorEntity> beeSensorEntities = beeSensorRepository.findAll();
        List<BeeSensor> output = new ArrayList<>();
        beeSensorEntities.forEach(entity -> output.add(iMapTo.toDto(entity)));

        return output;
    }

    @Override
    public List<BeeSensor> findAllByBeehiveId(Integer id) {
        Iterable<BeeSensorEntity> beeSensorEntities = beeSensorRepository.findAllByBeehiveId(id);
        List<BeeSensor> output = new ArrayList<>();
        beeSensorEntities.forEach(entity -> output.add(iMapTo.toDto(entity)));

        return output;
    }

    @Override
    public BeeSensor findByBeehiveId(Integer beehiveId) {
        Optional<BeeSensorEntity> beeSensorEntity = beeSensorRepository.findByBeehiveId(beehiveId);
        return iMapTo.toDto(beeSensorEntity.orElse(null));
    }

    @Override
    public BeeSensor findByPhoneNo(String phoneNo) {
        Optional<BeeSensorEntity> beeSensorEntity = beeSensorRepository.findByPhoneNo(phoneNo);
        return iMapTo.toDto(beeSensorEntity.orElse(null));
    }

}

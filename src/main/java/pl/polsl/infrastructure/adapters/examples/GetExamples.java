package pl.polsl.infrastructure.adapters.examples;

import org.springframework.stereotype.Component;
import pl.polsl.domain.examples.Example;
import pl.polsl.domain.examples.services.IGetExamples;

import java.util.ArrayList;
import java.util.List;

@Component
public class GetExamples implements IGetExamples {

    private ExampleRepository exampleRepository;

    public GetExamples(ExampleRepository examplesRepository) {
        this.exampleRepository = examplesRepository;
    }

    @Override
    public Example byId(Integer id) {
        ExampleEntity example = exampleRepository.findById(id).orElse(null);
        if (example == null) {
            return null;
        }

        return toDto(example);
    }

    @Override
    public List<Example> getAll() {
        Iterable<ExampleEntity> exampleEntityList = exampleRepository.findAll();
        List<Example> output = new ArrayList<>();

        exampleEntityList.forEach(entity -> output.add(toDto(entity)));

        return output;
    }

    private Example toDto(ExampleEntity entity) {
        Example exampleDto = new Example();

        exampleDto.setId(entity.getId());
        exampleDto.setMessage(entity.getMessage());

        return exampleDto;
    }
}

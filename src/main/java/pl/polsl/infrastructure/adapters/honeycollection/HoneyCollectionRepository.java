package pl.polsl.infrastructure.adapters.honeycollection;

import org.springframework.data.repository.CrudRepository;
import pl.polsl.infrastructure.adapters.inspection.InspectionEntity;

import java.util.List;
import java.util.Optional;

public interface HoneyCollectionRepository extends CrudRepository<HoneyCollectionEntity, Integer> {
    Optional<HoneyCollectionEntity> findById(Integer id);

    List<HoneyCollectionEntity> findAllByBeehiveId(Integer userId);

    List<HoneyCollectionEntity> findAllByBeehiveIdIsNull();

    Optional<HoneyCollectionEntity> findByBeehiveId(Integer id);
}

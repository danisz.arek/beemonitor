package pl.polsl.infrastructure.adapters.honeycollection;

import org.springframework.stereotype.Component;
import pl.polsl.domain.beehive.Beehive;
import pl.polsl.domain.beehive.exception.NoSuchBeehive;
import pl.polsl.domain.beehive.service.ISaveBeehive;
import pl.polsl.domain.honeycollection.HoneyCollection;
import pl.polsl.domain.honeycollection.exception.NoSuchHoneycollection;
import pl.polsl.domain.honeycollection.service.ISaveHoneycollection;
import pl.polsl.infrastructure.adapters.beehive.BeehiveEntity;
import pl.polsl.infrastructure.adapters.beehive.BeehiveRepository;
import pl.polsl.infrastructure.collection.IMapTo;

import java.util.Optional;

@Component
public class SaveHoneyCollection implements ISaveHoneycollection {

    private HoneyCollectionRepository honeyCollectionRepository;
    private IMapTo<HoneyCollection, HoneyCollectionEntity> iMapTo;

    public SaveHoneyCollection(HoneyCollectionRepository honeyCollectionRepository, IMapTo<HoneyCollection, HoneyCollectionEntity> iMapTo) {
        this.honeyCollectionRepository = honeyCollectionRepository;
        this.iMapTo = iMapTo;
    }

    @Override
    public void save(HoneyCollection honeyCollection) {
        honeyCollectionRepository.save(iMapTo.toEntity(honeyCollection, null));
    }

    @Override
    public void update(Integer id, HoneyCollection honeyCollection) throws NoSuchHoneycollection {
        Optional<HoneyCollectionEntity> entity = this.honeyCollectionRepository.findById(id);

        entity.orElseThrow(NoSuchHoneycollection::new);

        this.honeyCollectionRepository.save(iMapTo.toEntity(honeyCollection, entity.get()));
    }

    @Override
    public void delete(Integer id) {
        this.honeyCollectionRepository.deleteById(id);
    }


}

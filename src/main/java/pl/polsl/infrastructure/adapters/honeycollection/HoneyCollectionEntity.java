package pl.polsl.infrastructure.adapters.honeycollection;

import javax.persistence.*;

@Table
@Entity(name = "honey_collection")
public class HoneyCollectionEntity {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "collection_date",
            length = 20,
            nullable = false
    )
    private String collectionDate;

    @Column(name = "honey_type",
            length = 30,
            nullable = false
    )
    private String honeyType;

    @Column(name = "amount",
            nullable = false
    )
    private Integer amount;

    @Column(name = "beehive_id")
    private Integer beehiveId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCollectionDate() {
        return collectionDate;
    }

    public void setCollectionDate(String collectionDate) {
        this.collectionDate = collectionDate;
    }

    public String getHoneyType() {
        return honeyType;
    }

    public void setHoneyType(String honeyType) {
        this.honeyType = honeyType;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getBeehiveId() {
        return beehiveId;
    }

    public void setBeehiveId(Integer beehiveId) {
        this.beehiveId = beehiveId;
    }
}

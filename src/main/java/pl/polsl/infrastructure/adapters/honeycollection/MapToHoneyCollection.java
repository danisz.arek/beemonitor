package pl.polsl.infrastructure.adapters.honeycollection;

import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pl.polsl.domain.honeycollection.HoneyCollection;
import pl.polsl.infrastructure.collection.IMapTo;

@Component
public class MapToHoneyCollection implements IMapTo<HoneyCollection, HoneyCollectionEntity> {

    public HoneyCollectionEntity toEntity(HoneyCollection dto, @Nullable HoneyCollectionEntity entity) {
        if (entity == null) {
            entity = new HoneyCollectionEntity();
            entity.setId(dto.getId());
        }

        entity.setCollectionDate(dto.getCollectionDate());
        entity.setHoneyType(dto.getHoneyType());
        entity.setAmount(dto.getAmount());
        entity.setBeehiveId(dto.getBeehiveId());

        return entity;
    }

    @Override
    public HoneyCollection toDto(@Nullable HoneyCollectionEntity entity) {
        if (entity == null)
            return null;

        HoneyCollection dto = new HoneyCollection();
        dto.setId(entity.getId());
        dto.setCollectionDate(entity.getCollectionDate());
        dto.setHoneyType(entity.getHoneyType());
        dto.setAmount(entity.getAmount());
        dto.setBeehiveId(entity.getBeehiveId());

        return dto;
    }

}

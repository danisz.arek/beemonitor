package pl.polsl.infrastructure.adapters.honeycollection;

import org.springframework.stereotype.Component;
import pl.polsl.domain.beehive.Beehive;
import pl.polsl.domain.beehive.service.IGetBeehive;
import pl.polsl.domain.honeycollection.HoneyCollection;
import pl.polsl.domain.honeycollection.service.IGetHoneyCollection;
import pl.polsl.infrastructure.adapters.beehive.BeehiveEntity;
import pl.polsl.infrastructure.adapters.beehive.BeehiveRepository;
import pl.polsl.infrastructure.collection.IMapTo;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class GetHoneycollection implements IGetHoneyCollection {

    private final HoneyCollectionRepository honeyCollectionRepository;
    private final IMapTo<HoneyCollection, HoneyCollectionEntity> iMapTo;

    public GetHoneycollection(HoneyCollectionRepository honeyCollectionRepository, IMapTo<HoneyCollection, HoneyCollectionEntity> iMapTo) {
        this.honeyCollectionRepository = honeyCollectionRepository;
        this.iMapTo = iMapTo;
    }


    @Override
    public HoneyCollection byId(Integer id) {
        HoneyCollectionEntity entity = this.honeyCollectionRepository.findById(id).orElse(null);

        if (null == entity) {
            return null;
        }

        return iMapTo.toDto(entity);
    }

    @Override
    public List<HoneyCollection> getAll() {
        Iterable<HoneyCollectionEntity> honeyCollectionEntitiesList = honeyCollectionRepository.findAll();
        List<HoneyCollection> output = new ArrayList<>();
        honeyCollectionEntitiesList.forEach(entity -> output.add(iMapTo.toDto(entity)));

        return output;
    }

    @Override
    public List<HoneyCollection> findAllByBeehiveId(Integer id) {
        Iterable<HoneyCollectionEntity> honeyCollectionEntitiesList = honeyCollectionRepository.findAllByBeehiveId(id);
        List<HoneyCollection> output = new ArrayList<>();
        honeyCollectionEntitiesList.forEach(entity -> output.add(iMapTo.toDto(entity)));

        return output;
    }

    @Override
    public List<HoneyCollection> getUnassigned() {
        Iterable<HoneyCollectionEntity> honeyCollectionEntitiesList = honeyCollectionRepository.findAllByBeehiveIdIsNull();
        List<HoneyCollection> output = new ArrayList<>();
        honeyCollectionEntitiesList.forEach(entity -> output.add(iMapTo.toDto(entity)));

        return output;
    }

    @Override
    public HoneyCollection findByBeehiveId(Integer beehiveId) {

        Optional<HoneyCollectionEntity> honeyCollection = honeyCollectionRepository.findByBeehiveId(beehiveId);
        return iMapTo.toDto(honeyCollection.orElse(null));
    }
}

package pl.polsl.infrastructure.adapters.beequeen;

import org.springframework.stereotype.Component;
import pl.polsl.domain.beequeen.BeeQueen;
import pl.polsl.domain.beequeen.exception.NoSuchBeeQueen;
import pl.polsl.domain.beequeen.service.ISaveBeeQueen;
import pl.polsl.infrastructure.collection.IMapTo;

import java.util.Optional;

@Component
public class SaveBeeQueen implements ISaveBeeQueen {

    private BeeQueenRepository beeQueenRepository;
    private IMapTo<BeeQueen, BeeQueenEntity> iMapTo;

    public SaveBeeQueen(BeeQueenRepository beeQueenRepository, IMapTo<BeeQueen, BeeQueenEntity> iMapTo) {
        this.beeQueenRepository = beeQueenRepository;
        this.iMapTo = iMapTo;
    }

    @Override
    public void save(BeeQueen beeQueen) {
        beeQueenRepository.save(iMapTo.toEntity(beeQueen, null));
    }

    @Override
    public void update(Integer id, BeeQueen beeQueen) throws NoSuchBeeQueen {
        Optional<BeeQueenEntity> entity = this.beeQueenRepository.findById(id);

        entity.orElseThrow(NoSuchBeeQueen::new);

        this.beeQueenRepository.save(iMapTo.toEntity(beeQueen, entity.get()));
    }

    @Override
    public void delete(Integer id) {
        this.beeQueenRepository.deleteById(id);
    }


}

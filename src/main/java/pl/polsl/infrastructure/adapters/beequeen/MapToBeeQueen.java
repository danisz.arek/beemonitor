package pl.polsl.infrastructure.adapters.beequeen;

import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pl.polsl.domain.beequeen.BeeQueen;
import pl.polsl.domain.inspection.Inspection;
import pl.polsl.infrastructure.adapters.inspection.InspectionEntity;
import pl.polsl.infrastructure.collection.IMapTo;

@Component
public class MapToBeeQueen implements IMapTo<BeeQueen, BeeQueenEntity> {

    public BeeQueenEntity toEntity(BeeQueen dto, @Nullable BeeQueenEntity entity) {
        if (entity == null) {
            entity = new BeeQueenEntity();
            entity.setId(dto.getId());
        }

        entity.setQueenNo(dto.getQueenNo());
        entity.setColour(dto.getColour());
        entity.setFertile(dto.isFertile());
        entity.setVariety(dto.getVariety());
        entity.setDateOfApplication(dto.getDateOfApplication());
        entity.setBeehiveId(dto.getBeehiveId());

        return entity;
    }

    @Override
    public BeeQueen toDto(@Nullable BeeQueenEntity entity) {
        if (entity == null)
            return null;

        BeeQueen dto = new BeeQueen();
        dto.setId(entity.getId());
        dto.setQueenNo(entity.getQueenNo());
        dto.setColour(entity.getColour());
        dto.setFertile(entity.isFertile());
        dto.setVariety(entity.getVariety());
        dto.setDateOfApplication(entity.getDateOfApplication());
        dto.setBeehiveId(entity.getBeehiveId());

        return dto;
    }

}

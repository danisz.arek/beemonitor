package pl.polsl.infrastructure.adapters.beequeen;

import org.springframework.data.repository.CrudRepository;
import pl.polsl.infrastructure.adapters.beesensor.BeeSensorEntity;
import pl.polsl.infrastructure.adapters.inspection.InspectionEntity;

import java.util.List;
import java.util.Optional;

public interface BeeQueenRepository extends CrudRepository<BeeQueenEntity, Integer> {

    Optional<BeeQueenEntity> findById(Integer id);

    List<BeeQueenEntity> findAllByBeehiveId(Integer beehiveId);

    List<BeeQueenEntity> findAllByBeehiveIdIsNull();

    Optional<BeeQueenEntity> findByBeehiveId(Integer id);
}

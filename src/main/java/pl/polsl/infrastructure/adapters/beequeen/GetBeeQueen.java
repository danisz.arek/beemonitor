package pl.polsl.infrastructure.adapters.beequeen;

import org.springframework.stereotype.Component;
import pl.polsl.domain.beequeen.BeeQueen;
import pl.polsl.domain.beequeen.service.IGetBeeQueen;
import pl.polsl.domain.beesensor.BeeSensor;
import pl.polsl.domain.inspection.Inspection;
import pl.polsl.domain.inspection.service.IGetInspection;
import pl.polsl.infrastructure.adapters.beesensor.BeeSensorEntity;
import pl.polsl.infrastructure.adapters.inspection.InspectionEntity;
import pl.polsl.infrastructure.adapters.inspection.InspectionRepository;
import pl.polsl.infrastructure.collection.IMapTo;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class GetBeeQueen implements IGetBeeQueen {

    private final BeeQueenRepository beeQueenRepository;
    private final IMapTo<BeeQueen, BeeQueenEntity> iMapTo;

    public GetBeeQueen(BeeQueenRepository beeQueenRepository, IMapTo<BeeQueen, BeeQueenEntity> iMapTo) {
        this.beeQueenRepository = beeQueenRepository;
        this.iMapTo = iMapTo;
    }


    @Override
    public BeeQueen byId(Integer id) {
        BeeQueenEntity entity = this.beeQueenRepository.findById(id).orElse(null);

        if (null == entity) {
            return null;
        }

        return iMapTo.toDto(entity);
    }

    @Override
    public List<BeeQueen> getAll() {
        Iterable<BeeQueenEntity> beeQueenEntities = beeQueenRepository.findAll();
        List<BeeQueen> output = new ArrayList<>();
        beeQueenEntities.forEach(entity -> output.add(iMapTo.toDto(entity)));

        return output;
    }

    @Override
    public List<BeeQueen> findAllByBeehiveId(Integer id) {
        Iterable<BeeQueenEntity> beeQueenEntities = beeQueenRepository.findAllByBeehiveId(id);
        List<BeeQueen> output = new ArrayList<>();
        beeQueenEntities.forEach(entity -> output.add(iMapTo.toDto(entity)));

        return output;
    }

    @Override
    public List<BeeQueen> getUnassigned() {
        Iterable<BeeQueenEntity> beeQueenEntities = beeQueenRepository.findAllByBeehiveIdIsNull();
        List<BeeQueen> output = new ArrayList<>();
        beeQueenEntities.forEach(entity -> output.add(iMapTo.toDto(entity)));

        return output;
    }

    @Override
    public BeeQueen findByBeehiveId(Integer beehiveId) {
        Optional<BeeQueenEntity> beeQueenEntity = beeQueenRepository.findByBeehiveId(beehiveId);
        return iMapTo.toDto(beeQueenEntity.orElse(null));
    }
}

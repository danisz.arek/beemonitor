package pl.polsl.infrastructure.adapters.beequeen;

import javax.persistence.*;

@Table
@Entity(name = "beequeen")
public class BeeQueenEntity {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "queen_no",
            nullable = false
    )
    private Integer queenNo;

    @Column(name = "colour",
            length = 20,
            nullable = false)
    private String colour;

    @Column(name = "fertile",
            nullable = false
    )
    private boolean fertile;

    @Column(name = "variety",
            length = 20,
            nullable = false
    )
    private String variety;

    @Column(name = "date_of_application",
            length = 20,
            nullable = false
    )
    private String dateOfApplication;

    @Column(name = "beehive_id")
    private Integer beehiveId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQueenNo() {
        return queenNo;
    }

    public void setQueenNo(Integer queenNo) {
        this.queenNo = queenNo;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public boolean isFertile() {
        return fertile;
    }

    public void setFertile(boolean fertile) {
        this.fertile = fertile;
    }

    public String getVariety() {
        return variety;
    }

    public void setVariety(String variety) {
        this.variety = variety;
    }

    public String getDateOfApplication() {
        return dateOfApplication;
    }

    public void setDateOfApplication(String dateOfApplication) {
        this.dateOfApplication = dateOfApplication;
    }

    public Integer getBeehiveId() {
        return beehiveId;
    }

    public void setBeehiveId(Integer beehiveId) {
        this.beehiveId = beehiveId;
    }
}

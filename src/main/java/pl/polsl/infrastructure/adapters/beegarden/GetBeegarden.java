package pl.polsl.infrastructure.adapters.beegarden;

import org.springframework.stereotype.Component;
import pl.polsl.domain.beegarden.Beegarden;
import pl.polsl.domain.beegarden.services.IGetBeegarden;
import pl.polsl.infrastructure.collection.IMapTo;

import java.util.ArrayList;
import java.util.List;

@Component
public class GetBeegarden implements IGetBeegarden {

    private final BeegardenRepository beegardenRepository;
    private final IMapTo<Beegarden, BeegardenEntity> iMapTo;

    public GetBeegarden(BeegardenRepository beegardenRepository, IMapTo<Beegarden, BeegardenEntity> iMapTo) {
        this.beegardenRepository = beegardenRepository;
        this.iMapTo = iMapTo;
    }


    @Override
    public Beegarden byId(Integer id) {
        BeegardenEntity entity = this.beegardenRepository.findById(id).orElse(null);

        if (null == entity) {
            return null;
        }

        return iMapTo.toDto(entity);
    }

    @Override
    public List<Beegarden> getAll() {
        Iterable<BeegardenEntity> beegardensList = beegardenRepository.findAll();
        List<Beegarden> output = new ArrayList<>();
        beegardensList.forEach(entity -> output.add(iMapTo.toDto(entity)));

        return output;
    }

    @Override
    public List<Beegarden> findAllByUserId(Integer id) {
        Iterable<BeegardenEntity> beegardensList = beegardenRepository.findAllByUserId(id);
        List<Beegarden> output = new ArrayList<>();
        beegardensList.forEach(entity -> output.add(iMapTo.toDto(entity)));

        return output;
    }

    @Override
    public List<Beegarden> getUnassigned() {
        Iterable<BeegardenEntity> beegardensList = beegardenRepository.findAllByUserIdIsNull();
        List<Beegarden> output = new ArrayList<>();
        beegardensList.forEach(entity -> output.add(iMapTo.toDto(entity)));

        return output;
    }
}

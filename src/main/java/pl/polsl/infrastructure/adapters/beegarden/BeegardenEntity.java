package pl.polsl.infrastructure.adapters.beegarden;

import pl.polsl.infrastructure.adapters.beehive.BeehiveEntity;

import javax.persistence.*;
import java.util.Set;

@Table
@Entity(name = "beegarden")
public class BeegardenEntity {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "beegarden_name",
            nullable = false,
            length = 60
    )
    private String beegardenName;

    @Column(name = "location",
            nullable = false,
            length = 30
    )
    private String location;

    @Column(name = "type",
            nullable = false,
            length = 30
    )
    private String type;

    @Column(name = "user_id",
        nullable = false
    )
    private Integer userId;

    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "beegardenId")
    private Set<BeehiveEntity> beehiveEntities;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Set<BeehiveEntity> getBeehiveEntities() {
        return beehiveEntities;
    }

    public void setBeehiveEntities(Set<BeehiveEntity> beehiveEntities) {
        this.beehiveEntities = beehiveEntities;
    }

    public String getBeegardenName() {
        return beegardenName;
    }

    public void setBeegardenName(String beegardenName) {
        this.beegardenName = beegardenName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

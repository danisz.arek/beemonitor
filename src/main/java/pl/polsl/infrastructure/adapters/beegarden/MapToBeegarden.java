package pl.polsl.infrastructure.adapters.beegarden;

import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pl.polsl.domain.beegarden.Beegarden;
import pl.polsl.infrastructure.collection.IMapTo;

@Component
public class MapToBeegarden implements IMapTo<Beegarden, BeegardenEntity> {

    public BeegardenEntity toEntity(Beegarden dto, @Nullable BeegardenEntity entity) {
        if (entity == null) {
            entity = new BeegardenEntity();
            entity.setId(dto.getId());
        }

        entity.setBeegardenName(dto.getBeegardenName());
        entity.setLocation(dto.getLocation());
        entity.setType(dto.getType());
        entity.setUserId(dto.getUserId());

        return entity;
    }

    @Override
    public Beegarden toDto(@Nullable BeegardenEntity entity) {
        if (entity == null)
            return null;

        Beegarden dto = new Beegarden();

        dto.setId(entity.getId());
        dto.setBeegardenName(entity.getBeegardenName());
        dto.setLocation(entity.getLocation());
        dto.setType(entity.getType());
        dto.setUserId(entity.getUserId());

        return dto;
    }

}

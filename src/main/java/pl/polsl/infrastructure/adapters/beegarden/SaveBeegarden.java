package pl.polsl.infrastructure.adapters.beegarden;

import org.springframework.stereotype.Component;
import pl.polsl.domain.beegarden.Beegarden;
import pl.polsl.domain.beegarden.exception.NoSuchBeegarden;
import pl.polsl.domain.beegarden.services.ISaveBeegarden;
import pl.polsl.domain.beehive.Beehive;
import pl.polsl.domain.beehive.service.IGetBeehive;
import pl.polsl.domain.beehive.service.ISaveBeehive;
import pl.polsl.infrastructure.collection.IMapTo;

import java.util.List;
import java.util.Optional;

@Component
public class SaveBeegarden implements ISaveBeegarden {

    private BeegardenRepository beegardenRepository;
    private IMapTo<Beegarden, BeegardenEntity> iMapTo;
    private IGetBeehive iGetBeehive;
    private ISaveBeehive iSaveBeehive;

    public SaveBeegarden(BeegardenRepository beegardenRepository, IMapTo<Beegarden, BeegardenEntity> iMapTo, IGetBeehive iGetBeehive, ISaveBeehive iSaveBeehive) {
        this.beegardenRepository = beegardenRepository;
        this.iMapTo = iMapTo;
        this.iGetBeehive = iGetBeehive;
        this.iSaveBeehive = iSaveBeehive;
    }

    @Override
    public void save(Beegarden beegarden) {
        beegardenRepository.save(iMapTo.toEntity(beegarden, null));
    }

    @Override
    public void update(Integer id, Beegarden beegarden) throws NoSuchBeegarden {
        Optional<BeegardenEntity> entity = this.beegardenRepository.findById(id);

        entity.orElseThrow(NoSuchBeegarden::new);

        this.beegardenRepository.save(iMapTo.toEntity(beegarden, entity.get()));
    }

    @Override
    public void delete(Integer id) {

        List<Beehive> beehivesToDelete = this.iGetBeehive.findAllByBeegardenId(id);

        for (Beehive beehiveToDelete : beehivesToDelete)
            iSaveBeehive.delete(beehiveToDelete.getId());

        this.beegardenRepository.deleteById(id);
    }


}

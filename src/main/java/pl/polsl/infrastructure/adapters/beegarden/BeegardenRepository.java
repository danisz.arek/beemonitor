package pl.polsl.infrastructure.adapters.beegarden;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface BeegardenRepository extends CrudRepository<BeegardenEntity, Integer> {
    Optional<BeegardenEntity> findById(Integer id);

    List<BeegardenEntity> findAllByUserId(Integer userId);

    List<BeegardenEntity> findAllByUserIdIsNull();
}

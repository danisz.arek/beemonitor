package pl.polsl.infrastructure.adapters.beehive;

import pl.polsl.infrastructure.adapters.beequeen.BeeQueenEntity;
import pl.polsl.infrastructure.adapters.beesensor.BeeSensorEntity;
import pl.polsl.infrastructure.adapters.honeycollection.HoneyCollectionEntity;
import pl.polsl.infrastructure.adapters.inspection.InspectionEntity;

import javax.persistence.*;
import java.util.Set;

@Table
@Entity(name = "beehive")
public class BeehiveEntity {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "beehive_name",
            length = 30,
            nullable = false
    )
    private String beehiveName;

    @Column(name = "type",
            length = 30,
            nullable = false
    )
    private String type;

    @Column(name = "date_of_creation",
            length = 20,
            nullable = false
    )
    private String dateOfCreation;

    @Column(name = "frames_amount")
    private Integer framesAmount;

    @Column(name = "trunks_amount")
    private Integer trunksAmount;

    @Column(name = "bottom",
            length = 30,
            nullable = false
    )
    private String bottom;

    @Column(name = "feeder",
            nullable = false
    )
    private boolean feeder;

    @Column(name = "poolen_trapper",
            nullable = false
    )
    private boolean poolenTrapper;

    @Column(name = "propolis_sinker",
            nullable = false
    )
    private boolean propolisSinker;

    @Column(name = "queen_cage",
            nullable = false
    )
    private boolean queenCage;

    @Column(name = "barrier",
            nullable = false
    )
    private boolean barrier;

    @Column(name = "isolator",
            nullable = false
    )
    private boolean isolator;

    @Column(name = "heater",
            nullable = false
    )
    private boolean heater;

    @Column(name = "inlet_width",
            nullable = false
    )
    private Integer inletWidth;

    @Column(name = "work_frames_amount",
            nullable = false
    )
    private Integer workFramesAmount;

    @Column(name = "bee_force",
            length = 30,
            nullable = false
    )
    private String force;

    @Column(name = "temperament",
            length = 30,
            nullable = false
    )
    private String temperament;

    @Column(name = "queen_cell",
            nullable = false
    )
    private boolean queenCell;

    @Column(name = "swarm_mood",
            nullable = false
    )
    private boolean swarmMood;

    @Column(name = "lack_of_food",
            nullable = false
    )
    private boolean lackOfFood;

    @Column(name = "grubs",
            nullable = false
    )
    private Integer grubs;

    @Column(name = "propolis",
            nullable = false
    )
    private Integer propolis;

    @Column(name = "honey",
            nullable = false
    )
    private Integer honey;

    @Column(name = "beegarden_id",
            nullable = false
    )
    private Integer beegardenId;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "beehiveId")
    private Set<HoneyCollectionEntity> honeyCollectionEntities;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "beehiveId")
    private Set<InspectionEntity> inspectionEntities;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "beehiveId")
    private Set<BeeQueenEntity> beeQueenEntities;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBeehiveName() {
        return beehiveName;
    }

    public void setBeehiveName(String beehiveName) {
        this.beehiveName = beehiveName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(String dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public Integer getFramesAmount() {
        return framesAmount;
    }

    public void setFramesAmount(Integer framesAmount) {
        this.framesAmount = framesAmount;
    }

    public Integer getTrunksAmount() {
        return trunksAmount;
    }

    public void setTrunksAmount(Integer trunksAmount) {
        this.trunksAmount = trunksAmount;
    }

    public String getBottom() {
        return bottom;
    }

    public void setBottom(String bottom) {
        this.bottom = bottom;
    }

    public boolean isFeeder() {
        return feeder;
    }

    public void setFeeder(boolean feeder) {
        this.feeder = feeder;
    }

    public boolean isPoolenTrapper() {
        return poolenTrapper;
    }

    public void setPoolenTrapper(boolean poolenTrapper) {
        this.poolenTrapper = poolenTrapper;
    }

    public boolean isPropolisSinker() {
        return propolisSinker;
    }

    public void setPropolisSinker(boolean propolisSinker) {
        this.propolisSinker = propolisSinker;
    }

    public boolean isQueenCage() {
        return queenCage;
    }

    public void setQueenCage(boolean queenCage) {
        this.queenCage = queenCage;
    }

    public boolean isBarrier() {
        return barrier;
    }

    public void setBarrier(boolean barrier) {
        this.barrier = barrier;
    }

    public boolean isIsolator() {
        return isolator;
    }

    public void setIsolator(boolean isolator) {
        this.isolator = isolator;
    }

    public boolean isHeater() {
        return heater;
    }

    public void setHeater(boolean heater) {
        this.heater = heater;
    }

    public Integer getInletWidth() {
        return inletWidth;
    }

    public void setInletWidth(Integer inletWidth) {
        this.inletWidth = inletWidth;
    }

    public Integer getWorkFramesAmount() {
        return workFramesAmount;
    }

    public void setWorkFramesAmount(Integer workFramesAmount) {
        this.workFramesAmount = workFramesAmount;
    }

    public String getForce() {
        return force;
    }

    public void setForce(String force) {
        this.force = force;
    }

    public String getTemperament() {
        return temperament;
    }

    public void setTemperament(String temperament) {
        this.temperament = temperament;
    }

    public boolean isQueenCell() {
        return queenCell;
    }

    public void setQueenCell(boolean queenCell) {
        this.queenCell = queenCell;
    }

    public boolean isSwarmMood() {
        return swarmMood;
    }

    public void setSwarmMood(boolean swarmMood) {
        this.swarmMood = swarmMood;
    }

    public boolean isLackOfFood() {
        return lackOfFood;
    }

    public void setLackOfFood(boolean lackOfFood) {
        this.lackOfFood = lackOfFood;
    }

    public Integer getGrubs() {
        return grubs;
    }

    public void setGrubs(Integer grubs) {
        this.grubs = grubs;
    }

    public Integer getPropolis() {
        return propolis;
    }

    public void setPropolis(Integer propolis) {
        this.propolis = propolis;
    }

    public Integer getHoney() {
        return honey;
    }

    public void setHoney(Integer honey) {
        this.honey = honey;
    }

    public Integer getBeegardenId() {
        return beegardenId;
    }

    public void setBeegardenId(Integer beegardenId) {
        this.beegardenId = beegardenId;
    }

    public Set<HoneyCollectionEntity> getHoneyCollectionEntities() {
        return honeyCollectionEntities;
    }

    public void setHoneyCollectionEntities(Set<HoneyCollectionEntity> honeyCollectionEntities) {
        this.honeyCollectionEntities = honeyCollectionEntities;
    }

    public Set<InspectionEntity> getInspectionEntities() {
        return inspectionEntities;
    }

    public void setInspectionEntities(Set<InspectionEntity> inspectionEntities) {
        this.inspectionEntities = inspectionEntities;
    }

    public Set<BeeQueenEntity> getBeeQueenEntities() {
        return beeQueenEntities;
    }

    public void setBeeQueenEntities(Set<BeeQueenEntity> beeQueenEntities) {
        this.beeQueenEntities = beeQueenEntities;
    }
}

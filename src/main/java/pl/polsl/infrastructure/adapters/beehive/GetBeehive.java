package pl.polsl.infrastructure.adapters.beehive;

import org.springframework.stereotype.Component;
import pl.polsl.domain.beegarden.Beegarden;
import pl.polsl.domain.beegarden.services.IGetBeegarden;
import pl.polsl.domain.beehive.Beehive;
import pl.polsl.domain.beehive.service.IGetBeehive;
import pl.polsl.infrastructure.adapters.beegarden.BeegardenEntity;
import pl.polsl.infrastructure.adapters.beegarden.BeegardenRepository;
import pl.polsl.infrastructure.collection.IMapTo;

import java.util.ArrayList;
import java.util.List;

@Component
public class GetBeehive implements IGetBeehive {

    private final BeehiveRepository beehiveRepository;
    private final IMapTo<Beehive, BeehiveEntity> iMapTo;

    public GetBeehive(BeehiveRepository beehiveRepository, IMapTo<Beehive, BeehiveEntity> iMapTo) {
        this.beehiveRepository = beehiveRepository;
        this.iMapTo = iMapTo;
    }


    @Override
    public Beehive byId(Integer id) {
        BeehiveEntity entity = this.beehiveRepository.findById(id).orElse(null);

        if (null == entity) {
            return null;
        }

        return iMapTo.toDto(entity);
    }

    @Override
    public List<Beehive> getAll() {
        Iterable<BeehiveEntity> beegardensList = beehiveRepository.findAll();
        List<Beehive> output = new ArrayList<>();
        beegardensList.forEach(entity -> output.add(iMapTo.toDto(entity)));

        return output;
    }

    @Override
    public List<Beehive> findAllByBeegardenId(Integer id) {
        Iterable<BeehiveEntity> beegardensList = beehiveRepository.findAllByBeegardenId(id);
        List<Beehive> output = new ArrayList<>();
        beegardensList.forEach(entity -> output.add(iMapTo.toDto(entity)));

        return output;
    }

    @Override
    public List<Beehive> getUnassigned() {
        Iterable<BeehiveEntity> beegardensList = beehiveRepository.findAllByBeegardenIdIsNull();
        List<Beehive> output = new ArrayList<>();
        beegardensList.forEach(entity -> output.add(iMapTo.toDto(entity)));

        return output;
    }
}

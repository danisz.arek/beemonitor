package pl.polsl.infrastructure.adapters.beehive;

import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pl.polsl.domain.beehive.Beehive;
import pl.polsl.infrastructure.collection.IMapTo;

@Component
public class MapToBeehive implements IMapTo<Beehive, BeehiveEntity> {

    public BeehiveEntity toEntity(Beehive dto, @Nullable BeehiveEntity entity) {
        if (entity == null) {
            entity = new BeehiveEntity();
            entity.setId(dto.getId());
        }

        entity.setBeegardenId(dto.getBeegardenId());
        entity.setBeehiveName(dto.getBeehiveName());
        entity.setType(dto.getType());
        entity.setDateOfCreation(dto.getDateOfCreation());
        entity.setFramesAmount(dto.getFramesAmount());
        entity.setTrunksAmount(dto.getTrunksAmount());
        entity.setBottom(dto.getBottom());
        entity.setFeeder(dto.isFeeder());
        entity.setPoolenTrapper(dto.isPoolenTrapper());
        entity.setPropolisSinker(dto.isPropolisSinker());
        entity.setQueenCage(dto.isQueenCage());
        entity.setBarrier(dto.isBarrier());
        entity.setIsolator(dto.isIsolator());
        entity.setHeater(dto.isHeater());
        entity.setInletWidth(dto.getInletWidth());
        entity.setWorkFramesAmount(dto.getWorkFramesAmount());
        entity.setForce(dto.getForce());
        entity.setTemperament(dto.getTemperament());
        entity.setQueenCell(dto.isQueenCell());
        entity.setSwarmMood(dto.isSwarmMood());
        entity.setLackOfFood(dto.isLackOfFood());
        entity.setGrubs(dto.getGrubs());
        entity.setPropolis(dto.getPropolis());
        entity.setHoney(dto.getHoney());

        return entity;
    }

    @Override
    public Beehive toDto(@Nullable BeehiveEntity entity) {
        if (entity == null)
            return null;

        Beehive dto = new Beehive();

        dto.setId(entity.getId());
        dto.setBeegardenId(entity.getBeegardenId());
        dto.setBeehiveName(entity.getBeehiveName());
        dto.setType(entity.getType());
        dto.setDateOfCreation(entity.getDateOfCreation());
        dto.setFramesAmount(entity.getFramesAmount());
        dto.setTrunksAmount(entity.getTrunksAmount());
        dto.setBottom(entity.getBottom());
        dto.setFeeder(entity.isFeeder());
        dto.setPoolenTrapper(entity.isPoolenTrapper());
        dto.setPropolisSinker(entity.isPropolisSinker());
        dto.setQueenCage(entity.isQueenCage());
        dto.setBarrier(entity.isBarrier());
        dto.setIsolator(entity.isIsolator());
        dto.setHeater(entity.isHeater());
        dto.setInletWidth(entity.getInletWidth());
        dto.setWorkFramesAmount(entity.getWorkFramesAmount());
        dto.setForce(entity.getForce());
        dto.setTemperament(entity.getTemperament());
        dto.setQueenCell(entity.isQueenCell());
        dto.setSwarmMood(entity.isSwarmMood());
        dto.setLackOfFood(entity.isLackOfFood());
        dto.setGrubs(entity.getGrubs());
        dto.setPropolis(entity.getPropolis());
        dto.setHoney(entity.getHoney());

        return dto;
    }

}


package pl.polsl.infrastructure.adapters.beehive;

import org.springframework.data.repository.CrudRepository;
import pl.polsl.infrastructure.adapters.beegarden.BeegardenEntity;

import java.util.List;
import java.util.Optional;

public interface BeehiveRepository extends CrudRepository<BeehiveEntity, Integer> {
    Optional<BeehiveEntity> findById(Integer id);

    List<BeehiveEntity> findAllByBeegardenId(Integer userId);

    List<BeehiveEntity> findAllByBeegardenIdIsNull();
}

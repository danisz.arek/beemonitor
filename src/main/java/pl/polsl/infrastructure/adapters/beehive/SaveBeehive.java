package pl.polsl.infrastructure.adapters.beehive;

import org.springframework.stereotype.Component;

import pl.polsl.domain.beehive.Beehive;
import pl.polsl.domain.beehive.exception.NoSuchBeehive;
import pl.polsl.domain.beehive.service.ISaveBeehive;
import pl.polsl.domain.beequeen.BeeQueen;
import pl.polsl.domain.beequeen.service.IGetBeeQueen;
import pl.polsl.domain.beequeen.service.ISaveBeeQueen;
import pl.polsl.domain.beesensor.BeeSensor;
import pl.polsl.domain.beesensor.service.IGetBeeSensor;
import pl.polsl.domain.beesensor.service.ISaveBeeSensor;
import pl.polsl.domain.honeycollection.HoneyCollection;
import pl.polsl.domain.honeycollection.service.IGetHoneyCollection;
import pl.polsl.domain.honeycollection.service.ISaveHoneycollection;
import pl.polsl.domain.inspection.Inspection;
import pl.polsl.domain.inspection.service.IGetInspection;
import pl.polsl.domain.inspection.service.ISaveInspection;
import pl.polsl.infrastructure.collection.IMapTo;

import java.util.List;
import java.util.Optional;

@Component
public class SaveBeehive implements ISaveBeehive {

    private BeehiveRepository beehiveRepository;
    private IMapTo<Beehive, BeehiveEntity> iMapTo;
    private ISaveHoneycollection iSaveHoneycollection;
    private IGetHoneyCollection iGetHoneyCollection;
    private ISaveInspection iSaveInspection;
    private IGetInspection iGetInspection;
    private ISaveBeeSensor iSaveBeeSensor;
    private IGetBeeSensor iGetBeeSensor;
    private ISaveBeeQueen iSaveBeeQueen;
    private IGetBeeQueen iGetBeeQueen;


    public SaveBeehive(BeehiveRepository beehiveRepository, IMapTo<Beehive, BeehiveEntity> iMapTo,
                       ISaveHoneycollection iSaveHoneycollection, IGetHoneyCollection iGetHoneyCollection,
                       ISaveInspection iSaveInspection, IGetInspection iGetInspection,
                       ISaveBeeSensor iSaveBeeSensor, IGetBeeSensor iGetBeeSensor,
                       ISaveBeeQueen iSaveBeeQueen, IGetBeeQueen iGetBeeQueen) {

        this.beehiveRepository = beehiveRepository;
        this.iMapTo = iMapTo;
        this.iGetHoneyCollection = iGetHoneyCollection;
        this.iSaveHoneycollection = iSaveHoneycollection;
        this.iGetInspection = iGetInspection;
        this.iSaveInspection = iSaveInspection;
        this.iSaveBeeSensor = iSaveBeeSensor;
        this.iGetBeeSensor = iGetBeeSensor;
        this.iGetBeeQueen = iGetBeeQueen;
        this.iSaveBeeQueen = iSaveBeeQueen;
    }

    @Override
    public void save(Beehive beegarden) {
        beehiveRepository.save(iMapTo.toEntity(beegarden, null));
    }

    @Override
    public void update(Integer id, Beehive beegarden) throws NoSuchBeehive {
        Optional<BeehiveEntity> entity = this.beehiveRepository.findById(id);

        entity.orElseThrow(NoSuchBeehive::new);

        this.beehiveRepository.save(iMapTo.toEntity(beegarden, entity.get()));
    }

    @Override
    public void delete(Integer id) {

        List<HoneyCollection> honeyCollectionsToDelete = this.iGetHoneyCollection.findAllByBeehiveId(id);
        List<Inspection> inspectionsToDelete = this.iGetInspection.findAllByBeehiveId(id);
        List<BeeSensor> beeSensorsToDelete = this.iGetBeeSensor.findAllByBeehiveId(id);
        List<BeeQueen> beeQueensToDelete = this.iGetBeeQueen.findAllByBeehiveId(id);

        for (HoneyCollection honeyCollectionToDelete : honeyCollectionsToDelete)
            iSaveHoneycollection.delete(honeyCollectionToDelete.getId());

        for (Inspection inspectionToDelete : inspectionsToDelete)
            iSaveInspection.delete(inspectionToDelete.getId());

        for (BeeSensor beeSensorToDelete : beeSensorsToDelete)
            iSaveBeeSensor.delete(beeSensorToDelete.getId());

        for (BeeQueen beeQueenToDelete : beeQueensToDelete)
            iSaveBeeQueen.delete(beeQueenToDelete.getId());

        this.beehiveRepository.deleteById(id);
    }


}

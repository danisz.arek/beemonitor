package pl.polsl.infrastructure.adapters.beesensordata;

import org.springframework.stereotype.Component;
import pl.polsl.domain.beesensor.BeeSensor;
import pl.polsl.domain.beesensor.service.IGetBeeSensor;
import pl.polsl.domain.beesensor_data.BeeSensorData;
import pl.polsl.domain.beesensor_data.service.IGetBeeSensorData;
import pl.polsl.infrastructure.adapters.beesensor.BeeSensorEntity;
import pl.polsl.infrastructure.adapters.beesensor.BeeSensorRepository;
import pl.polsl.infrastructure.collection.IMapTo;

import java.util.ArrayList;
import java.util.List;

@Component
public class GetBeeSensorData implements IGetBeeSensorData {

    private final BeeSensorDataRepository beeSensorDataRepository;
    private final IMapTo<BeeSensorData, BeeSensorDataEntity> iMapTo;

    public GetBeeSensorData(BeeSensorDataRepository beeSensorDataRepository, IMapTo<BeeSensorData, BeeSensorDataEntity> iMapTo) {
        this.beeSensorDataRepository = beeSensorDataRepository;
        this.iMapTo = iMapTo;
    }


    @Override
    public BeeSensorData byId(Integer id) {
        BeeSensorDataEntity entity = this.beeSensorDataRepository.findById(id).orElse(null);

        if (null == entity) {
            return null;
        }

        return iMapTo.toDto(entity);
    }

    @Override
    public List<BeeSensorData> getAll() {
        Iterable<BeeSensorDataEntity> beeSensorDataEntities = beeSensorDataRepository.findAll();
        List<BeeSensorData> output = new ArrayList<>();
        beeSensorDataEntities.forEach(entity -> output.add(iMapTo.toDto(entity)));

        return output;
    }

    @Override
    public List<BeeSensorData> findAllByBeeSensorId(Integer id) {
        Iterable<BeeSensorDataEntity> beeSensorDataEntities = beeSensorDataRepository.findAllByBeeSensorId(id);
        List<BeeSensorData> output = new ArrayList<>();
        beeSensorDataEntities.forEach(entity -> output.add(iMapTo.toDto(entity)));

        return output;
    }

    @Override
    public List<BeeSensorData> findAllBeetweenDates (String dateFrom, String dateTo, Integer id) {
        Iterable<BeeSensorDataEntity> beeSensorDataEntities = beeSensorDataRepository.getAllBetweenDates(dateTo, dateFrom, id);
        List<BeeSensorData> output = new ArrayList<>();
        beeSensorDataEntities.forEach(entity -> output.add(iMapTo.toDto(entity)));

        return output;
    }

    @Override
    public List<BeeSensorData> findAllFromDate (String date, Integer id) {
        Iterable<BeeSensorDataEntity> beeSensorDataEntities = beeSensorDataRepository.getAllFromDate(date, id);
        List<BeeSensorData> output = new ArrayList<>();
        beeSensorDataEntities.forEach(entity -> output.add(iMapTo.toDto(entity)));

        return output;
    }


    @Override
    public List<BeeSensorData> getUnassigned() {
        Iterable<BeeSensorDataEntity> beeSensorDataEntities = beeSensorDataRepository.findAllByBeeSensorIdIsNull();
        List<BeeSensorData> output = new ArrayList<>();
        beeSensorDataEntities.forEach(entity -> output.add(iMapTo.toDto(entity)));

        return output;
    }
}

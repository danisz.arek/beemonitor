package pl.polsl.infrastructure.adapters.beesensordata;

import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pl.polsl.domain.beesensor_data.BeeSensorData;
import pl.polsl.infrastructure.adapters.beesensor.BeeSensorEntity;
import pl.polsl.infrastructure.adapters.beesensor.BeeSensorRepository;
import pl.polsl.infrastructure.collection.IMapTo;

@Component
public class MapToBeeSensorData implements IMapTo<BeeSensorData, BeeSensorDataEntity> {

    BeeSensorRepository beeSensorRepository;

    public MapToBeeSensorData(BeeSensorRepository beeSensorRepository) {
        this.beeSensorRepository = beeSensorRepository;
    }

    public BeeSensorDataEntity toEntity(BeeSensorData dto, @Nullable BeeSensorDataEntity entity) {

        BeeSensorEntity tmp = beeSensorRepository.findByPhoneNo(dto.getPhoneNo()).orElse(null);

        if (tmp == null)
            return null;

        if (entity == null) {
            entity = new BeeSensorDataEntity();
            entity.setId(dto.getId());
        }

        entity.setReadDate(dto.getReadDate());
        entity.setHumidityIn(dto.getHumidityIn());
        entity.setHumidityOut(dto.getHumidityOut());
        entity.setTemperatureIn(dto.getTemperatureIn());
        entity.setTemperatureOut(dto.getTemperatureOut());
        entity.setPhoneNo(dto.getPhoneNo());
        entity.setLatitude(dto.getLatitude());
        entity.setLogitude(dto.getLogitude());
        entity.setBeeSensorId(tmp.getId());

        return entity;
    }

    @Override
    public BeeSensorData toDto(@Nullable BeeSensorDataEntity entity) {

        if (entity == null)
            return null;

        BeeSensorData dto = new BeeSensorData();

        dto.setId(entity.getId());
        dto.setReadDate(entity.getReadDate());
        dto.setHumidityIn(entity.getHumidityIn());
        dto.setHumidityOut(entity.getHumidityOut());
        dto.setTemperatureIn(entity.getTemperatureIn());
        dto.setTemperatureOut(entity.getTemperatureOut());
        dto.setPhoneNo(entity.getPhoneNo());
        dto.setLatitude(entity.getLatitude());
        dto.setLogitude(entity.getLogitude());
        dto.setBeeSensorId(entity.getBeeSensorId());

        return dto;
    }
}

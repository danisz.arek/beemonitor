package pl.polsl.infrastructure.adapters.beesensordata;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import java.util.List;
import java.util.Optional;

public interface BeeSensorDataRepository extends CrudRepository<BeeSensorDataEntity, Integer> {
    Optional<BeeSensorDataEntity> findById(Integer id);

    List<BeeSensorDataEntity> findAllByBeeSensorId(Integer userId);

    List<BeeSensorDataEntity> findAllByBeeSensorIdIsNull();

    @Query(value = "select db from beesensor_data db where db.beeSensorId = :id and db.readDate BETWEEN :startDate AND :endDate")
    List<BeeSensorDataEntity> getAllBetweenDates(@Param("startDate") String startDate, @Param("endDate") String endDate, @Param("id") Integer id);

    @Query(value = "select db from beesensor_data db where db.beeSensorId = :id and db.readDate like :date%")
    List<BeeSensorDataEntity> getAllFromDate(@Param("date") String date, @Param("id") Integer id);
}

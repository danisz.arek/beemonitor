package pl.polsl.infrastructure.adapters.beesensordata;

import javax.persistence.*;

@Table
@Entity(name = "beesensor_data")
public class BeeSensorDataEntity {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "read_date",
            length = 30,
            nullable = false
    )
    private String readDate;

    @Column(name = "temperature_in",
            length = 10,
            nullable = false
    )
    private String temperatureIn;

    @Column(name = "humidity_in",
            length = 10,
            nullable = false
    )
    private String humidityIn;

    @Column(name = "temperature_out",
            length = 10,
            nullable = false
    )
    private String temperatureOut;

    @Column(name = "humidity_out",
            length = 10,
            nullable = false
    )
    private String humidityOut;

    @Column(name = "latitude",
            length = 20,
            nullable = false
    )
    private String latitude;

    @Column(name = "logitude",
            length = 20,
            nullable = false
    )
    private String logitude;

    @Column(name = "phone_no",
            length = 20,
            nullable = false
    )
    private String phoneNo;

    @Column(name = "beesensor_id")
    private Integer beeSensorId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getReadDate() {
        return readDate;
    }

    public void setReadDate(String readDate) {
        this.readDate = readDate;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLogitude() {
        return logitude;
    }

    public void setLogitude(String logitude) {
        this.logitude = logitude;
    }

    public Integer getBeeSensorId() {
        return beeSensorId;
    }

    public void setBeeSensorId(Integer beeSensorId) {
        this.beeSensorId = beeSensorId;
    }

    public String getTemperatureIn() {
        return temperatureIn;
    }

    public void setTemperatureIn(String temperatureIn) {
        this.temperatureIn = temperatureIn;
    }

    public String getHumidityIn() {
        return humidityIn;
    }

    public void setHumidityIn(String humidityIn) {
        this.humidityIn = humidityIn;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getTemperatureOut() {
        return temperatureOut;
    }

    public void setTemperatureOut(String temperatureOut) {
        this.temperatureOut = temperatureOut;
    }

    public String getHumidityOut() {
        return humidityOut;
    }

    public void setHumidityOut(String humidityOut) {
        this.humidityOut = humidityOut;
    }
}

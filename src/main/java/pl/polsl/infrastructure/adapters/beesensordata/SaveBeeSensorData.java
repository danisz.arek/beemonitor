package pl.polsl.infrastructure.adapters.beesensordata;

import org.springframework.stereotype.Component;
import pl.polsl.domain.beesensor.BeeSensor;
import pl.polsl.domain.beesensor.exception.NoSuchBeeSensor;
import pl.polsl.domain.beesensor.service.ISaveBeeSensor;
import pl.polsl.domain.beesensor_data.BeeSensorData;
import pl.polsl.domain.beesensor_data.exception.NoSuchBeeSensorData;
import pl.polsl.domain.beesensor_data.service.ISaveBeeSensorData;
import pl.polsl.infrastructure.adapters.beesensor.BeeSensorEntity;
import pl.polsl.infrastructure.adapters.beesensor.BeeSensorRepository;
import pl.polsl.infrastructure.collection.IMapTo;

import java.util.Optional;

@Component
public class SaveBeeSensorData implements ISaveBeeSensorData {

    private BeeSensorDataRepository beeSensorDataRepository;
    private IMapTo<BeeSensorData, BeeSensorDataEntity> iMapTo;

    public SaveBeeSensorData(BeeSensorDataRepository beeSensorDataRepository, IMapTo<BeeSensorData, BeeSensorDataEntity> iMapTo) {
        this.beeSensorDataRepository = beeSensorDataRepository;
        this.iMapTo = iMapTo;
    }

    @Override
    public void save(BeeSensorData beeSensorData) {
        beeSensorDataRepository.save(iMapTo.toEntity(beeSensorData, null));
    }

    @Override
    public void update(Integer id, BeeSensorData beeSensorData) throws NoSuchBeeSensorData {
        Optional<BeeSensorDataEntity> entity = this.beeSensorDataRepository.findById(id);

        entity.orElseThrow(NoSuchBeeSensorData::new);

        this.beeSensorDataRepository.save(iMapTo.toEntity(beeSensorData, entity.get()));
    }

    @Override
    public void delete(Integer id) {
        this.beeSensorDataRepository.deleteById(id);
    }


}

package pl.polsl.infrastructure.identity.service;

import org.springframework.stereotype.Service;
import pl.polsl.domain.identity.dto.Role;
import pl.polsl.domain.identity.service.IRoleService;
import pl.polsl.infrastructure.identity.entity.RoleEntity;
import pl.polsl.infrastructure.identity.repository.IRoleRepository;
import pl.polsl.infrastructure.identity.utility.RoleConverter;

import java.util.ArrayList;
import java.util.List;

@Service
public class RoleService implements IRoleService {

    private final IRoleRepository repository;
    private final RoleConverter roleConverter;

    public RoleService(IRoleRepository repository, RoleConverter roleConverter) {
        this.repository = repository;
        this.roleConverter = roleConverter;
    }

    public List<Role> getAll() {
        List<RoleEntity> roleEntities = repository.findAll();
        List<Role> roles = new ArrayList<>();
        for (RoleEntity role : roleEntities) {
            roles.add(roleConverter.entityToDto(role));
        }

        return roles;
    }


}

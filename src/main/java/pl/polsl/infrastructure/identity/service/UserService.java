package pl.polsl.infrastructure.identity.service;

import org.springframework.stereotype.Service;
import pl.polsl.domain.identity.command.*;
import pl.polsl.domain.identity.dto.Role;
import pl.polsl.domain.identity.dto.User;
import pl.polsl.domain.identity.service.IUserService;
import pl.polsl.domain.identity.valueObject.IResult;
import pl.polsl.domain.identity.valueObject.NotFound;
import pl.polsl.domain.identity.valueObject.RoleId;
import pl.polsl.domain.identity.valueObject.Success;
import pl.polsl.infrastructure.identity.encoder.IEncoder;
import pl.polsl.infrastructure.identity.entity.RoleEntity;
import pl.polsl.infrastructure.identity.entity.UserEntity;
import pl.polsl.infrastructure.identity.exception.UserNotFoundException;
import pl.polsl.infrastructure.identity.exception.ValidationException;
import pl.polsl.infrastructure.identity.repository.IRoleRepository;
import pl.polsl.infrastructure.identity.repository.IUserRepository;
import pl.polsl.infrastructure.identity.utility.UserConverter;
import pl.polsl.infrastructure.identity.validation.UserCreateValidator;
import pl.polsl.infrastructure.identity.validation.ValidationResult;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService implements IUserService {

    private final IEncoder encoder;
    private final IUserRepository userRepository;
    private final UserCreateValidator userCreateValidator;
    private final IRoleRepository roleRepository;
    private final UserConverter userConverter;

    public UserService(
            IEncoder encoder,
            IUserRepository userRepository,
            UserCreateValidator userCreateValidator,
            IRoleRepository roleRepository,
            UserConverter userConverter
    ) {
        this.encoder = encoder;
        this.userRepository = userRepository;
        this.userCreateValidator = userCreateValidator;
        this.roleRepository = roleRepository;
        this.userConverter = userConverter;
    }

    public void createUser(CreateUser command) throws ValidationException {

        String encodedPassword = encoder.encode(command.getPassword());
        UserEntity userEntity = new UserEntity(command.getUsername(), encodedPassword);

        ValidationResult validationResult = userCreateValidator.validate(userEntity);
        if (validationResult.getStatus() != 200) {
            throw new ValidationException(validationResult.getMessage());
        }

        RoleEntity baseRoleEntity = roleRepository.getById(1);
        userEntity.setRoles(new ArrayList<>(Collections.singletonList(baseRoleEntity)));

        userRepository.save(userEntity);
    }

    @Override
    public User byId(Integer id) {
        return userConverter.entityToDto(userRepository.getById(id));
    }

    public pl.polsl.domain.identity.dto.User findOneByUsername(String username) {
        return userConverter.entityToDto(userRepository.findOneByUsername(username));
    }

    @Override
    public List<User> getAll() {
        List<User> users = new ArrayList<>();
        for (UserEntity userEntity : userRepository.findAll())
            users.add(userConverter.entityToDto(userEntity));
        return users;
    }

    public IResult assignRoleToUser(AssignRoleToUser data) {
        List<String> messages = new ArrayList<>();

        UserEntity userEntity = userRepository.getById(data.getUserId().getId());
        if (userEntity == null) {
            messages.add("User with name: " + data.getUserId() + " was not found");
        }

        RoleEntity roleEntity = roleRepository.getById(data.getRoleId().getId());
        if (roleEntity == null) {
            messages.add("Role with id: " + data.getRoleId() + " was not found");
        }

        if (!messages.isEmpty()) {
            return new NotFound(String.join(". ", messages));
        }

        if (userEntity == null)
            return new NotFound("Failed");

        if (!userEntity.getRoles().contains(roleEntity)) {
            userEntity.getRoles().add(roleEntity);
            userRepository.save(userEntity);
            return new Success("Success");
        } else
            return new NotFound("User has already this role!");
    }

    @Override
    public void deleteUserRole(Integer userId, Integer roleId) throws UserNotFoundException {
        UserEntity userEntity = userRepository.getById(userId);
        if (userEntity == null)
            throw new UserNotFoundException();

        userEntity.deleteRoleById(roleId);
        userRepository.save(userEntity);
    }


    @Override
    public void deleteUser(DeleteUser command) throws UserNotFoundException {
        UserEntity userEntity = userRepository.getById(command.getUserId().getId());
        userEntity.setRoles(null);

        if (userEntity == null) {
            throw new UserNotFoundException();
        }

        userRepository.deleteById(command.getUserId().getId());
    }

    @Override
    public void update(UpdateUser command) throws UserNotFoundException {
        UserEntity userEntity = userRepository.getById(command.getUserId().getId());

        if (userEntity == null) {
            throw new UserNotFoundException();
        }
        userEntity.setUsername(command.getUsername());
        List<Integer> roleIds = command.getRoleIds().stream().map(RoleId::getId).collect(Collectors.toList());
        Iterable<RoleEntity> roleEntityList = roleRepository.findAllById(roleIds);
        List<RoleEntity> roles = new ArrayList<>();

        roleEntityList.forEach(roles::add);
        userEntity.setRoles(roles);

        userRepository.save(userEntity);
    }

    public void changeUserPassword(ChangePassword command) {
        UserEntity userEntity = this.userRepository.findOneByUsername(command.getUsername());
        userEntity.setPassword(encoder.encode(command.getNewPassword()));
        userRepository.save(userEntity);
    }

}

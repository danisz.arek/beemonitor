package pl.polsl.infrastructure.identity.service;

import org.springframework.stereotype.Service;
import pl.polsl.domain.identity.query.GetToken;
import pl.polsl.domain.identity.service.IAuthenticationService;
import pl.polsl.domain.identity.valueObject.AuthToken;

@Service
public class AuthenticationService implements IAuthenticationService {

    private final JWTAuthentication jwtAuthentication;

    public AuthenticationService(JWTAuthentication jwtAuthentication) {
        this.jwtAuthentication = jwtAuthentication;
    }

    public AuthToken getTokenForUser(GetToken query) {
        return new AuthToken(
                jwtAuthentication.generateToken(
                        query.getUsername().toString(),
                        query.getPassword().toString()
                )
        );
    }
}

package pl.polsl.infrastructure.identity.service;


import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.polsl.infrastructure.identity.entity.RoleEntity;
import pl.polsl.infrastructure.identity.entity.UserEntity;
import pl.polsl.infrastructure.identity.repository.IUserRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class UserDetailsLoader implements UserDetailsService {

    private final IUserRepository repository;

    public UserDetailsLoader(IUserRepository service) {
        this.repository = service;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity userEntity = repository.findOneByUsername(username);

        return new org.springframework.security.core.userdetails.User(userEntity.getUsername(), userEntity.getPassword(), getAuthorities(userEntity.getRoles()));
    }

    private Collection<? extends GrantedAuthority> getAuthorities(
            Collection<RoleEntity> roleEntities) {

        return getGrantedAuthorities(getPrivileges(roleEntities));
    }

    private List<String> getPrivileges(Collection<RoleEntity> roleEntities) {

        List<String> privileges = new ArrayList<>();
        List<RoleEntity> collection = new ArrayList<>(roleEntities);

        for (RoleEntity item : collection) {
            privileges.add(item.getName());
        }

        return privileges;
    }

    private List<GrantedAuthority> getGrantedAuthorities(List<String> privileges) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (String privilege : privileges) {
            authorities.add(new SimpleGrantedAuthority(privilege));
        }
        return authorities;
    }
}

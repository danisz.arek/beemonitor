package pl.polsl.infrastructure.identity.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import pl.polsl.infrastructure.adapters.beegarden.BeegardenEntity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Entity
@Table(name = "user")
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(length = 20, nullable = false)
    @NotBlank(message = "Username can not be empty")
    private String username;

    @Column(length = 100, nullable = false)
    @JsonIgnore
    @NotBlank(message = "Password can not be empty")
    private String password;


    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "user_roles", joinColumns = {
            @JoinColumn(name = "user_id")}, inverseJoinColumns = {
            @JoinColumn(name = "role_id")})
    private List<RoleEntity> roleEntities = new ArrayList<>();


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userId")
    private Set<BeegardenEntity> beegardenEntities;

    public Set<BeegardenEntity> getBeegardens() {
        return beegardenEntities;
    }

    public void setBeegardens(Set<BeegardenEntity> beegardenEntities) {
        this.beegardenEntities = beegardenEntities;
    }

    public UserEntity() {
    }

    public UserEntity(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<RoleEntity> getRoles() {
        return roleEntities;
    }

    public void setRoles(List<RoleEntity> roleEntities) {
        this.roleEntities = roleEntities;
    }

    public Optional<RoleEntity> getRoleById(Integer id) {
        return roleEntities.stream().filter(predicate -> predicate.getId().equals(id)).findFirst();
    }

    public void deleteRoleById(Integer id) {
        roleEntities.stream().filter(predicate -> predicate.getId().equals(id)).findFirst().ifPresent(roleEntity -> roleEntities.remove(roleEntity));
    }
}
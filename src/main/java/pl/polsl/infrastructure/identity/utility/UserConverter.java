package pl.polsl.infrastructure.identity.utility;

import org.springframework.stereotype.Component;
import pl.polsl.domain.identity.dto.Beegarden;
import pl.polsl.domain.identity.dto.Role;
import pl.polsl.domain.identity.dto.User;
import pl.polsl.domain.identity.valueObject.Password;
import pl.polsl.domain.identity.valueObject.UserId;
import pl.polsl.domain.identity.valueObject.Username;
import pl.polsl.infrastructure.adapters.beegarden.BeegardenEntity;
import pl.polsl.infrastructure.identity.entity.RoleEntity;
import pl.polsl.infrastructure.identity.entity.UserEntity;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserConverter {

    private final RoleConverter roleConverter;
    private final BeegardenConverter beegardenConverter;

    public UserConverter(RoleConverter roleConverter, BeegardenConverter beegardenConverter) {
        this.roleConverter = roleConverter;
        this.beegardenConverter = beegardenConverter;
    }

    public User entityToDto(UserEntity userEntity) {
        List<Role> roles = new ArrayList<>();
        List<Beegarden> beegardens = new ArrayList<>();

        for (BeegardenEntity beegarden : userEntity.getBeegardens()) {
            beegardens.add(beegardenConverter.entityToDto(beegarden));
        }

        for (RoleEntity role : userEntity.getRoles()) {
            roles.add(roleConverter.entityToDto(role));
        }

        return new User(
                new UserId(userEntity.getId()),
                new Username(userEntity.getUsername()),
                new Password(userEntity.getPassword()),
                roles,
                beegardens
        );
    }

    public UserEntity dtoToEntity(User user) {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(user.getId().getId());
        userEntity.setPassword(user.getPassword().toString());
        userEntity.setPassword(user.getUsername().toString());

        return userEntity;
    }
}

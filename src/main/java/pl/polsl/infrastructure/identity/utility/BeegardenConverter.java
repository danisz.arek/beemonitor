package pl.polsl.infrastructure.identity.utility;

import org.springframework.stereotype.Component;
import pl.polsl.domain.identity.dto.Beegarden;
import pl.polsl.domain.identity.valueObject.BeegardenId;
import pl.polsl.domain.identity.valueObject.BeegardenName;
import pl.polsl.infrastructure.adapters.beegarden.BeegardenEntity;


@Component
public class BeegardenConverter {

    public Beegarden entityToDto(BeegardenEntity entity) {
        return new Beegarden(
                new BeegardenId(entity.getId()),
                new BeegardenName(entity.getBeegardenName())
        );
    }

    public BeegardenEntity dtoToEntity(Beegarden beegarden) {
        BeegardenEntity beegardenEntity = new BeegardenEntity();
        beegardenEntity.setId(beegarden.getBeegardenId().getId());
        beegardenEntity.setBeegardenName(beegarden.getBeegardenName().toString());

        return beegardenEntity;
    }
}

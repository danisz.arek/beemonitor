package pl.polsl.infrastructure.identity.exception;

public class ValidationException extends Exception {
    public ValidationException(String message) {
        super(message);
    }
}

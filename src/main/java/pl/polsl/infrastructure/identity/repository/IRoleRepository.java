package pl.polsl.infrastructure.identity.repository;

import org.springframework.data.repository.CrudRepository;
import pl.polsl.infrastructure.identity.entity.RoleEntity;

import java.util.List;

public interface IRoleRepository extends CrudRepository<RoleEntity, Integer> {
    List<RoleEntity> findAll();

    RoleEntity getById(Integer id);

    RoleEntity getByName(String name);
}

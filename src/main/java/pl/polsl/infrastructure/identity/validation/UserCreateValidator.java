package pl.polsl.infrastructure.identity.validation;

import org.springframework.stereotype.Service;
import pl.polsl.infrastructure.identity.entity.UserEntity;
import pl.polsl.infrastructure.identity.validation.specifications.UniqueUsername;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserCreateValidator {

    private final UniqueUsername uniqueUsername;

    private List<String> messages = new ArrayList<>();

    public UserCreateValidator(UniqueUsername uniqueUsername) {
        this.uniqueUsername = uniqueUsername;
    }

    public ValidationResult validate(@Valid UserEntity userEntity) {
        if (!uniqueUsername.isValid(userEntity.getUsername())) {
            return new ValidationResult(409, "Already used username");
        }

        return new ValidationResult(200, "Success");
    }
}

package pl.polsl.infrastructure.identity.validation;

public class ValidationResult {

    private final Integer status;

    private final String message;

    public ValidationResult(Integer status, String message) {
        this.status = status;
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}

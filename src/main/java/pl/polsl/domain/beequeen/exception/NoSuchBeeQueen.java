package pl.polsl.domain.beequeen.exception;

public class NoSuchBeeQueen extends Exception {
    public NoSuchBeeQueen() {
        super("There is no such beequeen in given parameters");
    }
}

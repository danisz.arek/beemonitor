package pl.polsl.domain.beequeen.service;

import pl.polsl.domain.beequeen.BeeQueen;

import java.util.List;

public interface IGetBeeQueen {
    BeeQueen byId(Integer id);

    List<BeeQueen> getAll();

    List<BeeQueen> findAllByBeehiveId(Integer id);

    List<BeeQueen> getUnassigned();

    BeeQueen findByBeehiveId(Integer beehiveId);
}

package pl.polsl.domain.beequeen.service;

import pl.polsl.domain.beequeen.BeeQueen;
import pl.polsl.domain.beequeen.exception.NoSuchBeeQueen;

public interface ISaveBeeQueen {
    void save(BeeQueen beeQueen);

    void update(Integer id, BeeQueen beeQueen) throws NoSuchBeeQueen;

    void delete(Integer id);
}
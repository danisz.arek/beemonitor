package pl.polsl.domain.beequeen;

public class BeeQueen {

    private Integer id;
    private Integer queenNo;
    private String colour;
    private boolean fertile;
    private String variety;
    private String dateOfApplication;
    private Integer beehiveId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQueenNo() {
        return queenNo;
    }

    public void setQueenNo(Integer queenNo) {
        this.queenNo = queenNo;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public boolean isFertile() {
        return fertile;
    }

    public void setFertile(boolean fertile) {
        this.fertile = fertile;
    }

    public String getVariety() {
        return variety;
    }

    public void setVariety(String variety) {
        this.variety = variety;
    }

    public String getDateOfApplication() {
        return dateOfApplication;
    }

    public void setDateOfApplication(String dateOfApplication) {
        this.dateOfApplication = dateOfApplication;
    }

    public Integer getBeehiveId() {
        return beehiveId;
    }

    public void setBeehiveId(Integer beehiveId) {
        this.beehiveId = beehiveId;
    }
}

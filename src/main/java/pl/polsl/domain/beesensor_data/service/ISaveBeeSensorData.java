package pl.polsl.domain.beesensor_data.service;

import pl.polsl.domain.beesensor_data.BeeSensorData;
import pl.polsl.domain.beesensor_data.exception.NoSuchBeeSensorData;

public interface ISaveBeeSensorData {
    void save(BeeSensorData beeSensorData);

    void update(Integer id, BeeSensorData beeSensorData) throws NoSuchBeeSensorData;

    void delete(Integer id);
}
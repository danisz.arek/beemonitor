package pl.polsl.domain.beesensor_data.service;

import pl.polsl.domain.beesensor.BeeSensor;
import pl.polsl.domain.beesensor_data.BeeSensorData;

import java.util.List;

public interface IGetBeeSensorData {
    BeeSensorData byId(Integer id);

    List<BeeSensorData> getAll();

    List<BeeSensorData> findAllByBeeSensorId(Integer id);

    List<BeeSensorData> getUnassigned();

    List<BeeSensorData> findAllBeetweenDates (String dateFrom, String dateTo, Integer id);

    List<BeeSensorData> findAllFromDate (String date, Integer id);
}

package pl.polsl.domain.beesensor_data.exception;

public class NoSuchBeeSensorData extends Exception {
    public NoSuchBeeSensorData() {
        super("There is no such beesensor_data in given parameters");
    }
}

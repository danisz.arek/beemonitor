package pl.polsl.domain.beesensor_data;

public class BeeSensorData {

    private Integer id;
    private String readDate;
    private String temperatureIn;
    private String humidityIn;
    private String temperatureOut;
    private String humidityOut;
    private String latitude;
    private String logitude;
    private String phoneNo;
    private Integer beeSensorId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getReadDate() {
        return readDate;
    }

    public void setReadDate(String readDate) {
        this.readDate = readDate;
    }

    public String getTemperatureIn() {
        return temperatureIn;
    }

    public void setTemperatureIn(String temperatureIn) {
        this.temperatureIn = temperatureIn;
    }

    public String getHumidityIn() {
        return humidityIn;
    }

    public void setHumidityIn(String humidityIn) {
        this.humidityIn = humidityIn;
    }

    public String getTemperatureOut() {
        return temperatureOut;
    }

    public void setTemperatureOut(String temperatureOut) {
        this.temperatureOut = temperatureOut;
    }

    public String getHumidityOut() {
        return humidityOut;
    }

    public void setHumidityOut(String humidityOut) {
        this.humidityOut = humidityOut;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLogitude() {
        return logitude;
    }

    public void setLogitude(String logitude) {
        this.logitude = logitude;
    }

    public Integer getBeeSensorId() {
        return beeSensorId;
    }

    public void setBeeSensorId(Integer beeSensorId) {
        this.beeSensorId = beeSensorId;
    }
}

package pl.polsl.domain.identity.valueObject;

public interface IResult {
    Integer getResultCode();

    String getMessage();
}

package pl.polsl.domain.identity.valueObject;

public class Success implements IResult {

    private final String message;

    public Success(String message) {
        this.message = message;
    }

    @Override
    public Integer getResultCode() {
        return 200;
    }

    @Override
    public String getMessage() {
        return message;
    }
}

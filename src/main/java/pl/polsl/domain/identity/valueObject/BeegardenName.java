package pl.polsl.domain.identity.valueObject;

public class BeegardenName {

    private final String name;

    public BeegardenName(String name) {

        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }
}

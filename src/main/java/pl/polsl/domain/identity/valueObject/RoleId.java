package pl.polsl.domain.identity.valueObject;

public class RoleId {

    private final Integer id;

    public RoleId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }
}

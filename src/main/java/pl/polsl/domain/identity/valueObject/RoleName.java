package pl.polsl.domain.identity.valueObject;

public class RoleName {

    private final String name;

    public RoleName(String name) {

        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }
}

package pl.polsl.domain.identity.valueObject;

public class BeegardenId {

    private final Integer id;

    public BeegardenId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }
}

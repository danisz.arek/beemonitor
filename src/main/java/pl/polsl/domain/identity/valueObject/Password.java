package pl.polsl.domain.identity.valueObject;

public class Password {

    private final String password;

    public Password(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return password;
    }

    public String getPassword() {
        return password;
    }
}

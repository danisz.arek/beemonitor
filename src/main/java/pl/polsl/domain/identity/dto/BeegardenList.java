package pl.polsl.domain.identity.dto;

import pl.polsl.domain.beegarden.Beegarden;

import java.util.List;
import java.util.Optional;

public class BeegardenList {

    private List<Beegarden> beegardens;

    public Optional<Beegarden> byId(Integer id) {
        return beegardens.stream().filter(predicate -> predicate.getId().equals(id)).findFirst();
    }

    public List<Beegarden> getBeegardens() {
        return beegardens;
    }

    public void setBeegardens(List<Beegarden> beegardens) {
        this.beegardens = beegardens;
    }

}

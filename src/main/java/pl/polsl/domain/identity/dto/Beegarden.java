package pl.polsl.domain.identity.dto;

import pl.polsl.domain.identity.valueObject.BeegardenId;
import pl.polsl.domain.identity.valueObject.BeegardenName;

public class Beegarden {

    private final BeegardenId id;
    private final BeegardenName name;

    public Beegarden(BeegardenId id, BeegardenName name) {
        this.id = id;
        this.name = name;
    }

    public BeegardenId getBeegardenId() {
        return id;
    }

    public BeegardenName getBeegardenName() {
        return name;
    }
}

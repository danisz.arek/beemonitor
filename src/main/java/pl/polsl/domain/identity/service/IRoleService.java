package pl.polsl.domain.identity.service;

import pl.polsl.domain.identity.dto.Role;

import java.util.List;

public interface IRoleService {
    List<Role> getAll();
}

package pl.polsl.domain.identity.query;

import pl.polsl.domain.identity.valueObject.Password;
import pl.polsl.domain.identity.valueObject.Username;

public class GetToken {

    private Username username;

    private Password password;

    public GetToken(Username username, Password password) {
        this.password = password;
        this.username = username;
    }

    public Username getUsername() {
        return username;
    }

    public Password getPassword() {
        return password;
    }
}

package pl.polsl.domain.identity.command;

import pl.polsl.domain.identity.valueObject.RoleId;
import pl.polsl.domain.identity.valueObject.UserId;

public class AssignRoleToUser {

    private RoleId roleId;
    private UserId userId;

    public AssignRoleToUser(RoleId roleId, UserId userId) {
        this.roleId = roleId;
        this.userId = userId;
    }

    public RoleId getRoleId() {
        return roleId;
    }

    public UserId getUserId() {
        return userId;
    }
}

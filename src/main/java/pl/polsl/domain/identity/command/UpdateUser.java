package pl.polsl.domain.identity.command;

import pl.polsl.domain.beegarden.Beegarden;
import pl.polsl.domain.identity.valueObject.BeegardenId;
import pl.polsl.domain.identity.valueObject.RoleId;
import pl.polsl.domain.identity.valueObject.UserId;

import java.util.List;

public class UpdateUser {

    private final UserId userId;
    private final String username;
    private final List<RoleId> roleIds;
    private final List<BeegardenId> beegardenIds;

    public UpdateUser(UserId userId, String username, List<RoleId> roleIds, List<BeegardenId> beegardenIds) {
        this.userId = userId;
        this.username = username;
        this.roleIds = roleIds;
        this.beegardenIds = beegardenIds;
    }

    public String getUsername() {
        return username;
    }

    public List<RoleId> getRoleIds() {
        return roleIds;
    }

    public UserId getUserId() {
        return userId;
    }

    public List<BeegardenId> getBeegardenIds() {
        return beegardenIds;
    }
}

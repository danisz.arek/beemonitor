package pl.polsl.domain.identity.command;

public class ChangePassword {
    private String username;
    private String newPassword;

    public ChangePassword(String username, String newPassword) {
        this.username = username;
        this.newPassword = newPassword;
    }

    public String getUsername() {
        return username;
    }

    public String getNewPassword() {
        return newPassword;
    }
}

package pl.polsl.domain.identity.exception;

public class BeegardenIsInUse extends Exception {
    public BeegardenIsInUse(Throwable cause) {
        super("Beegarden is assigned to a room or stil has items assigned.", cause);
    }
}

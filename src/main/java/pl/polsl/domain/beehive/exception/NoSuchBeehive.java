package pl.polsl.domain.beehive.exception;

public class NoSuchBeehive extends Exception {
    public NoSuchBeehive() {
        super("There is no such Beehive in given parameters");
    }
}

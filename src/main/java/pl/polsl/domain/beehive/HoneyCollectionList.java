package pl.polsl.domain.beehive;

import pl.polsl.domain.honeycollection.HoneyCollection;

import java.util.List;
import java.util.Optional;

public class HoneyCollectionList {

    private List<HoneyCollection> honeyCollections;

    public Optional<HoneyCollection> byId(Integer id) {
        return honeyCollections.stream().filter(predicate -> predicate.getId().equals(id)).findFirst();
    }

    public List<HoneyCollection> getHoneyCollectionList() {
        return honeyCollections;
    }

    public void setHoneyCollectionList(List<HoneyCollection> honeyCollections) {
        this.honeyCollections = honeyCollections;
    }


}

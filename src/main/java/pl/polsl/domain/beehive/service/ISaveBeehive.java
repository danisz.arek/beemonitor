package pl.polsl.domain.beehive.service;

import pl.polsl.domain.beegarden.Beegarden;
import pl.polsl.domain.beegarden.exception.NoSuchBeegarden;
import pl.polsl.domain.beehive.Beehive;
import pl.polsl.domain.beehive.exception.NoSuchBeehive;

public interface ISaveBeehive {
    void save(Beehive beehive);

    void update(Integer id, Beehive beehive) throws NoSuchBeehive;

    void delete(Integer id);
}
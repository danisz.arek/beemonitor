package pl.polsl.domain.beehive.service;

import pl.polsl.domain.beegarden.Beegarden;
import pl.polsl.domain.beehive.Beehive;

import java.util.List;

public interface IGetBeehive {
    Beehive byId(Integer id);

    List<Beehive> getAll();

    List<Beehive> findAllByBeegardenId(Integer id);

    List<Beehive> getUnassigned();
}

package pl.polsl.domain.beehive;

public class Beehive {

    private Integer id;
    private Integer beegardenId;
    private String beehiveName;
    private String type;
    private String dateOfCreation;
    private Integer framesAmount;
    private Integer trunksAmount;
    private String bottom;
    private boolean feeder;
    private boolean poolenTrapper;
    private boolean propolisSinker;
    private boolean queenCage;
    private boolean barrier;
    private boolean isolator;
    private boolean heater;
    private Integer inletWidth;
    private Integer workFramesAmount;
    private String force;
    private String temperament;
    private boolean queenCell;
    private boolean swarmMood;
    private boolean lackOfFood;
    private Integer grubs;
    private Integer propolis;
    private Integer honey;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBeegardenId() {
        return beegardenId;
    }

    public void setBeegardenId(Integer beegardenId) {
        this.beegardenId = beegardenId;
    }

    public String getBeehiveName() {
        return beehiveName;
    }

    public void setBeehiveName(String beehiveName) {
        this.beehiveName = beehiveName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(String dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public Integer getFramesAmount() {
        return framesAmount;
    }

    public void setFramesAmount(Integer framesAmount) {
        this.framesAmount = framesAmount;
    }

    public Integer getTrunksAmount() {
        return trunksAmount;
    }

    public void setTrunksAmount(Integer trunksAmount) {
        this.trunksAmount = trunksAmount;
    }

    public String getBottom() {
        return bottom;
    }

    public void setBottom(String bottom) {
        this.bottom = bottom;
    }

    public boolean isFeeder() {
        return feeder;
    }

    public void setFeeder(boolean feeder) {
        this.feeder = feeder;
    }

    public boolean isPoolenTrapper() {
        return poolenTrapper;
    }

    public void setPoolenTrapper(boolean poolenTrapper) {
        this.poolenTrapper = poolenTrapper;
    }

    public boolean isPropolisSinker() {
        return propolisSinker;
    }

    public void setPropolisSinker(boolean propolisSinker) {
        this.propolisSinker = propolisSinker;
    }

    public boolean isQueenCage() {
        return queenCage;
    }

    public void setQueenCage(boolean queenCage) {
        this.queenCage = queenCage;
    }

    public boolean isBarrier() {
        return barrier;
    }

    public void setBarrier(boolean barrier) {
        this.barrier = barrier;
    }

    public boolean isIsolator() {
        return isolator;
    }

    public void setIsolator(boolean isolator) {
        this.isolator = isolator;
    }

    public boolean isHeater() {
        return heater;
    }

    public void setHeater(boolean heater) {
        this.heater = heater;
    }

    public Integer getInletWidth() {
        return inletWidth;
    }

    public void setInletWidth(Integer inletWidth) {
        this.inletWidth = inletWidth;
    }

    public Integer getWorkFramesAmount() {
        return workFramesAmount;
    }

    public void setWorkFramesAmount(Integer workFramesAmount) {
        this.workFramesAmount = workFramesAmount;
    }

    public String getForce() {
        return force;
    }

    public void setForce(String force) {
        this.force = force;
    }

    public String getTemperament() {
        return temperament;
    }

    public void setTemperament(String temperament) {
        this.temperament = temperament;
    }

    public boolean isQueenCell() {
        return queenCell;
    }

    public void setQueenCell(boolean queenCell) {
        this.queenCell = queenCell;
    }

    public boolean isSwarmMood() {
        return swarmMood;
    }

    public void setSwarmMood(boolean swarmMood) {
        this.swarmMood = swarmMood;
    }

    public boolean isLackOfFood() {
        return lackOfFood;
    }

    public void setLackOfFood(boolean lackOfFood) {
        this.lackOfFood = lackOfFood;
    }

    public Integer getGrubs() {
        return grubs;
    }

    public void setGrubs(Integer grubs) {
        this.grubs = grubs;
    }

    public Integer getPropolis() {
        return propolis;
    }

    public void setPropolis(Integer propolis) {
        this.propolis = propolis;
    }

    public Integer getHoney() {
        return honey;
    }

    public void setHoney(Integer honey) {
        this.honey = honey;
    }
}

package pl.polsl.domain.honeycollection.service;

import pl.polsl.domain.beehive.Beehive;
import pl.polsl.domain.honeycollection.HoneyCollection;
import pl.polsl.domain.honeycollection.exception.NoSuchHoneycollection;

public interface ISaveHoneycollection {
    void save(HoneyCollection honeyCollection);

    void update(Integer id, HoneyCollection honeyCollection) throws NoSuchHoneycollection;

    void delete(Integer id);
}
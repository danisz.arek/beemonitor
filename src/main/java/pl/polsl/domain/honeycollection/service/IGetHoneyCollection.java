package pl.polsl.domain.honeycollection.service;

import pl.polsl.domain.beehive.Beehive;
import pl.polsl.domain.honeycollection.HoneyCollection;

import java.util.List;

public interface IGetHoneyCollection {
    HoneyCollection byId(Integer id);

    List<HoneyCollection> getAll();

    List<HoneyCollection> findAllByBeehiveId(Integer id);

    List<HoneyCollection> getUnassigned();

    HoneyCollection findByBeehiveId(Integer beehiveId);
}

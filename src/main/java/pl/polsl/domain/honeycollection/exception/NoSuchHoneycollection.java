package pl.polsl.domain.honeycollection.exception;

public class NoSuchHoneycollection extends Exception {
    public NoSuchHoneycollection() {
        super("There is no such honeycollection in given parameters");
    }
}

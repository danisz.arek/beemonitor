package pl.polsl.domain.honeycollection;

public class HoneyCollection {

    private Integer id;
    private String collectionDate;
    private String honeyType;
    private Integer amount;
    private Integer beehiveId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCollectionDate() {
        return collectionDate;
    }

    public void setCollectionDate(String collectionDate) {
        this.collectionDate = collectionDate;
    }

    public String getHoneyType() {
        return honeyType;
    }

    public void setHoneyType(String honeyType) {
        this.honeyType = honeyType;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getBeehiveId() {
        return beehiveId;
    }

    public void setBeehiveId(Integer beehiveId) {
        this.beehiveId = beehiveId;
    }
}

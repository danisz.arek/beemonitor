package pl.polsl.domain.inspection.exception;

public class NoSuchInspection extends Exception {
    public NoSuchInspection() {
        super("There is no such inspection in given parameters");
    }
}

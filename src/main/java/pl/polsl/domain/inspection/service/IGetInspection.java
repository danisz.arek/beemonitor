package pl.polsl.domain.inspection.service;

import pl.polsl.domain.honeycollection.HoneyCollection;
import pl.polsl.domain.inspection.Inspection;

import java.util.List;

public interface IGetInspection {
    Inspection byId(Integer id);

    List<Inspection> getAll();

    List<Inspection> findAllByBeehiveId(Integer id);

    List<Inspection> getUnassigned();

    Inspection findByBeehiveId(Integer beehiveId);
}

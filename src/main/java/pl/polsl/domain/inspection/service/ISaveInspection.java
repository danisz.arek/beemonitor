package pl.polsl.domain.inspection.service;

import pl.polsl.domain.inspection.Inspection;
import pl.polsl.domain.inspection.exception.NoSuchInspection;

public interface ISaveInspection {
    void save(Inspection inspection);

    void update(Integer id, Inspection inspection) throws NoSuchInspection;

    void delete(Integer id);
}
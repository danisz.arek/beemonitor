package pl.polsl.domain.beegarden.exception;

public class NoSuchBeegarden extends Exception {
    public NoSuchBeegarden() {
        super("There is no such Beegarden in given parameters");
    }
}

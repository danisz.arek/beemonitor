package pl.polsl.domain.beegarden.services;

import pl.polsl.domain.beegarden.Beegarden;

import java.util.List;

public interface IGetBeegarden {
    Beegarden byId(Integer id);

    List<Beegarden> getAll();

    List<Beegarden> findAllByUserId(Integer id);

    List<Beegarden> getUnassigned();
}

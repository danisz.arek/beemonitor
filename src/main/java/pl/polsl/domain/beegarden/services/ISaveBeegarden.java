package pl.polsl.domain.beegarden.services;

import pl.polsl.domain.beegarden.Beegarden;
import pl.polsl.domain.beegarden.exception.NoSuchBeegarden;


public interface ISaveBeegarden {
    void save(Beegarden beegarden);

    void update(Integer id, Beegarden beegarden) throws NoSuchBeegarden;

    void delete(Integer id);
}
package pl.polsl.domain.beegarden;


public class Beegarden {

    private Integer id;
    private String beegardenName;
    private String location;
    private String type;
    private Integer userId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBeegardenName() {
        return beegardenName;
    }

    public void setBeegardenName(String beegardenName) {
        this.beegardenName = beegardenName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

}

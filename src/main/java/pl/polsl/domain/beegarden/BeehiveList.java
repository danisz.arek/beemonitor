package pl.polsl.domain.beegarden;

import pl.polsl.domain.beehive.Beehive;

import java.util.List;
import java.util.Optional;

public class BeehiveList {


    private List<Beehive> beehiveList;

    public Optional<Beehive> byId(Integer id) {
        return beehiveList.stream().filter(predicate -> predicate.getId().equals(id)).findFirst();
    }

    public List<Beehive> getBeehiveList() {
        return beehiveList;
    }

    public void setBeehiveList(List<Beehive> beehiveList) {
        this.beehiveList = beehiveList;
    }


}

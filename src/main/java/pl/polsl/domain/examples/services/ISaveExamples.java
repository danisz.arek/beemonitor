package pl.polsl.domain.examples.services;

import pl.polsl.domain.examples.Example;
import pl.polsl.domain.examples.exception.NoSuchExample;

public interface ISaveExamples {
    void save(Example example);

    void update(Integer id, Example example) throws NoSuchExample;

    void delete(Integer id);
}

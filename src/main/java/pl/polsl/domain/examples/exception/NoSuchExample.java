package pl.polsl.domain.examples.exception;

public class NoSuchExample extends Exception {
    public NoSuchExample() {
        super("There is no such Example in given parameters");
    }
}

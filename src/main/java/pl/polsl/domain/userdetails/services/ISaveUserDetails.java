package pl.polsl.domain.userdetails.services;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import pl.polsl.domain.userdetails.UserDetails;
import pl.polsl.domain.userdetails.exception.NoSuchDetail;


public interface ISaveUserDetails {
    ResponseEntity<HttpStatus> save(UserDetails userDetails);

    void updateByUser(Integer id, UserDetails userDetails) throws NoSuchDetail;

    void delete(Integer id);

    void deleteByUser(Integer userId);
}
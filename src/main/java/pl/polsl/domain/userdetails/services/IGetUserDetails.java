package pl.polsl.domain.userdetails.services;

import pl.polsl.domain.userdetails.UserDetails;
import java.util.List;

public interface IGetUserDetails {
    UserDetails byId(Integer id);

    List<UserDetails> getAll();

    UserDetails findByUserEntityId(Integer userId);

    List<UserDetails> findAllByUserEntityId(Integer userId);
}

package pl.polsl.domain.userdetails;

import pl.polsl.domain.identity.dto.User;
import pl.polsl.infrastructure.identity.entity.UserEntity;

public class UserDetails {

    private Integer id;
    private String beekeperName;
    private String beekeperSurname;
    private String email;
    private String postCode;
    private String city;
    private String road;
    private String roadNr;
    private User user;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBeekeperName() {
        return beekeperName;
    }

    public void setBeekeperName(String beekeperName) {
        this.beekeperName = beekeperName;
    }

    public String getBeekeperSurname() {
        return beekeperSurname;
    }

    public void setBeekeperSurname(String beekeperSurname) {
        this.beekeperSurname = beekeperSurname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRoad() {
        return road;
    }

    public void setRoad(String road) {
        this.road = road;
    }

    public String getRoadNr() {
        return roadNr;
    }

    public void setRoadNr(String roadNr) {
        this.roadNr = roadNr;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}

package pl.polsl.domain.userdetails.exception;

public class NoSuchDetail extends Exception {
    public NoSuchDetail() {
        super("There is no such Detail in given parameters");
    }
}

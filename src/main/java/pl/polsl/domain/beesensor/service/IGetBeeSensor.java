package pl.polsl.domain.beesensor.service;

import pl.polsl.domain.beesensor.BeeSensor;

import java.util.List;

public interface IGetBeeSensor {
    BeeSensor byId(Integer id);

    List<BeeSensor> getAll();

    List<BeeSensor> findAllByBeehiveId(Integer id);

    BeeSensor findByBeehiveId(Integer beehiveId);

    BeeSensor findByPhoneNo(String phoneNo);
}

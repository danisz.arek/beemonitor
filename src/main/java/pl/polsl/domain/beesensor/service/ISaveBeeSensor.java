package pl.polsl.domain.beesensor.service;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import pl.polsl.domain.beesensor.BeeSensor;
import pl.polsl.domain.beesensor.exception.NoSuchBeeSensor;

public interface ISaveBeeSensor {

    ResponseEntity<HttpStatus> save(BeeSensor beeSensor);

    void update(Integer id, BeeSensor beeSensor) throws NoSuchBeeSensor;

    void delete(Integer id);
}
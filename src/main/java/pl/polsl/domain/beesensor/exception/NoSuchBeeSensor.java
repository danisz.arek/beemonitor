package pl.polsl.domain.beesensor.exception;

public class NoSuchBeeSensor extends Exception {
    public NoSuchBeeSensor() {
        super("There is no such beesensor in given parameters");
    }
}

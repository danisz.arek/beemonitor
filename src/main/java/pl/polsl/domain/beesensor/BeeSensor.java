package pl.polsl.domain.beesensor;

import pl.polsl.domain.beehive.Beehive;
import pl.polsl.infrastructure.adapters.beehive.BeehiveEntity;

public class BeeSensor {

    private Integer id;
    private String phoneNo;
    private Beehive beehive;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public Beehive getBeehive() {
        return beehive;
    }

    public void setBeehive(Beehive beehive) {
        this.beehive = beehive;
    }
}

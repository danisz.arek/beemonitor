package pl.polsl.application.beehive.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.polsl.domain.beegarden.Beegarden;
import pl.polsl.domain.beegarden.services.IGetBeegarden;
import pl.polsl.domain.beehive.Beehive;
import pl.polsl.domain.beehive.exception.NoSuchBeehive;
import pl.polsl.domain.beehive.service.IGetBeehive;
import pl.polsl.domain.beehive.service.ISaveBeehive;

import java.util.ArrayList;
import java.util.List;

@RestController
public class BeehiveController {

    private final IGetBeehive iGetBeehive;
    private final ISaveBeehive iSaveBeehive;
    private final IGetBeegarden iGetBeegarden;


    public BeehiveController(IGetBeehive iGetBeehive, ISaveBeehive iSaveBeehive, IGetBeegarden iGetBeegarden) {
        this.iGetBeehive = iGetBeehive;
        this.iSaveBeehive = iSaveBeehive;
        this.iGetBeegarden = iGetBeegarden;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/beehives")
    public ResponseEntity<List<Beehive>> get() {
        List<Beehive> list = iGetBeehive.getAll();

        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/beehives/unassigned")
    public ResponseEntity<List<Beehive>> getUnassigned() {
        List<Beehive> list = iGetBeehive.getUnassigned();

        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/beehive/{id}")
    public ResponseEntity<HttpStatus> update(@PathVariable Integer id, @RequestBody Beehive beehive) {
        try {
            iSaveBeehive.update(id, beehive);
        } catch (NoSuchBeehive noSuchExample) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/beehive/{beehiveId}")
    public ResponseEntity<Beehive> getSpecificBeehive (@PathVariable Integer beehiveId) {
            Beehive result = iGetBeehive.byId(beehiveId);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/countbeehives/{userId}")
    public ResponseEntity<Integer> countBeehivesForUser (@PathVariable Integer userId) {

        List <Beegarden> listOfBeegardens = iGetBeegarden.findAllByUserId(userId);
        List<Beehive> result = new ArrayList<>();

        for (Beegarden beegarden : listOfBeegardens)
            result.addAll(iGetBeehive.findAllByBeegardenId(beegarden.getId()));

        return new ResponseEntity<>(result.size(), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/beehivesfrombeegardens/{beegardenIds}")
    public ResponseEntity<List<Beehive>> getSpecificBeehive (@PathVariable ArrayList<Integer> beegardenIds) {

        List<Beehive> result = new ArrayList<>();
        for (Integer x : beegardenIds)
            result.addAll(iGetBeehive.findAllByBeegardenId(x));

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/beehives")
    ResponseEntity<HttpStatus> add(@RequestBody Beehive beehive) {
        iSaveBeehive.save(beehive);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/beehive/{beehiveId}")
    public ResponseEntity<HttpStatus> delete(@PathVariable Integer beehiveId) {

        iSaveBeehive.delete(beehiveId);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/beehives/{beegardeId}")
    public ResponseEntity<List<Beehive>> getBeehivesFromWholeBeegarden (@PathVariable Integer beegardeId) {
        List<Beehive> list = iGetBeehive.findAllByBeegardenId(beegardeId);

        return new ResponseEntity<>(list, HttpStatus.OK);
    }


}

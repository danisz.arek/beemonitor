package pl.polsl.application.beesensor_data.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.polsl.application.notification.IncomingDataAnalyser;
import pl.polsl.application.notification.LocationAnalyser;
import pl.polsl.domain.beegarden.services.IGetBeegarden;
import pl.polsl.domain.beesensor.service.IGetBeeSensor;
import pl.polsl.domain.beesensor_data.BeeSensorData;
import pl.polsl.domain.beesensor_data.exception.NoSuchBeeSensorData;
import pl.polsl.domain.beesensor_data.service.IGetBeeSensorData;
import pl.polsl.domain.beesensor_data.service.ISaveBeeSensorData;
import pl.polsl.domain.userdetails.services.IGetUserDetails;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@RestController
public class BeeSensorDataController {

    private final IGetBeeSensorData iGetBeeSensorData;
    private final ISaveBeeSensorData iSaveBeeSensorData;
    private final IGetBeeSensor iGetBeeSensor;
    private final IGetBeegarden iGetBeegarden;
    private final IGetUserDetails iGetUserDetails;

    public BeeSensorDataController(IGetBeeSensorData iGetBeeSensorData, ISaveBeeSensorData iSaveBeeSensorData,
                                   IGetBeeSensor iGetBeeSensor, IGetBeegarden iGetBeegarden,
                                   IGetUserDetails iGetUserDetails) {
        this.iGetBeeSensorData = iGetBeeSensorData;
        this.iSaveBeeSensorData = iSaveBeeSensorData;
        this.iGetBeeSensor = iGetBeeSensor;
        this.iGetBeegarden = iGetBeegarden;
        this.iGetUserDetails = iGetUserDetails;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/beesensordatas")
    public ResponseEntity<List<BeeSensorData>> get() {
        List<BeeSensorData> list = iGetBeeSensorData.getAll();

        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/beesensordatas/unassigned")
    public ResponseEntity<List<BeeSensorData>> getUnassigned() {
        List<BeeSensorData> list = iGetBeeSensorData.getUnassigned();

        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/beesensordata/{id}")
    public ResponseEntity<HttpStatus> update(@PathVariable Integer id, @RequestBody BeeSensorData beeSensorData) {
        try {
            iSaveBeeSensorData.update(id, beeSensorData);
        } catch (NoSuchBeeSensorData noSuchExample) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/beesensordata/{id}")
    public ResponseEntity<BeeSensorData> getSpecificBeeSensorData(@PathVariable Integer id) {
        BeeSensorData result = iGetBeeSensorData.byId(id);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/beesensordatas")
    ResponseEntity<HttpStatus> add(@RequestBody BeeSensorData beeSensorData) {
        iSaveBeeSensorData.save(beeSensorData);

        new IncomingDataAnalyser(iGetBeeSensor, iGetBeegarden, iGetUserDetails)
                .analyseTempAndHumid(beeSensorData);
        new LocationAnalyser(iGetBeeSensor, iGetBeegarden, iGetBeeSensorData, iGetUserDetails)
                .analyseLocation(beeSensorData);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/beesensordata/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable Integer id) {
        iSaveBeeSensorData.delete(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/checkrobbery/{id}")
    public ResponseEntity<Boolean> checkrobbery(@PathVariable Integer id) {

        List<BeeSensorData> list = iGetBeeSensorData.findAllByBeeSensorId(id);
        float tmpLatitude = 0;
        float result;
        boolean robbery = false;

        if (list.size() > 0)
            tmpLatitude = Float.parseFloat(list.get(0).getLatitude());

        for (BeeSensorData beeSensorData : list) {

            result = tmpLatitude - Float.parseFloat(beeSensorData.getLatitude());

            if (result > 0.001) {
                robbery = true;
                break;
            }

            tmpLatitude = Float.parseFloat(beeSensorData.getLatitude());
        }
        return new ResponseEntity<>(robbery, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/actualbeesensordata/{id}")
    public ResponseEntity<List<BeeSensorData>> getActualBeesensorData(@PathVariable Integer id) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime now = LocalDateTime.now();

        List<BeeSensorData> resultList = iGetBeeSensorData.findAllFromDate(dtf.format(now), id);

        return new ResponseEntity<>(resultList, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/thismonthbeesensordata/{id}")
    public ResponseEntity<List<BeeSensorData>> getBeesensorDataFromThisMonth(@PathVariable Integer id) {

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime now = LocalDateTime.now();

        List<BeeSensorData> list = iGetBeeSensorData.findAllBeetweenDates(
                dtf.format(now),
                dtf.format(now.minusMonths(1)),
                id);

        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/thisweekbeesensordata/{id}")
    public ResponseEntity<List<BeeSensorData>> getBeesensorDataFromThisWeek(@PathVariable Integer id) {

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime now = LocalDateTime.now();

        List<BeeSensorData> list = iGetBeeSensorData.findAllBeetweenDates(dtf.format(now), dtf.format(now.minusWeeks(1)), id);

        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/thisyearbeesensordata/{id}")
    public ResponseEntity<List<BeeSensorData>> getBeesensorDataFromThisYear(@PathVariable Integer id) {
        List<BeeSensorData> list = iGetBeeSensorData.findAllByBeeSensorId(id);
        List<BeeSensorData> resultList = new ArrayList<>();

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy");
        LocalDateTime now = LocalDateTime.now();

        for (BeeSensorData beeSensorData : list)
            if (beeSensorData.getReadDate().contains(dtf.format(now)))
                resultList.add(beeSensorData);

        return new ResponseEntity<>(resultList, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/beesensordatas/{id}")
    public ResponseEntity<List<BeeSensorData>> getBeeSensorDatasByBeehive(@PathVariable Integer id) {
        List<BeeSensorData> list = iGetBeeSensorData.findAllByBeeSensorId(id);

        return new ResponseEntity<>(list, HttpStatus.OK);
    }
}

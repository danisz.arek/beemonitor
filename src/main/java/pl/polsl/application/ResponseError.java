package pl.polsl.application;

public class ResponseError {
    public String error;

    private ResponseError(String error) {
        this.error = error;
    }

    public static ResponseError fromException(Exception exception) {
        return new ResponseError(exception.getMessage());
    }
}

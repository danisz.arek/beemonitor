package pl.polsl.application.beegarden.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.polsl.domain.beegarden.Beegarden;
import pl.polsl.domain.beegarden.exception.NoSuchBeegarden;
import pl.polsl.domain.beegarden.services.IGetBeegarden;
import pl.polsl.domain.beegarden.services.ISaveBeegarden;
import java.util.List;

@RestController
public class BeegardenController {

    private final IGetBeegarden iGetBeegarden;
    private final ISaveBeegarden iSaveBeegarden;

    public BeegardenController(IGetBeegarden iGetBeegarden, ISaveBeegarden iSaveBeegarden) {
        this.iGetBeegarden = iGetBeegarden;
        this.iSaveBeegarden = iSaveBeegarden;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/beegardens")
    public ResponseEntity<List<Beegarden>> getAllBeegardens () {
        List<Beegarden> list = iGetBeegarden.getAll();

        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/beegardens/unassigned")
    public ResponseEntity<List<Beegarden>> getUnassigned() {
        List<Beegarden> list = iGetBeegarden.getUnassigned();

        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/beegarden/{beegardenId}")
    public ResponseEntity<Beegarden> getById (@PathVariable Integer beegardenId) {
        Beegarden response = iGetBeegarden.byId(beegardenId);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/beegardens/{userId}")
    public ResponseEntity<List<Beegarden>> getBeegardenFromUser(@PathVariable Integer userId) {
        List<Beegarden> list = iGetBeegarden.findAllByUserId(userId);

        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/countuserbeegardens/{userId}")
    public ResponseEntity<Integer> countUserBeegardens(@PathVariable Integer userId) {
        Integer result = iGetBeegarden.findAllByUserId(userId).size();

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/beegarden/{id}")
    public ResponseEntity<HttpStatus> update(@PathVariable Integer id, @RequestBody Beegarden beegarden) {
        try {
            iSaveBeegarden.update(id, beegarden);
        } catch (NoSuchBeegarden noSuchExample) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/beegardens")
    ResponseEntity<HttpStatus> add(@RequestBody Beegarden beegarden) {
        iSaveBeegarden.save(beegarden);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/beegarden/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable Integer id) {
        iSaveBeegarden.delete(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

}

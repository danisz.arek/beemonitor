package pl.polsl.application.inspection.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.polsl.domain.honeycollection.HoneyCollection;
import pl.polsl.domain.honeycollection.exception.NoSuchHoneycollection;
import pl.polsl.domain.honeycollection.service.IGetHoneyCollection;
import pl.polsl.domain.honeycollection.service.ISaveHoneycollection;
import pl.polsl.domain.inspection.Inspection;
import pl.polsl.domain.inspection.exception.NoSuchInspection;
import pl.polsl.domain.inspection.service.IGetInspection;
import pl.polsl.domain.inspection.service.ISaveInspection;

import java.util.List;

@RestController
public class InspectionController {

    private final IGetInspection iGetInspection;
    private final ISaveInspection iSaveInspection;

    public InspectionController(IGetInspection iGetInspection, ISaveInspection iSaveInspection) {
        this.iGetInspection = iGetInspection;
        this.iSaveInspection = iSaveInspection;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/inspections")
    public ResponseEntity<List<Inspection>> getAllInspections() {
        List<Inspection> list = iGetInspection.getAll();

        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/inspections/unassigned")
    public ResponseEntity<List<Inspection>> getUnassigned() {
        List<Inspection> list = iGetInspection.getUnassigned();

        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/inspection/{id}")
    public ResponseEntity<HttpStatus> update(@PathVariable Integer id, @RequestBody Inspection inspection) {
        try {
            iSaveInspection.update(id, inspection);
        } catch (NoSuchInspection noSuchExample) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/oneinspection/{id}")
    public ResponseEntity<Inspection> getSpecificInspectionByInspectionId (@PathVariable Integer id) {
        Inspection result = iGetInspection.byId(id);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/inspection/{id}")
    public ResponseEntity<HttpStatus> deleteByInspectionId (@PathVariable Integer id) {
        iSaveInspection.delete(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/inspections")
    ResponseEntity<HttpStatus> add(@RequestBody Inspection inspection) {
        iSaveInspection.save(inspection);

        return new ResponseEntity<>(HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.GET, path = "/inspections/{id}")
    public ResponseEntity<List<Inspection>> getInspectionsByBeehive (@PathVariable Integer id) {
        List<Inspection> list = iGetInspection.findAllByBeehiveId(id);

        return new ResponseEntity<>(list, HttpStatus.OK);
    }

}

package pl.polsl.application.honeycollection.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.polsl.domain.beehive.Beehive;
import pl.polsl.domain.beehive.exception.NoSuchBeehive;
import pl.polsl.domain.beehive.service.IGetBeehive;
import pl.polsl.domain.honeycollection.HoneyCollection;
import pl.polsl.domain.honeycollection.exception.NoSuchHoneycollection;
import pl.polsl.domain.honeycollection.service.IGetHoneyCollection;
import pl.polsl.domain.honeycollection.service.ISaveHoneycollection;

import java.util.List;

@RestController
public class HoneyCollectionController {

    private final IGetHoneyCollection iGetHoneyCollection;
    private final ISaveHoneycollection iSaveHoneycollection;

    public HoneyCollectionController(IGetHoneyCollection iGetHoneyCollection, ISaveHoneycollection iSaveHoneycollection) {
        this.iGetHoneyCollection = iGetHoneyCollection;
        this.iSaveHoneycollection = iSaveHoneycollection;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/honeycollections")
    public ResponseEntity<List<HoneyCollection>> get() {
        List<HoneyCollection> list = iGetHoneyCollection.getAll();

        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/honeycollections/unassigned")
    public ResponseEntity<List<HoneyCollection>> getUnassigned() {
        List<HoneyCollection> list = iGetHoneyCollection.getUnassigned();

        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/honeycollection/{id}")
    public ResponseEntity<HttpStatus> update(@PathVariable Integer id, @RequestBody HoneyCollection honeyCollection) {
        try {
            iSaveHoneycollection.update(id, honeyCollection);
        } catch (NoSuchHoneycollection noSuchExample) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/onehoneycollection/{id}")
    public ResponseEntity<HoneyCollection> getSpecificHoneyCollection (@PathVariable Integer id) {
        HoneyCollection result = iGetHoneyCollection.byId(id);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/honeycollections")
    ResponseEntity<HttpStatus> add (@RequestBody HoneyCollection honeyCollection) {
        iSaveHoneycollection.save(honeyCollection);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/honeycollection/{id}")
    public ResponseEntity<HttpStatus> deleteHoneyCollectionById (@PathVariable Integer id) {
        iSaveHoneycollection.delete(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/honeycollections/{beehiveId}")
    public ResponseEntity<List<HoneyCollection>> getHoneyCollectionsByBeehiveId (@PathVariable Integer beehiveId) {
        List<HoneyCollection> list = iGetHoneyCollection.findAllByBeehiveId(beehiveId);

        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/amountofhoney/{beehiveId}")
    public ResponseEntity<Integer> getAmountOfHoneyFromBeehive (@PathVariable Integer beehiveId) {
        List<HoneyCollection> list = iGetHoneyCollection.findAllByBeehiveId(beehiveId);

        Integer amount = 0;

        for (HoneyCollection honeyCollection : list)
            amount += honeyCollection.getAmount();

        return new ResponseEntity<>(amount, HttpStatus.OK);
    }



}

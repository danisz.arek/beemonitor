package pl.polsl.application.notification;


import microsoft.exchange.webservices.data.core.enumeration.misc.ExchangeVersion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

@Component
public class Mailer {

    private String hostname;
    private String username;
    private String password;
    private String domain;

    public void setEnvVars() {

        // done due to problems with adnotation @Value

        try {
            File myObj = new File("src//main//resources//application.properties");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();

                if (data.contains("mailer.hostname"))
                    hostname = data.substring(data.indexOf("=") + 1);

                if (data.contains("mailer.username"))
                    username = data.substring(data.indexOf("=") + 1);

                if (data.contains("mailer.password"))
                    password = data.substring(data.indexOf("=") + 1);

                if (data.contains("mailer.domain"))
                    domain = data.substring(data.indexOf("=") + 1);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    public void sendMail(String content, String subject, String recipient_to) {

        setEnvVars();

        ExchangeClient client = new ExchangeClient.ExchangeClientBuilder()
                .hostname(hostname)
                .exchangeVersion(ExchangeVersion.Exchange2010)
                .username(username)
                .password(password)
                .domain(domain)
                .recipientTo(recipient_to)
                .subject(subject)
                .message(content)
                .build();
        client.sendExchange();
    }
}

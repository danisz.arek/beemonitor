package pl.polsl.application.notification;

import pl.polsl.domain.beegarden.Beegarden;
import pl.polsl.domain.beegarden.services.IGetBeegarden;
import pl.polsl.domain.beesensor.BeeSensor;
import pl.polsl.domain.beesensor.service.IGetBeeSensor;
import pl.polsl.domain.beesensor_data.BeeSensorData;
import pl.polsl.domain.userdetails.services.IGetUserDetails;

public class IncomingDataAnalyser {

    private IGetBeeSensor iGetBeeSensor;
    private IGetBeegarden iGetBeegarden;
    private IGetUserDetails iGetUserDetails;

    public IncomingDataAnalyser(IGetBeeSensor iGetBeeSensor, IGetBeegarden iGetBeegarden,
                                IGetUserDetails iGetUserDetails) {
        this.iGetBeeSensor = iGetBeeSensor;
        this.iGetBeegarden = iGetBeegarden;
        this.iGetUserDetails = iGetUserDetails;
    }

    public void analyseTempAndHumid(BeeSensorData beeSensorData) {

        if (Float.parseFloat(beeSensorData.getTemperatureIn()) < 25.0) {

            BeeSensor beeSensor = iGetBeeSensor.findByPhoneNo(beeSensorData.getPhoneNo());
            Beegarden beegarden = iGetBeegarden.byId(beeSensor.getBeehive().getBeegardenId());

            new Mailer().sendMail("Pszczoły z ula " +
                            beeSensor.getBeehive().getBeehiveName() +
                            " w pasiece " +
                            beegarden.getBeegardenName() +
                            " prawdopodobnie nie żyją!",
                    "Beemonitoring",
                    iGetUserDetails.findByUserEntityId(beegarden.getUserId()).getEmail());
        }
    }
}

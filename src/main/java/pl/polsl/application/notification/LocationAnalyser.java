package pl.polsl.application.notification;

import pl.polsl.domain.beegarden.Beegarden;
import pl.polsl.domain.beegarden.services.IGetBeegarden;
import pl.polsl.domain.beesensor.BeeSensor;
import pl.polsl.domain.beesensor.service.IGetBeeSensor;
import pl.polsl.domain.beesensor_data.BeeSensorData;
import pl.polsl.domain.beesensor_data.service.IGetBeeSensorData;
import pl.polsl.domain.userdetails.services.IGetUserDetails;

import java.util.List;

public class LocationAnalyser {

    private IGetBeeSensor iGetBeeSensor;
    private IGetBeegarden iGetBeegarden;
    private IGetBeeSensorData iGetBeeSensorData;
    private IGetUserDetails iGetUserDetails;

    public LocationAnalyser(IGetBeeSensor iGetBeeSensor, IGetBeegarden iGetBeegarden,
                            IGetBeeSensorData iGetBeeSensorData, IGetUserDetails iGetUserDetails) {
        this.iGetBeeSensor = iGetBeeSensor;
        this.iGetBeegarden = iGetBeegarden;
        this.iGetBeeSensorData = iGetBeeSensorData;
        this.iGetUserDetails = iGetUserDetails;
    }

    public void analyseLocation(BeeSensorData beeSensorData) {

        List<BeeSensorData> list = iGetBeeSensorData.findAllByBeeSensorId(beeSensorData.getBeeSensorId());

        float homeLatitude;
        float homeLogitude;

        if (list.size() > 0) {
            homeLatitude = Float.parseFloat(list.get(0).getLatitude());
            homeLogitude = Float.parseFloat(list.get(0).getLogitude());

            if (Float.parseFloat(beeSensorData.getLatitude()) - homeLatitude > 0.01 ||
                    Float.parseFloat(beeSensorData.getLogitude()) - homeLogitude > 0.01) {

                BeeSensor beeSensor = iGetBeeSensor.findByPhoneNo(beeSensorData.getPhoneNo());
                Beegarden beegarden = iGetBeegarden.byId(beeSensor.getBeehive().getBeegardenId());

                new Mailer().sendMail(
                        "Ul " +
                                beeSensor.getBeehive().getBeehiveName() +
                                " z pasieki " +
                                beegarden.getBeegardenName() +
                                " jest właśnie przewożony! " +
                                "Aktualnie znajduje się w: " +
                                "lat: " + beeSensorData.getLatitude() +
                                " log: " + beeSensorData.getLogitude()
                        ,
                        "Beemonitoring",
                        iGetUserDetails.findByUserEntityId(beegarden.getUserId()).getEmail()
                );
            }
        }
    }
}

package pl.polsl.application.beequeen.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.polsl.domain.beequeen.BeeQueen;
import pl.polsl.domain.beequeen.exception.NoSuchBeeQueen;
import pl.polsl.domain.beequeen.service.IGetBeeQueen;
import pl.polsl.domain.beequeen.service.ISaveBeeQueen;
import pl.polsl.domain.inspection.Inspection;
import pl.polsl.domain.inspection.exception.NoSuchInspection;
import pl.polsl.domain.inspection.service.IGetInspection;
import pl.polsl.domain.inspection.service.ISaveInspection;

import java.util.List;

@RestController
public class BeeQueenController {

    private final IGetBeeQueen iGetBeeQueen;
    private final ISaveBeeQueen iSaveBeeQueen;

    public BeeQueenController(IGetBeeQueen iGetBeeQueen, ISaveBeeQueen iSaveBeeQueen) {
        this.iGetBeeQueen = iGetBeeQueen;
        this.iSaveBeeQueen = iSaveBeeQueen;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/beequeens")
    public ResponseEntity<List<BeeQueen>> get() {
        List<BeeQueen> list = iGetBeeQueen.getAll();

        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/beequeens/unassigned")
    public ResponseEntity<List<BeeQueen>> getUnassigned() {
        List<BeeQueen> list = iGetBeeQueen.getUnassigned();

        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/beequeen/{id}")
    public ResponseEntity<HttpStatus> update(@PathVariable Integer id, @RequestBody BeeQueen beeQueen) {
        try {
            iSaveBeeQueen.update(id, beeQueen);
        } catch (NoSuchBeeQueen noSuchExample) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/onebeequeen/{id}")
    public ResponseEntity<BeeQueen> getSpecificBeeQueen (@PathVariable Integer id) {
        BeeQueen result = iGetBeeQueen.byId(id);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/beequeens")
    ResponseEntity<HttpStatus> add(@RequestBody BeeQueen beeQueen) {
        iSaveBeeQueen.save(beeQueen);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/beequeen/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable Integer id) {
        iSaveBeeQueen.delete(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/beequeens/{id}")
    public ResponseEntity<List<BeeQueen>> getBeeQueensByBeehive(@PathVariable Integer id) {
        List<BeeQueen> list = iGetBeeQueen.findAllByBeehiveId(id);

        return new ResponseEntity<>(list, HttpStatus.OK);
    }
}

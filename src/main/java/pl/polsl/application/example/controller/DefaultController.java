package pl.polsl.application.example.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import pl.polsl.domain.examples.Example;
import pl.polsl.domain.examples.exception.NoSuchExample;
import pl.polsl.domain.examples.services.IGetExamples;
import pl.polsl.domain.examples.services.ISaveExamples;

import java.util.List;

@RestController
public class DefaultController {

    private final IGetExamples iGetExamples;
    private ISaveExamples iSaveExamples;

    public DefaultController(IGetExamples iGetExamples, ISaveExamples iSaveExamples) {
        this.iGetExamples = iGetExamples;
        this.iSaveExamples = iSaveExamples;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/example")
    public List<Example> getAll() {
        return iGetExamples.getAll();
    }

    @RequestMapping(method = RequestMethod.GET, path = "/example/{id}")
    public Example get(@PathVariable Integer id) {
        Example ex = iGetExamples.byId(id);
        if (ex == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Example not found");
        }
        return ex;
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/example/{id}")
    public ResponseEntity update(@PathVariable Integer id, Example example) {
        try {
            iSaveExamples.update(id, example);
        } catch (NoSuchExample noSuchExample) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/example")
    ResponseEntity add(Example example) {
        iSaveExamples.save(example);

        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/example/{id}")
    public ResponseEntity delete(@PathVariable Integer id) {
        iSaveExamples.delete(id);

        return new ResponseEntity(HttpStatus.OK);
    }
}

package pl.polsl.application.identity.request;

import pl.polsl.domain.identity.valueObject.RoleId;

public class AssignRoleToUserRequest {

    private RoleId roleId;

    public AssignRoleToUserRequest() {}

    public AssignRoleToUserRequest(Integer id) {
        this.roleId = new RoleId(id);
    }

    public RoleId getRoleId() {
        return roleId;
    }
}

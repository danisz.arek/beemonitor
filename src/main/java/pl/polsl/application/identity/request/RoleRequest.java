package pl.polsl.application.identity.request;

import pl.polsl.domain.identity.valueObject.RoleId;

public class RoleRequest {
    private final RoleId id;
    private final String name;

    public RoleRequest(Integer id, String name) {
        this.id = new RoleId(id);
        this.name = name;
    }

    public RoleId getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}

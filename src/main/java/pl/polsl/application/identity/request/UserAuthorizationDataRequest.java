package pl.polsl.application.identity.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserAuthorizationDataRequest {

    private String username;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    public UserAuthorizationDataRequest() {
    }

    public UserAuthorizationDataRequest(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}

package pl.polsl.application.identity.request;

import pl.polsl.domain.identity.valueObject.BeegardenId;

public class BeegardenRequest {
    private final BeegardenId id;
    private final String name;

    public BeegardenRequest(Integer id, String name) {
        this.id = new BeegardenId(id);
        this.name = name;
    }

    public BeegardenId getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}

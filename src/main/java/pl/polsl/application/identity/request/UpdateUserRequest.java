package pl.polsl.application.identity.request;

import java.util.List;

public class UpdateUserRequest {

    private final String username;
    private final List<RoleRequest> roles;
    private final List<BeegardenRequest> beegardens;

    public UpdateUserRequest(String username, List<RoleRequest> roles, List<BeegardenRequest> beegardens) {

        this.username = username;
        this.roles = roles;
        this.beegardens = beegardens;
    }

    public String getUsername() {
        return username;
    }

    public List<RoleRequest> getRoles() {
        return roles;
    }

    public List<BeegardenRequest> getBeegardens() {
        return beegardens;
    }
}

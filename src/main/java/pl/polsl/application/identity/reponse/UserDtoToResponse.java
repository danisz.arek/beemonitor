package pl.polsl.application.identity.reponse;

import org.springframework.stereotype.Component;
import pl.polsl.domain.identity.dto.User;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserDtoToResponse {
    private final RoleDtoConverter roleConverter;
    private final BeegardenDtoConverter beegardenDtoConverter;

    public UserDtoToResponse(RoleDtoConverter roleConverter, BeegardenDtoConverter beegardenDtoConverter) {
        this.roleConverter = roleConverter;
        this.beegardenDtoConverter = beegardenDtoConverter;
    }


    public UserResponse dtoToEntity(User user) {
        List<RoleResponse> roles = new ArrayList<>();
        List<BeegardenResponse> beegardens = new ArrayList<>();

        user.getRoles().forEach((role) ->
                roles.add(roleConverter.dtoToResponse(role))
        );

        user.getBeegardens().forEach((beegarden) ->
                beegardens.add(beegardenDtoConverter.dtoToResponse(beegarden))
        );

        return new UserResponse(
                user.getId().getId(),
                user.getUsername().getUsername(),
                roles,
                beegardens
        );
    }
}

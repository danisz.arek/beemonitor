package pl.polsl.application.identity.reponse;

import pl.polsl.application.identity.reponse.BeegardenResponse;
import pl.polsl.application.identity.reponse.RoleResponse;
import java.util.ArrayList;
import java.util.List;

public class JwtResponse {
    private String token;
    private Integer id;
    private String username;
    private List<String> roles;


    public JwtResponse(String accessToken, Integer id, String username, List<RoleResponse> roles) {
        this.token = accessToken;
        this.id = id;
        this.username = username;
        this.roles = new ArrayList<>();

        for (RoleResponse role : roles)
            this.roles.add(role.getName());
    }

    public String getAccessToken() {
        return token;
    }

    public void setAccessToken(String accessToken) {
        this.token = accessToken;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

}


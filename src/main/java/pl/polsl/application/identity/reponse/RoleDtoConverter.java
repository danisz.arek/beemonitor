package pl.polsl.application.identity.reponse;

import org.springframework.stereotype.Component;
import pl.polsl.domain.identity.dto.Role;

@Component
public class RoleDtoConverter {

    public RoleResponse dtoToResponse(Role role) {
        return new RoleResponse(
                role.getRoleId().getId(),
                role.getRoleName().toString()
        );
    }
}

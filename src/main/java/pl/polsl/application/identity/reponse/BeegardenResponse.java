package pl.polsl.application.identity.reponse;

public class BeegardenResponse {

    private final Integer id;
    private final String name;

    public BeegardenResponse(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}

package pl.polsl.application.identity.reponse;

import org.springframework.stereotype.Component;
import pl.polsl.domain.identity.dto.Beegarden;

@Component
public class BeegardenDtoConverter {

    public BeegardenResponse dtoToResponse(Beegarden beegarden) {
        return new BeegardenResponse(
                beegarden.getBeegardenId().getId(),
                beegarden.getBeegardenName().toString()
        );
    }
}

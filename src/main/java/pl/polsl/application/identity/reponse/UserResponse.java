package pl.polsl.application.identity.reponse;

import java.util.List;

public class UserResponse {
    private final Integer id;
    private final String username;
    private final List<RoleResponse> roles;
    private final List<BeegardenResponse> beegardens;

    public UserResponse(Integer id, String username, List<RoleResponse> roles, List<BeegardenResponse> beegardens) {

        this.id = id;
        this.username = username;
        this.roles = roles;
        this.beegardens = beegardens;
    }

    public Integer getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public List<RoleResponse> getRoles() {
        return roles;
    }

    public List<BeegardenResponse> getBeegardens() { return beegardens; }
}

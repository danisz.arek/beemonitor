package pl.polsl.application.identity.controller;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.polsl.application.identity.reponse.RoleDtoConverter;
import pl.polsl.application.identity.reponse.RoleResponse;
import pl.polsl.domain.identity.dto.Role;
import pl.polsl.domain.identity.service.IRoleService;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/role")
public class RoleController {

    private final IRoleService service;
    private final RoleDtoConverter converter;

    public RoleController(IRoleService service, RoleDtoConverter converter) {
        this.service = service;
        this.converter = converter;
    }

    @GetMapping("")
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity getRoles() {
        List<Role> roles = service.getAll();

        List<RoleResponse> response = new ArrayList<>();
        roles.forEach(r -> response.add(converter.dtoToResponse(r)));


        return new ResponseEntity(response, HttpStatus.OK);
    }
}

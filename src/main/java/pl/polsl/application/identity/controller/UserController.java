package pl.polsl.application.identity.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import pl.polsl.application.identity.reponse.*;
import pl.polsl.application.identity.request.AssignRoleToUserRequest;
import pl.polsl.application.identity.request.UpdateUserRequest;
import pl.polsl.application.identity.request.UserAuthorizationDataRequest;
import pl.polsl.domain.beegarden.Beegarden;
import pl.polsl.domain.beegarden.services.IGetBeegarden;
import pl.polsl.domain.beegarden.services.ISaveBeegarden;
import pl.polsl.domain.identity.command.AssignRoleToUser;
import pl.polsl.domain.identity.command.CreateUser;
import pl.polsl.domain.identity.command.DeleteUser;
import pl.polsl.domain.identity.command.UpdateUser;
import pl.polsl.domain.identity.dto.Role;
import pl.polsl.domain.identity.dto.User;
import pl.polsl.domain.identity.query.GetToken;
import pl.polsl.domain.identity.service.IAuthenticationService;
import pl.polsl.domain.identity.service.IUserService;
import pl.polsl.domain.identity.valueObject.*;
import pl.polsl.domain.userdetails.UserDetails;
import pl.polsl.domain.userdetails.services.IGetUserDetails;
import pl.polsl.domain.userdetails.services.ISaveUserDetails;
import pl.polsl.infrastructure.identity.exception.UserNotFoundException;
import pl.polsl.infrastructure.identity.exception.ValidationException;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/user")
public class UserController {

    private final IUserService userService;
    private final IAuthenticationService authenticationService;
    private final UserDtoToResponse dtoToResponseConverter;
    private final RoleDtoConverter roleDtoConverter;
    private final IGetBeegarden iGetBeegarden;
    private final ISaveBeegarden iSaveBeegarden;
    private final IGetUserDetails iGetUserDetails;
    private final ISaveUserDetails iSaveUserDetails;


    public UserController (
            IUserService userService,
            IAuthenticationService authenticationService,
            UserDtoToResponse dtoToResponseConverter,
            RoleDtoConverter roleDtoConverter,
            IGetBeegarden iGetBeegarden,
            ISaveBeegarden iSaveBeegarden,
            IGetUserDetails iGetUserDetails,
            ISaveUserDetails iSaveUserDetails
    ) {
        this.userService = userService;
        this.authenticationService = authenticationService;
        this.dtoToResponseConverter = dtoToResponseConverter;
        this.roleDtoConverter = roleDtoConverter;
        this.iGetBeegarden = iGetBeegarden;
        this.iSaveBeegarden = iSaveBeegarden;
        this.iGetUserDetails = iGetUserDetails;
        this.iSaveUserDetails = iSaveUserDetails;
    }

    @PostMapping("")
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity signup(@RequestBody UserAuthorizationDataRequest user) {
        try {
            userService.createUser(new CreateUser(user.getUsername(), user.getPassword()));
        } catch (ValidationException exception) {
            return new ResponseEntity(exception.getMessage(), HttpStatus.CONFLICT);
        }
        return new ResponseEntity(user, HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.GET, path = "{userId}/roles")
    public List<RoleResponse> getRoles(@PathVariable Integer userId, @AuthenticationPrincipal Principal user) {
        List<Role> roles =  userService.byId(userId).getRoles();
        List<RoleResponse> response = new ArrayList<>();

        roles.forEach(role -> response.add(roleDtoConverter.dtoToResponse(role)));

        return response;
    }

    @RequestMapping(method = RequestMethod.GET, path = "{userId}/roles/{roleId}")
    public Optional<RoleResponse> getRoleById(@PathVariable Integer userId, @PathVariable Integer roleId, @AuthenticationPrincipal Principal user) {
        return Optional.ofNullable(roleDtoConverter.dtoToResponse(userService.byId(userId).getRoleById(roleId).orElseGet(null)));
    }


    @RequestMapping(method = RequestMethod.POST, path = "{userId}/role/{roleId}")
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity assignRoleToUser(@PathVariable Integer userId, @PathVariable Integer roleId, @AuthenticationPrincipal Principal user) {
        IResult result = userService.assignRoleToUser(new AssignRoleToUser(new RoleId(roleId), new UserId(userId)));

        HttpStatus status = HttpStatus.resolve(result.getResultCode());
        if (status == null)
            return new ResponseEntity(result.getMessage(), HttpStatus.BAD_REQUEST);

        return new ResponseEntity(result.getMessage(), status);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "{userId}/roles/{roleId}")
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity deleteUserRole (@PathVariable Integer userId, @PathVariable Integer roleId, @AuthenticationPrincipal Principal user) {
        try {
            userService.deleteUserRole(userId, roleId);
        } catch (UserNotFoundException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.GET, path = "{userId}")
    public UserResponse get(@PathVariable Integer userId) {
        return dtoToResponseConverter.dtoToEntity(userService.byId(userId));
    }

    @RequestMapping(method = RequestMethod.GET, path = "")
    public List<UserResponse> getAll() {
        List<User> users = userService.getAll();
        List<UserResponse> response = new ArrayList<>();
        users.forEach(user -> response.add(dtoToResponseConverter.dtoToEntity(user)));

        return response;
    }


    @RequestMapping(method = RequestMethod.DELETE, path = "{userId}")
    public ResponseEntity deleteAction(@PathVariable Integer userId) {

        try {

            List<Beegarden> beegardensToDelete = iGetBeegarden.findAllByUserId(userId);

            for (Beegarden beegardenToDelete : beegardensToDelete)
                iSaveBeegarden.delete(beegardenToDelete.getId());

            List<UserDetails> userDetailsToDelete = iGetUserDetails.findAllByUserEntityId(userId);

            for (UserDetails userDetailToDelete : userDetailsToDelete)
                iSaveUserDetails.delete(userDetailToDelete.getId());

            userService.deleteUser(new DeleteUser(new UserId(userId)));

        } catch (UserNotFoundException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/login")
    public ResponseEntity login(@RequestBody UserAuthorizationDataRequest loginUser) throws AuthenticationException {
        AuthToken token = authenticationService.getTokenForUser(
                new GetToken(
                        new Username(loginUser.getUsername()),
                        new Password(loginUser.getPassword())
                )
        );

        UserResponse ur = dtoToResponseConverter.dtoToEntity(userService.findOneByUsername(loginUser.getUsername()));

        return ResponseEntity.ok(new JwtResponse(token.toString(), ur.getId(), ur.getUsername(), ur.getRoles() ));
    }

    @RequestMapping(method = RequestMethod.POST, value = "/token")
    public ResponseEntity getToken (@RequestBody UserAuthorizationDataRequest loginUser) throws AuthenticationException {
        AuthToken token = authenticationService.getTokenForUser(
                new GetToken(
                        new Username(loginUser.getUsername()),
                        new Password(loginUser.getPassword())
                )
        );

        UserResponse ur = dtoToResponseConverter.dtoToEntity(userService.findOneByUsername(loginUser.getUsername()));

        return ResponseEntity.ok(token.toString());
    }

    @PutMapping(path = "{userId}")
    public ResponseEntity update(@PathVariable Integer userId, @RequestBody UpdateUserRequest userRequest) {
        List<RoleId> roleIds = userRequest.getRoles().stream().map(r -> r.getId()).collect(Collectors.toList());
        List<BeegardenId> beegardenIds = userRequest.getBeegardens().stream().map(b -> b.getId()).collect(Collectors.toList());

        try {
            userService.update(new UpdateUser(new UserId(userId), userRequest.getUsername(), roleIds, beegardenIds));
        } catch (UserNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }
}

package pl.polsl.application.identity.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import pl.polsl.application.identity.request.ChangePasswordRequest;
import pl.polsl.domain.identity.command.ChangePassword;
import pl.polsl.domain.identity.dto.User;
import pl.polsl.domain.identity.service.IUserService;
import pl.polsl.infrastructure.identity.encoder.IEncoder;

import java.security.Principal;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class MeController {

    private final IEncoder encoder;
    private final IUserService userService;

    public MeController(IEncoder encoder, IUserService userService) {
        this.encoder = encoder;
        this.userService = userService;
    }

    @RequestMapping(method = RequestMethod.POST, path = "/me/changepassword")
    public ResponseEntity changePassword(@RequestBody ChangePasswordRequest request, @AuthenticationPrincipal Principal user) {
        User userDto = this.userService.findOneByUsername(user.getName());

        if (!this.encoder.match(request.getOldPassword(), userDto.getPassword().toString())) {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }

        this.userService.changeUserPassword(new ChangePassword(user.getName(), request.getNewPassword()));

        return new ResponseEntity(HttpStatus.OK);
    }
}

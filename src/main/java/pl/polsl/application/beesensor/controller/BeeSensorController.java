package pl.polsl.application.beesensor.controller;

import com.sun.org.apache.bcel.internal.generic.ANEWARRAY;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.polsl.domain.beegarden.Beegarden;
import pl.polsl.domain.beegarden.services.IGetBeegarden;
import pl.polsl.domain.beehive.Beehive;
import pl.polsl.domain.beehive.service.IGetBeehive;
import pl.polsl.domain.beesensor.BeeSensor;
import pl.polsl.domain.beesensor.exception.NoSuchBeeSensor;
import pl.polsl.domain.beesensor.service.IGetBeeSensor;
import pl.polsl.domain.beesensor.service.ISaveBeeSensor;
import pl.polsl.domain.beesensor_data.service.IGetBeeSensorData;
import pl.polsl.domain.beesensor_data.service.ISaveBeeSensorData;
import pl.polsl.domain.honeycollection.HoneyCollection;
import pl.polsl.domain.honeycollection.exception.NoSuchHoneycollection;
import pl.polsl.domain.honeycollection.service.IGetHoneyCollection;
import pl.polsl.domain.honeycollection.service.ISaveHoneycollection;

import java.util.ArrayList;
import java.util.List;

@RestController
public class BeeSensorController {

    private final IGetBeeSensor iGetBeeSensor;
    private final ISaveBeeSensor iSaveBeeSensor;
    private final IGetBeegarden iGetBeegarden;
    private final IGetBeehive iGetBeehive;

    public BeeSensorController(IGetBeeSensor iGetBeeSensor, ISaveBeeSensor iSaveBeeSensor,
                               IGetBeegarden iGetBeegarden, IGetBeehive iGetBeehive) {
        this.iGetBeeSensor = iGetBeeSensor;
        this.iSaveBeeSensor = iSaveBeeSensor;
        this.iGetBeegarden = iGetBeegarden;
        this.iGetBeehive = iGetBeehive;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/beesensors")
    public ResponseEntity<List<BeeSensor>> get() {
        List<BeeSensor> list = iGetBeeSensor.getAll();

        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/beesensor/{id}")
    public ResponseEntity<HttpStatus> update(@PathVariable Integer id, @RequestBody BeeSensor beeSensor) {
        try {
            iSaveBeeSensor.update(id, beeSensor);
        } catch (NoSuchBeeSensor noSuchExample) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/beesensor/{id}")
    public ResponseEntity<BeeSensor> getSpecificBeeSensor (@PathVariable Integer id) {
        BeeSensor result = iGetBeeSensor.byId(id);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/beesensorbyphone/{phoneNo}")
    public ResponseEntity<BeeSensor> getSpecificBeeSensorByPhone (@PathVariable String phoneNo) {
        BeeSensor result = iGetBeeSensor.findByPhoneNo(phoneNo);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/beesensors")
    ResponseEntity<HttpStatus> add(@RequestBody BeeSensor beeSensor) {
        return iSaveBeeSensor.save(beeSensor);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/beesensor/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable Integer id) {
        iSaveBeeSensor.delete(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/beesensors/{id}")
    public ResponseEntity<List<BeeSensor>> getBeeSensorsByBeehive (@PathVariable Integer id) {
        List<BeeSensor> list = iGetBeeSensor.findAllByBeehiveId(id);

        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/countsensors/{userId}")
    public ResponseEntity<Integer> countBeeSensorsForUser (@PathVariable Integer userId) {

        List<Beegarden> listOfBeegardens = iGetBeegarden.findAllByUserId(userId);
        List<Beehive> listOfBeehives = new ArrayList<>();
        List<BeeSensor> listOfSensors = new ArrayList<>();

        for (Beegarden beegarden : listOfBeegardens)
            listOfBeehives.addAll(iGetBeehive.findAllByBeegardenId(beegarden.getId()));

        for (Beehive beehive : listOfBeehives)
            listOfSensors.addAll(iGetBeeSensor.findAllByBeehiveId(beehive.getId()));

        return new ResponseEntity<>(listOfSensors.size(), HttpStatus.OK);
    }
}

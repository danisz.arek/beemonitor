package pl.polsl.application.userdetails.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.polsl.domain.beegarden.Beegarden;
import pl.polsl.domain.beegarden.exception.NoSuchBeegarden;
import pl.polsl.domain.beegarden.services.IGetBeegarden;
import pl.polsl.domain.beegarden.services.ISaveBeegarden;
import pl.polsl.domain.identity.dto.User;
import pl.polsl.domain.userdetails.UserDetails;
import pl.polsl.domain.userdetails.exception.NoSuchDetail;
import pl.polsl.domain.userdetails.services.IGetUserDetails;
import pl.polsl.domain.userdetails.services.ISaveUserDetails;

import java.util.List;

@RestController
public class UserDetailsController {

    private final IGetUserDetails iGetUserDetails;
    private final ISaveUserDetails iSaveUserDetails;

    public UserDetailsController(IGetUserDetails iGetUserDetails, ISaveUserDetails iSaveUserDetails) {
        this.iGetUserDetails = iGetUserDetails;
        this.iSaveUserDetails = iSaveUserDetails;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/userdetails")
    public ResponseEntity<List<UserDetails>> get() {
        List<UserDetails> list = iGetUserDetails.getAll();

        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/userdetail/{id}")
    public ResponseEntity<UserDetails> getUserDetailsById (@PathVariable Integer id) {
        UserDetails userDetails= iGetUserDetails.byId(id);

        return new ResponseEntity<>(userDetails, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/specificuserdetail/{id}")
    public ResponseEntity<UserDetails> getUserDetailsFromUser(@PathVariable Integer id) {
        UserDetails userDetails= iGetUserDetails.findByUserEntityId(id);

        return new ResponseEntity<>(userDetails, HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.PUT, path = "/userdetails/{id}")
    public ResponseEntity<HttpStatus> updateByUser (@PathVariable Integer id, @RequestBody UserDetails userDetails) {
        try {
            iSaveUserDetails.updateByUser(id, userDetails);
        } catch (NoSuchDetail noSuchExample) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/userdetails")
    ResponseEntity<HttpStatus> add(@RequestBody UserDetails userDetails) {
        return iSaveUserDetails.save(userDetails);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/specificuserdetail/{userId}")
    public ResponseEntity<HttpStatus> deleteByUserId (@PathVariable Integer userId) {

        iSaveUserDetails.deleteByUser(userId);

        return new ResponseEntity<>(HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.DELETE, path = "/userdetail/{id}")
    public ResponseEntity<HttpStatus> deleteByUserDetailId (@PathVariable Integer id) {
        iSaveUserDetails.delete(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/userdetails/{userId}")
    public ResponseEntity<List<UserDetails>> getUserDetailsForSpecificUser (@PathVariable Integer userId) {
        List<UserDetails> userDetails= iGetUserDetails.findAllByUserEntityId(userId);

        return new ResponseEntity<>(userDetails, HttpStatus.OK);
    }

}

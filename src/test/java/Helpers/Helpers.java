package Helpers;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import javax.json.Json;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;
import java.io.IOException;
import java.util.Collections;

public class Helpers {

    private String link = "http://127.0.0.1:";
    private String port = "8080";
    private String uri = link + port;
    private String loginPath = "/user/login";

    public String getUri() {
        return uri;
    }

    public String getLoginPath() {
        return loginPath;
    }

    public void setLoginPath(String loginPath) {
        this.loginPath = loginPath;
    }

    public String createToken(String username, String password) {

        JsonBuilderFactory builderFactory = Json.createBuilderFactory(Collections.emptyMap());

        JsonObject json = builderFactory.createObjectBuilder()
                .add("username", username)
                .add("password", password)
                .build();

        HttpPost postReq = new HttpPost(getUri() + getLoginPath());
        StringEntity en;

        try {
            en = new StringEntity(json.toString());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        postReq.setHeader("Accept", "application/json");
        postReq.setHeader("Content-type", "application/json");
        postReq.setEntity(en);

        HttpResponse postRes;
        try {
            postRes = HttpClientBuilder.create().build().execute(postReq);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        HttpEntity postResponse = postRes.getEntity();

        try {
            if (postResponse != null) {
                String retSrc = EntityUtils.toString(postResponse);
                return new JSONObject(retSrc).getString("accessToken");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return null;
    }

    public JsonObject createSignUpBody () {
        JsonBuilderFactory builderFactory = Json.createBuilderFactory(Collections.emptyMap());

        JsonObject json = builderFactory.createObjectBuilder()
                .add("username", "arkadiusz")
                .add("password", "danisz")
                .build();

        return json;
    }

    public JsonObject createLoginBody (String u, String p) {
        JsonBuilderFactory builderFactory = Json.createBuilderFactory(Collections.emptyMap());

        JsonObject json = builderFactory.createObjectBuilder()
                .add("username", u)
                .add("password", p)
                .build();

        return json;
    }

    public JsonObject createChangePasswordBody (String oldPass, String newPass) {
        JsonBuilderFactory builderFactory = Json.createBuilderFactory(Collections.emptyMap());

        JsonObject json = builderFactory.createObjectBuilder()
                .add("oldPassword", oldPass)
                .add("newPassword", newPass)
                .build();

        return json;
    }

    public String getSomeContent(String uri) throws IOException {

        String authToken = "Bearer " + createToken("test", "test");

        HttpGet get = new HttpGet(uri);
        get.setHeader("Accept", "application/json");
        get.setHeader("Content-type", "application/json");
        get.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        HttpResponse getResponse = HttpClientBuilder.create().build().execute(get);

        HttpEntity getEntity = getResponse.getEntity();
        return EntityUtils.toString(getEntity, "UTF-8");
    }
}

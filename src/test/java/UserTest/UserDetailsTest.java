package UserTest;

import Helpers.Helpers;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class UserDetailsTest {

    private Helpers helper = new Helpers();
    private String authToken = "Bearer " + helper.createToken("test2", "test");

    private String userDetailsUri = helper.getUri() + "/userdetails";
    private String userDetailUri = helper.getUri() + "/userdetail";

    @Test
    @Order(1)
    public void getUserDetailForUser1 () throws IOException {

        HttpGet request = new HttpGet(userDetailsUri + "/1");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertThat("GET response contains 'Arkadiusz' [beekeeper name]",
                responseContent, containsString("Arkadiusz"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(2)
    public void getUserDetailForUser2 () throws IOException {

        HttpGet request = new HttpGet(userDetailsUri + "/2");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertThat("GET response contains 'Rudolf' [beekeeper name]",
                responseContent, containsString("Rudolf"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    private String getIdOfTestUser () throws IOException {

        HttpGet request = new HttpGet(helper.getUri() + "/userdetails");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        Pattern pattern = Pattern.compile(".*id.:(\\d{1,}),.beekeperName.:.Rudolf.*");
        Matcher matcher = pattern.matcher(responseContent);

        String result = null;
        if (matcher.find())
            result = matcher.group(1);

        return result;
    }


    @Test
    @Order(3)
    public void deleteUserDetailsByID () throws IOException {

        HttpDelete request = new HttpDelete(userDetailUri + "/" + getIdOfTestUser() );
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(4)
    public void getUserDetailsToSeeIfDeleteStepWereSuccessful () throws IOException {

        HttpGet request = new HttpGet(userDetailsUri);
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertTrue("Response doesn't contain 'Rudolf'", !responseContent.contains("Rudolf"));
        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(5)
    public void saveNewUserDetails () throws IOException {

        String strJson = "{\"beekeperName\":\"Rudolf\",\"beekeperSurname\":\"Janecki\",\"email\":\"no.mail@gmail.com\",\"postCode\":\"44-160\",\"city\":\"Bojszow\",\"road\":\"Gorna\",\"roadNr\":\"70\",\"user\":{\"username\":\"test2\",\"roles\":[{\"roleName\":{\"name\":\"ROLE_user\"},\"roleId\":{\"id\":1}}],\"beegardens\":[{\"beegardenName\":{\"name\":\"pasieka3\"},\"beegardenId\":{\"id\":3}},{\"beegardenName\":{\"name\":\"pasieka2\"},\"beegardenId\":{\"id\":2}}]}}";

        HttpPost request = new HttpPost(userDetailsUri);
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        request.setEntity(new StringEntity(strJson));

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(6)
    public void getUserDetailForUser2ToCheckIfPostWereSuccessful () throws IOException {

        HttpGet request = new HttpGet(userDetailsUri);
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertThat("GET response contains 'Rudolf' [beekeeper name]",
                responseContent, containsString("Rudolf"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(7)
    public void deleteUserDetailsByUserId () throws IOException {

        HttpDelete request = new HttpDelete( userDetailUri + "/" + getIdOfTestUser());
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(8)
    public void getUserDetailsToSeeIfDeleteStepWereSuccessful2 () throws IOException {

        HttpGet request = new HttpGet(userDetailsUri);
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertTrue("GET response doesn't contains 'Rudolf' [beekeeper name]",
                !responseContent.contains("Rudolf"));
        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(9)
    public void restoreUserDetails () throws IOException {

        String strJson = "{\"beekeperName\":\"Rudolf\",\"beekeperSurname\":\"Janecki\",\"email\":\"no.mail@gmail.com\",\"postCode\":\"44-160\",\"city\":\"Bojszow\",\"road\":\"Gorna\",\"roadNr\":\"70\",\"user\":{\"username\":\"test2\",\"roles\":[{\"roleName\":{\"name\":\"ROLE_user\"},\"roleId\":{\"id\":1}}],\"beegardens\":[{\"beegardenName\":{\"name\":\"pasieka3\"},\"beegardenId\":{\"id\":3}},{\"beegardenName\":{\"name\":\"pasieka2\"},\"beegardenId\":{\"id\":2}}]}}";

        HttpPost request = new HttpPost(userDetailsUri);
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        request.setEntity(new StringEntity(strJson));

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(10)
    public void getAllDetails () throws IOException {

        HttpGet request = new HttpGet(userDetailsUri);
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertThat("GET response contains 'Rudolf' [beekeeper name]",
                responseContent, containsString("Rudolf"));

        assertThat("GET response contains 'Arkadiusz' [beekeeper name]",
                responseContent, containsString("Arkadiusz"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }


}

package UserTest;

import Helpers.Helpers;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class InspectionControllerTest {

    private Helpers helper = new Helpers();
    private String authToken = "Bearer " + helper.createToken("test2", "test");
    private String newBeehiveId = null;
    private String newInspectionId = null;

    private String getNewBeehiveId() {
        return newBeehiveId;
    }

    private void setNewBeehiveId(String newBeehiveId) {
        this.newBeehiveId = newBeehiveId;
    }

    private String getNewInspectionId() {
        return newInspectionId;
    }

    private void setNewInspectionId(String newInspectionId) {
        this.newInspectionId = newInspectionId;
    }

    @Test
    @Order(1)
    public void getAllInspections() throws IOException {

        HttpGet request = new HttpGet(helper.getUri() + "/inspections");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertThat("GET response contains 'inspectionDate'",
                responseContent, containsString("inspectionDate"));

        assertThat("GET response contains 'treatment'",
                responseContent, containsString("treatment"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    private void createTestBeehive() throws IOException {

        String beehiveCreateJson = "{\"beegardenId\":1,\"beehiveName\":\"testowy_ul\",\"type\":\"wielkopolski\",\"dateOfCreation\":\"2020.11.21 11:50\",\"framesAmount\":10,\"trunksAmount\":1,\"bottom\":\"higieniczna\",\"feeder\":true,\"poolenTrapper\":false,\"propolisSinker\":true,\"queenCage\":false,\"barrier\":true,\"isolator\":true,\"heater\":false,\"inletWidth\":10,\"workFramesAmount\":1,\"force\":\"silna\",\"temperament\":\"waleczny\",\"queenCell\":false,\"swarmMood\":false,\"lackOfFood\":false,\"grubs\":3,\"propolis\":30,\"honey\":30}";

        HttpPost request = new HttpPost(helper.getUri() + "/beehives");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        request.setEntity(new StringEntity(beehiveCreateJson));

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(200, response.getStatusLine().getStatusCode());
    }


    private String getIdOfNewlyCreatedBeehive() throws IOException {

        HttpGet request = new HttpGet(helper.getUri() + "/beehives");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        Pattern pattern = Pattern.compile(".*id.:(\\d{2,}),.beegardenId\":1,.beehiveName\":.testowy_ul.,.type.:.wielkopolski.*");
        Matcher matcher = pattern.matcher(responseContent);

        String result = null;
        if (matcher.find())
            result = matcher.group(1);

        return result;
    }

    private void getInspectionsOfNewlyCreatedBeehive() throws IOException {
        createTestBeehive();
        setNewBeehiveId(getIdOfNewlyCreatedBeehive());

        HttpGet request = new HttpGet(helper.getUri() + "/inspections/" + newBeehiveId);
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertThat("GET response is empty",
                responseContent, containsString("[]"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(2)
    public void createNewInspectionToNewBeehive() throws IOException {

        getInspectionsOfNewlyCreatedBeehive();

        String json = "{\"inspectionDate\":\"2020-11-01 11:30\",\"feeding\":false,\"queen\":true,\"treatment\":false,\"notes\":\"testowy_inspection\",\"beehiveId\":" + getNewBeehiveId() + "}";

        HttpPost request = new HttpPost(helper.getUri() + "/inspections");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        request.setEntity(new StringEntity(json));

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(3)
    public void checkPostRequest() throws IOException {

        setNewBeehiveId(getIdOfNewlyCreatedBeehive());

        HttpGet request = new HttpGet(helper.getUri() + "/inspections/" + getNewBeehiveId());
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertThat("GET response contains 'testowy_inspection'",
                responseContent, containsString("testowy_inspection"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    private String getIdOfNewlyInspection() throws IOException {

        setNewBeehiveId(getIdOfNewlyCreatedBeehive());

        HttpGet request = new HttpGet(helper.getUri() + "/inspections/" + getNewBeehiveId());
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        Pattern pattern = Pattern.compile(".*id.:(\\d{2,}),.inspectionDate.:.*");
        Matcher matcher = pattern.matcher(responseContent);

        String result = null;
        if (matcher.find())
            result = matcher.group(1);

        return result;
    }

    @Test
    @Order(4)
    public void testPutMessage() throws IOException {

        setNewInspectionId(getIdOfNewlyInspection());
        setNewBeehiveId(getNewBeehiveId());
        String json = "{\"inspectionDate\":\"2020-11-01 11:30\",\"feeding\":false,\"queen\":true,\"treatment\":false,\"notes\":\"put_inspection\",\"beehiveId\":" + getNewBeehiveId() + "}";

        HttpPut request = new HttpPut(helper.getUri() + "/inspection/" + getNewInspectionId());
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        request.setEntity(new StringEntity(json));

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(5)
    public void checkIfEditWasSuccessful() throws IOException {

        setNewBeehiveId(getIdOfNewlyCreatedBeehive());

        HttpGet request = new HttpGet(helper.getUri() + "/inspections/" + getNewBeehiveId());
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertThat("GET response contains 'put_inspection'",
                responseContent, containsString("put_inspection"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(6)
    public void testDeleteMessage() throws IOException {

        setNewInspectionId(getIdOfNewlyInspection());

        HttpDelete request = new HttpDelete(helper.getUri() + "/inspection/" + getNewInspectionId());
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(7)
    public void checkIfDeleteWasSuccessful() throws IOException {

        setNewBeehiveId(getIdOfNewlyCreatedBeehive());

        HttpGet request = new HttpGet(helper.getUri() + "/inspections/" + getNewBeehiveId());
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertThat("GET response is empty",
                responseContent, containsString("[]"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(8)
    public void cleanUp() throws IOException {

        setNewBeehiveId(getIdOfNewlyCreatedBeehive());

        HttpDelete request = new HttpDelete(helper.getUri() + "/beehive/" + getNewBeehiveId());
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

}

package UserTest;

import Helpers.Helpers;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;


@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class BeehiveControllerTest {

    private Helpers helper = new Helpers();
    private String authToken = "Bearer " + helper.createToken("test2", "test");
    private String newBeehiveId = null;

    private String getNewBeehiveId() {
        return newBeehiveId;
    }

    private void setNewBeehiveId(String newBeehiveId) {
        this.newBeehiveId = newBeehiveId;
    }

    @Test
    @Order(1)
    public void getAllBeehives() throws IOException {

        HttpGet request = new HttpGet(helper.getUri() + "/beehives");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertThat("GET response contains 'beehiveName'",
                responseContent, containsString("beehiveName"));

        assertThat("GET response contains 'poolenTrapper'",
                responseContent, containsString("poolenTrapper"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(2)
    public void createNewBeehive() throws IOException {

        String beehiveCreateJson = "{\"beegardenId\":1,\"beehiveName\":\"testowy_ul\",\"type\":\"wielkopolski\",\"dateOfCreation\":\"2020.11.21 11:50\",\"framesAmount\":10,\"trunksAmount\":1,\"bottom\":\"higieniczna\",\"feeder\":true,\"poolenTrapper\":false,\"propolisSinker\":true,\"queenCage\":false,\"barrier\":true,\"isolator\":true,\"heater\":false,\"inletWidth\":10,\"workFramesAmount\":1,\"force\":\"silna\",\"temperament\":\"waleczny\",\"queenCell\":false,\"swarmMood\":false,\"lackOfFood\":false,\"grubs\":3,\"propolis\":30,\"honey\":30}";

        HttpPost request = new HttpPost(helper.getUri() + "/beehives");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        request.setEntity(new StringEntity(beehiveCreateJson));

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(200, response.getStatusLine().getStatusCode());
    }


    private String getIdOfNewlyCreatedBeehive() throws IOException {

        HttpGet request = new HttpGet(helper.getUri() + "/beehives");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        Pattern pattern = Pattern.compile(".*id.:(\\d{1,}),.beegardenId\":1,.beehiveName\":.testowy_ul.,.type.:.wielkopolski.*");
        Matcher matcher = pattern.matcher(responseContent);

        String result = null;
        if (matcher.find())
            result = matcher.group(1);

        return result;
    }

    @Test
    @Order(3)
    public void showNewBeehive() throws IOException {

        setNewBeehiveId(getIdOfNewlyCreatedBeehive());

        HttpGet request = new HttpGet(helper.getUri() + "/beehive/" + getNewBeehiveId());
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertThat("GET response contains 'testowy_ul'",
                responseContent, containsString("testowy_ul"));

        assertThat("GET response contains 'barrier'",
                responseContent, containsString("barrier"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(4)
    public void testPutMessage() throws IOException {

        setNewBeehiveId(getIdOfNewlyCreatedBeehive());

        String json = "{\"beegardenId\":1,\"beehiveName\":\"testowy_ul\",\"type\":\"wielkopolski\",\"dateOfCreation\":\"2020.11.21 11:50\",\"framesAmount\":9999,\"trunksAmount\":1,\"bottom\":\"put_it_test\",\"feeder\":true,\"poolenTrapper\":false,\"propolisSinker\":true,\"queenCage\":false,\"barrier\":true,\"isolator\":true,\"heater\":false,\"inletWidth\":10,\"workFramesAmount\":1,\"force\":\"silna\",\"temperament\":\"putek\",\"queenCell\":false,\"swarmMood\":false,\"lackOfFood\":false,\"grubs\":3,\"propolis\":30,\"honey\":30}";

        HttpPut request = new HttpPut(helper.getUri() + "/beehive/" + getNewBeehiveId());
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        request.setEntity(new StringEntity(json));

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(5)
    public void checkPutMessageResults() throws IOException {

        setNewBeehiveId(getIdOfNewlyCreatedBeehive());

        HttpGet request = new HttpGet(helper.getUri() + "/beehive/" + getNewBeehiveId());
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertThat("GET response contains 'put_it_test'",
                responseContent, containsString("put_it_test"));

        assertThat("GET response contains '9999'",
                responseContent, containsString("9999"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(6)
    public void testDeleteMessage() throws IOException {

        setNewBeehiveId(getIdOfNewlyCreatedBeehive());

        HttpDelete request = new HttpDelete(helper.getUri() + "/beehive/" + getNewBeehiveId());
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(7)
    public void checkIfCleanUpWasSuccessful() throws IOException {

        HttpGet request = new HttpGet(helper.getUri() + "/beehives");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertTrue("Response doesn't contain testowy_ul", !responseContent.contains("testowy_ul"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }


}

package UserTest;

import Helpers.Helpers;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class MeControllerTest {

    private Helpers helper = new Helpers();
    private String authToken = "Bearer " + helper.createToken("test2", "test");

    private String changePassUri = helper.getUri() + "/me/changepassword";

    @Test
    @Order(1)
    public void changePassword() throws IOException {

        HttpPost request = new HttpPost(changePassUri);
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        request.setEntity(new StringEntity(helper.createChangePasswordBody("test", "newpass").toString()));

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(2)
    public void tryToLoginWithOldCredentials() throws IOException {

        HttpPost request = new HttpPost(helper.getUri() + "/user/login");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setEntity(new StringEntity(helper.createLoginBody("test2", "test").toString()));

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(401, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(3)
    public void loginWithNewCredentials() throws IOException {

        HttpPost request = new HttpPost(helper.getUri() + "/user/login");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setEntity(new StringEntity(helper.createLoginBody("test2", "newpass").toString()));

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        System.out.println(responseContent);

        assertTrue("Response contains proper details", responseContent.contains("test2")
                &&  responseContent.contains("ROLE_user"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(4)
    public void restorePassword() throws IOException {

        authToken = "Bearer " + helper.createToken("test2", "newpass");

        HttpPost request = new HttpPost(changePassUri);
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        request.setEntity(new StringEntity(helper.createChangePasswordBody("newpass", "test").toString()));

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

}

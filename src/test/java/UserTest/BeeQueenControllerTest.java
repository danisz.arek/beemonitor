package UserTest;

import Helpers.Helpers;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class BeeQueenControllerTest {

    private Helpers helper = new Helpers();
    private String authToken = "Bearer " + helper.createToken("test2", "test");
    private String newBeehiveId = null;
    private String newBeeQueenId = null;

    private String getNewBeehiveId() {
        return newBeehiveId;
    }

    private void setNewBeehiveId(String newBeehiveId) {
        this.newBeehiveId = newBeehiveId;
    }

    private String getNewBeeQueenId() {
        return newBeeQueenId;
    }

    private void setNewBeeQueenId(String newBeeQueenId) {
        this.newBeeQueenId = newBeeQueenId;
    }

    @Test
    @Order(1)
    public void getAllBeeQueens() throws IOException {

        HttpGet request = new HttpGet(helper.getUri() + "/beequeens");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertThat("GET response contains 'queenNo'",
                responseContent, containsString("queenNo"));

        assertThat("GET response contains 'variety'",
                responseContent, containsString("variety"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    private void createTestBeehive() throws IOException {

        String beehiveCreateJson = "{\"beegardenId\":1,\"beehiveName\":\"testowy_ul\",\"type\":\"wielkopolski\",\"dateOfCreation\":\"2020.11.21 11:50\",\"framesAmount\":10,\"trunksAmount\":1,\"bottom\":\"higieniczna\",\"feeder\":true,\"poolenTrapper\":false,\"propolisSinker\":true,\"queenCage\":false,\"barrier\":true,\"isolator\":true,\"heater\":false,\"inletWidth\":10,\"workFramesAmount\":1,\"force\":\"silna\",\"temperament\":\"waleczny\",\"queenCell\":false,\"swarmMood\":false,\"lackOfFood\":false,\"grubs\":3,\"propolis\":30,\"honey\":30}";

        HttpPost request = new HttpPost(helper.getUri() + "/beehives");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        request.setEntity(new StringEntity(beehiveCreateJson));

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(200, response.getStatusLine().getStatusCode());
    }


    private String getIdOfNewlyCreatedBeehive() throws IOException {

        HttpGet request = new HttpGet(helper.getUri() + "/beehives");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        Pattern pattern = Pattern.compile(".*id.:(\\d{2,}),.beegardenId\":1,.beehiveName\":.testowy_ul.,.type.:.wielkopolski.*");
        Matcher matcher = pattern.matcher(responseContent);

        String result = null;
        if (matcher.find())
            result = matcher.group(1);

        return result;
    }

    private void getBeeQueensOfNewlyCreatedBeehive() throws IOException {
        createTestBeehive();
        setNewBeehiveId(getIdOfNewlyCreatedBeehive());

        HttpGet request = new HttpGet(helper.getUri() + "/beequeens/" + newBeehiveId);
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertThat("GET response is empty",
                responseContent, containsString("[]"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(2)
    public void createNewBeeQueenToNewBeehive() throws IOException {

        getBeeQueensOfNewlyCreatedBeehive();

        String json = "{\"queenNo\":123,\"colour\":\"red\",\"fertile\":true,\"variety\":\"testowa_krolowa\",\"dateOfApplication\":\"2020-11-20\",\"beehiveId\":" + getNewBeehiveId() + "}";

        HttpPost request = new HttpPost(helper.getUri() + "/beequeens");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        request.setEntity(new StringEntity(json));

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(3)
    public void checkPostRequest() throws IOException {

        setNewBeehiveId(getIdOfNewlyCreatedBeehive());

        HttpGet request = new HttpGet(helper.getUri() + "/beequeens/" + getNewBeehiveId());
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertThat("GET response contains 'testowa_krolowa'",
                responseContent, containsString("testowa_krolowa"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    private String getIdOfNewlyBeeQueen() throws IOException {

        setNewBeehiveId(getIdOfNewlyCreatedBeehive());

        HttpGet request = new HttpGet(helper.getUri() + "/beequeens/" + getNewBeehiveId());
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        Pattern pattern = Pattern.compile(".*id.:(\\d{1,}),.queenNo.:.*");
        Matcher matcher = pattern.matcher(responseContent);

        String result = null;
        if (matcher.find())
            result = matcher.group(1);

        return result;
    }

    @Test
    @Order(4)
    public void testPutMessage() throws IOException {

        setNewBeeQueenId(getIdOfNewlyBeeQueen());
        setNewBeehiveId(getIdOfNewlyCreatedBeehive());

        String json = "{\"queenNo\":123,\"colour\":\"red\",\"fertile\":true,\"variety\":\"put_krolowa\",\"dateOfApplication\":\"2020-11-20\",\"beehiveId\":" + getNewBeehiveId() + "}";

        HttpPut request = new HttpPut(helper.getUri() + "/beequeen/" + getNewBeeQueenId());
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        request.setEntity(new StringEntity(json));

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(5)
    public void checkIfEditWasSuccessful() throws IOException {

        setNewBeehiveId(getIdOfNewlyCreatedBeehive());

        HttpGet request = new HttpGet(helper.getUri() + "/beequeens/" + getNewBeehiveId());
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertThat("GET response contains 'put_krolowa'",
                responseContent, containsString("put_krolowa"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(6)
    public void testDeleteMessage() throws IOException {

        setNewBeeQueenId(getIdOfNewlyBeeQueen());

        HttpDelete request = new HttpDelete(helper.getUri() + "/beequeen/" + getNewBeeQueenId());
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(7)
    public void checkIfDeleteWasSuccessful() throws IOException {

        setNewBeehiveId(getIdOfNewlyCreatedBeehive());

        HttpGet request = new HttpGet(helper.getUri() + "/beequeens/" + getNewBeehiveId());
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertThat("GET response is empty",
                responseContent, containsString("[]"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(8)
    public void cleanUp() throws IOException {

        setNewBeehiveId(getIdOfNewlyCreatedBeehive());

        HttpDelete request = new HttpDelete(helper.getUri() + "/beehive/" + getNewBeehiveId());
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

}

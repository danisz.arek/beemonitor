package UserTest;

import Helpers.Helpers;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class UserControllerTest {

    private Helpers helper = new Helpers();
    private String authToken = "Bearer " + helper.createToken("test2", "test");

    private String get_post_UsersUri = helper.getUri() + "/user";

    @Test
    @Order(1)
    public void getUsers() throws IOException {

        HttpGet request = new HttpGet(get_post_UsersUri);
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertThat("GET response contains 'ROLE_user'",
                responseContent, containsString("ROLE_user"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }


    @Test
    @Order(2)
    public void signUp() throws IOException {

        HttpPost request = new HttpPost(get_post_UsersUri);
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        request.setEntity(new StringEntity(helper.createSignUpBody().toString()));

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        System.out.println(responseContent);

        assertThat("Response contains new created username",
                responseContent, containsString("{\"username\":\"arkadiusz\"}"));

        assertEquals(200, response.getStatusLine().getStatusCode());

    }

    @Test
    @Order(3)
    public void signUpWithAlreadyTakenUsername () throws IOException {

        HttpPost request = new HttpPost(get_post_UsersUri);
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        request.setEntity(new StringEntity(helper.createSignUpBody().toString()));

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        System.out.println(responseContent);

        assertThat("Response contains information that this username is already taken",
                responseContent, containsString("Already used username"));

        assertEquals(409, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(4)
    public void getRoles () throws IOException {

        String userId = "/2/roles";

        HttpGet request = new HttpGet(get_post_UsersUri + userId);
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertThat("Response contains user role",
                responseContent, containsString("ROLE_user"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(5)
    public void getRole1ById () throws IOException {

        String userId = "/1/roles/1";

        HttpGet request = new HttpGet(get_post_UsersUri + userId);
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertThat("Response contains user role == user",
                responseContent, containsString("ROLE_user"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(6)
    public void getRole2ById () throws IOException {

        String userId = "/1/roles/2";

        HttpGet request = new HttpGet(get_post_UsersUri + userId);
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertThat("Response contains user role == admin",
                responseContent, containsString("ROLE_admin"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(7)
    public void getRolesFromANonAdminUser () throws IOException {

        String userId = "/2/roles";

        HttpGet request = new HttpGet(get_post_UsersUri + userId);
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        System.out.println(responseContent);

        assertThat("Response contains user role == user",
                responseContent, containsString("ROLE_user"));

        assertFalse("Response doesn't contains admin role", responseContent.contains("ROLE_admin"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(8)
    public void addAdminRoleToUserByAdmin () throws IOException {

        String adminAuthToken = "Bearer " + helper.createToken("test", "test");
        String userId = "/2/role/2";

        HttpPost request = new HttpPost(get_post_UsersUri + userId);
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, adminAuthToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(9)
    public void checkIfUserRoleWasAddedCorrectly () throws IOException {

        String userId = "/2/roles";

        HttpGet request = new HttpGet(get_post_UsersUri + userId);
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        System.out.println(responseContent);

        assertThat("Response contains new user role == admin",
                responseContent, containsString("ROLE_admin"));

        assertThat("Response still contains user role == user",
                responseContent, containsString("ROLE_user"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(10)
    public void addRoleToUserByAdmin_UserAlreadyHaveThisRole () throws IOException {

        String adminAuthToken = "Bearer " + helper.createToken("test", "test");
        String userId = "/2/role/2";

        HttpPost request = new HttpPost(get_post_UsersUri + userId);
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, adminAuthToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertThat("Response shows information that user already has this role",
                responseContent, containsString("User has already this role!"));

        assertEquals(404, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(11)
    public void deleteAdminRoleFromUser () throws IOException {

        String adminAuthToken = "Bearer " + helper.createToken("test", "test");
        String userId = "/2/roles/2";

        HttpDelete request = new HttpDelete(get_post_UsersUri + userId);
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, adminAuthToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(12)
    public void checkIfRoleWasDeletedProperly () throws IOException {

        String userId = "/2/roles";

        HttpGet request = new HttpGet(get_post_UsersUri + userId);
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        System.out.println(responseContent);

        assertThat("Response still contains user role == user",
                responseContent, containsString("ROLE_user"));

        assertFalse("Response doesn't contain user role == admin",
                responseContent.contains("ROLE_admin"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(13)
    public void addAdminRoleToUserByUser () throws IOException {
        // normal user cannot add roles to another users
        String userId = "/2/role/2";

        HttpPost request = new HttpPost(get_post_UsersUri + userId);
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(403, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(14)
    public void deleteRoleFromUserByUser () throws IOException {
        // normal user cannot add or delete roles to another users
        String userId = "/2/roles/1";

        HttpDelete request = new HttpDelete(get_post_UsersUri + userId);
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(403, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(15)
    public void getUserById () throws IOException {

        String userId = "/2";
        HttpGet request = new HttpGet(get_post_UsersUri + userId);
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        System.out.println(responseContent);
        assertThat("Response shows details from user with id 2",
                responseContent, containsString("test2"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(16)
    public void getAllUsers () throws IOException {

        HttpGet request = new HttpGet(get_post_UsersUri);
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        System.out.println(responseContent);
        assertThat("Response is presenting list of users",
                responseContent, containsString("arkadiusz"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }


    @Test
    @Order(17)
    public void deleteUserArkadiusz () throws IOException {

        // get all users and find out id from Arkadiusz

        HttpGet request = new HttpGet(get_post_UsersUri);
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");


        Pattern pattern = Pattern.compile(".*\"id\":(\\d),\"username\":\"arkadiusz\".*");
        Matcher matcher = pattern.matcher(responseContent);

        String idToDelete = null;
        if (matcher.find())
            idToDelete = "/" + matcher.group(1);

        // delete that guy!

        HttpDelete del = new HttpDelete(get_post_UsersUri + idToDelete);
        del.setHeader("Accept", "application/json");
        del.setHeader("Content-type", "application/json");
        del.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse responseAfterDel = HttpClientBuilder.create().build().execute(del);
        assertEquals(200, responseAfterDel.getStatusLine().getStatusCode());

    }

    @Test
    @Order(18)
    public void getAllUsersToCheckIfArkadiuszIsDeleted () throws IOException {

        HttpGet request = new HttpGet(get_post_UsersUri);
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertFalse("Response doesn't contain Arkadiusz", responseContent.contains("arkadiusz"));
        assertEquals(200, response.getStatusLine().getStatusCode());
    }


    @Test
    @Order(19)
    public void loginToAppWithProperCredentials () throws IOException {

        HttpPost request = new HttpPost(get_post_UsersUri + "/login");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setEntity(new StringEntity(helper.createLoginBody("test", "test").toString()));

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        System.out.println(responseContent);

        assertTrue("Response contains proper details", responseContent.contains("test")
                &&  responseContent.contains("ROLE_admin"));


        assertEquals(200, response.getStatusLine().getStatusCode());
    }


    @Test
    @Order(20)
    public void loginToAppWithNOTProperCredentials () throws IOException {

        HttpPost request = new HttpPost(get_post_UsersUri + "/login");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setEntity(new StringEntity(helper.createLoginBody("xo", "xo").toString()));

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(401, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(21)
    public void updateUser () throws IOException {

        String adminAuthToken = "Bearer " + helper.createToken("test", "test");
        String strJson = "{\"username\":\"put_test\",\"roles\":[{\"id\":2,\"name\":\"ROLE_admin\"},{\"id\":1,\"name\":\"ROLE_user\"}],\"beegardens\":[{\"id\":1,\"name\":\"pasieka1\"}]}";

        HttpPut request = new HttpPut(get_post_UsersUri + "/1");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, adminAuthToken);
        request.setEntity(new StringEntity(strJson));

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(22)
    public void getAllUsersToCheckIfPutWasSuccessful () throws IOException {

        String adminAuthToken = "Bearer " + helper.createToken("put_test", "test");
        HttpGet request = new HttpGet(get_post_UsersUri);
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, adminAuthToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertTrue("Response contain put_test as new username", responseContent.contains("put_test"));
        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(23)
    public void restoreUser () throws IOException {

        String adminAuthToken = "Bearer " + helper.createToken("put_test", "test");
        String strJson = "{\"username\":\"test\",\"roles\":[{\"id\":2,\"name\":\"ROLE_admin\"},{\"id\":1,\"name\":\"ROLE_user\"}],\"beegardens\":[{\"id\":1,\"name\":\"pasieka1\"}]}";

        HttpPut request = new HttpPut(get_post_UsersUri + "/1");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, adminAuthToken);
        request.setEntity(new StringEntity(strJson));

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

}

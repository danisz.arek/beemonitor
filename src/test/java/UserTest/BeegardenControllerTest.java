package UserTest;

import Helpers.Helpers;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class BeegardenControllerTest {

    private Helpers helper = new Helpers();
    private String authToken = "Bearer " + helper.createToken("test2", "test");
    private String newBeegardenId = null;

    private String getNewBeegardenId() {
        return newBeegardenId;
    }

    private void setNewBeegardenId(String newBeegardenId) {
        this.newBeegardenId = newBeegardenId;
    }

    @Test
    @Order(1)
    public void getAllBeegardens() throws IOException {

        HttpGet request = new HttpGet(helper.getUri() + "/beegardens");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertThat("GET response contains 'beegardenName'",
                responseContent, containsString("beegardenName"));

        assertThat("GET response contains 'type'",
                responseContent, containsString("type"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }



    @Test
    @Order(2)
    public void createNewBeegarden() throws IOException {


        String beehiveCreateJson = "{\"beegardenName\":\"testowa_pasieka\",\"location\":\"Gliwice\",\"type\":\"wedrowna\",\"userId\":1}";

        HttpPost request = new HttpPost(helper.getUri() + "/beegardens");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        request.setEntity(new StringEntity(beehiveCreateJson));

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(3)
    public void checkIfBeegardenWasCreatedProperly() throws IOException {

        HttpGet request = new HttpGet(helper.getUri() + "/beegardens");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertThat("GET response contains 'beegardenName'",
                responseContent, containsString("beegardenName"));

        assertThat("GET response contains 'testowa_pasieka'",
                responseContent, containsString("testowa_pasieka"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    private String getIdOfNewlyCreatedBeegarden() throws IOException {

        HttpGet request = new HttpGet(helper.getUri() + "/beegardens");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        Pattern pattern = Pattern.compile(".*id.:(\\d{1,}),.beegardenName.:.testowa_pasieka.*");
        Matcher matcher = pattern.matcher(responseContent);

        String result = null;
        if (matcher.find())
            result = matcher.group(1);

        return result;
    }

    @Test
    @Order(4)
    public void testPutMessage() throws IOException {

        setNewBeegardenId(getIdOfNewlyCreatedBeegarden());

        String json = "{\"beegardenName\":\"testowa_pasieka\",\"location\":\"put_lokalizacja\",\"type\":\"put_type\",\"userId\":1}";

        HttpPut request = new HttpPut(helper.getUri() + "/beegarden/" + getNewBeegardenId());
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        request.setEntity(new StringEntity(json));

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(5)
    public void checkIfBeegardenWasEditedProperly() throws IOException {

        HttpGet request = new HttpGet(helper.getUri() + "/beegardens");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertThat("GET response contains 'put_lokalizacja'",
                responseContent, containsString("put_lokalizacja"));

        assertThat("GET response contains 'put_type'",
                responseContent, containsString("put_type"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(6)
    public void testDeleteMessage() throws IOException {

        setNewBeegardenId(getIdOfNewlyCreatedBeegarden());

        HttpDelete request = new HttpDelete(helper.getUri() + "/beegarden/" + getNewBeegardenId());
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(7)
    public void checkIfCleanUpWasSuccessful() throws IOException {

        HttpGet request = new HttpGet(helper.getUri() + "/beegardens");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertTrue("Response doesn't contain testowa_pasieka", !responseContent.contains("testowa_pasieka"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }


}

package UserTest;

import Helpers.Helpers;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import javax.persistence.GeneratedValue;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class BeeSensorDataControllerTest {

    private Helpers helper = new Helpers();
    private String authToken = "Bearer " + helper.createToken("test2", "test");
    private String newBeehiveId = null;

    private String getNewBeehiveId() {
        return newBeehiveId;
    }

    private void setNewBeehiveId(String newBeehiveId) {
        this.newBeehiveId = newBeehiveId;
    }

    @Test
    @Order(1)
    public void getAllBeeSensorDatas() throws IOException {

        HttpGet request = new HttpGet(helper.getUri() + "/beesensordatas");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertThat("GET response contains 'temperature'",
                responseContent, containsString("temperature"));

        assertThat("GET response contains 'logitude'",
                responseContent, containsString("logitude"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    private void createTestBeehive() throws IOException {

        String beehiveCreateJson = "{\"beegardenId\":1,\"beehiveName\":\"testowy_ul\",\"type\":\"wielkopolski\",\"dateOfCreation\":\"2020.11.21 11:50\",\"framesAmount\":10,\"trunksAmount\":1,\"bottom\":\"higieniczna\",\"feeder\":true,\"poolenTrapper\":false,\"propolisSinker\":true,\"queenCage\":false,\"barrier\":true,\"isolator\":true,\"heater\":false,\"inletWidth\":10,\"workFramesAmount\":1,\"force\":\"silna\",\"temperament\":\"waleczny\",\"queenCell\":false,\"swarmMood\":false,\"lackOfFood\":false,\"grubs\":3,\"propolis\":30,\"honey\":30}";

        HttpPost request = new HttpPost(helper.getUri() + "/beehives");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        request.setEntity(new StringEntity(beehiveCreateJson));

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(200, response.getStatusLine().getStatusCode());
    }


    private String getIdOfNewlyCreatedBeehive() throws IOException {

        HttpGet request = new HttpGet(helper.getUri() + "/beehives");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        Pattern pattern = Pattern.compile(".*id.:(\\d{1,}),.beegardenId\":1,.beehiveName\":.testowy_ul.,.type.:.wielkopolski.*");
        Matcher matcher = pattern.matcher(responseContent);

        String result = null;
        if (matcher.find())
            result = matcher.group(1);

        return result;
    }

    private void getBeeSensorsOfNewlyCreatedBeehive() throws IOException {
        createTestBeehive();
        setNewBeehiveId(getIdOfNewlyCreatedBeehive());

        HttpGet request = new HttpGet(helper.getUri() + "/beesensors/" + newBeehiveId);
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertThat("GET response is empty",
                responseContent, containsString("[]"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(2)
    public void createNewBeeSensorToNewBeehive() throws IOException {

        setNewBeehiveId(getIdOfNewlyCreatedBeehive());
        getBeeSensorsOfNewlyCreatedBeehive();

        String json = "{\"phoneNo\":\"+48999666333\",\"beehive\":{\"id\":" + getNewBeehiveId() + "}}";

        HttpPost request = new HttpPost(helper.getUri() + "/beesensors");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        request.setEntity(new StringEntity(json));

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(3)
    public void checkPostRequest() throws IOException {

        setNewBeehiveId(getIdOfNewlyCreatedBeehive());


        HttpGet request = new HttpGet(helper.getUri() + "/beesensors/" + getNewBeehiveId());
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertThat("GET response contains '48999666333'",
                responseContent, containsString("48999666333"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(4)
    public void addDataToNewBeeSensor() throws IOException {

        String json = "{\"readDate\":\"2021-01-18 01:11\",\"temperatureIn\":\"28\",\"humidityIn\":\"80\",\"temperatureOut\":\"23\",\"humidityOut\":\"87\",\"latitude\":\"22.33\",\"logitude\":\"50.22\",\"phoneNo\":\"68442916\"}";

        HttpPost request = new HttpPost(helper.getUri() + "/beesensordatas");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        request.setEntity(new StringEntity(json));

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(200, response.getStatusLine().getStatusCode());

    }

    private String getIdOfNewlyBeeSensorData() throws IOException {

        HttpGet request = new HttpGet(helper.getUri() + "/beesensordatas");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");
        System.out.println(responseContent + " RESPONSE");
        Pattern pattern = Pattern.compile(".*id.:(\\d{1,}),.readDate.:.2021-01-18.01:11.,.temperatureIn.:.28.,.humidityIn.:.80.*");
        Matcher matcher = pattern.matcher(responseContent);

        String result = null;
        if (matcher.find())
            result = matcher.group(1);

        return result;
    }


    @Test
    @Order(5)
    public void checkNewBeeSensorData() throws IOException {

        HttpGet request = new HttpGet(helper.getUri() + "/beesensordatas");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertThat("GET response contains '31.1'",
                responseContent, containsString("31.1"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(6)
    public void testPutMessage() throws IOException {

        String json = "{\"readDate\":\"2021-01-19 02:11\",\"temperatureIn\":\"99999\",\"humidityIn\":\"80\",\"temperatureOut\":\"23\",\"humidityOut\":\"87\",\"latitude\":\"22.33\",\"logitude\":\"50.22\",\"phoneNo\":\"68442916\"}";

        HttpPut request = new HttpPut(helper.getUri() + "/beesensordata/" + getIdOfNewlyBeeSensorData());
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        request.setEntity(new StringEntity(json));

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(7)
    public void checkIfEditWasSuccessful() throws IOException {


        HttpGet request = new HttpGet(helper.getUri() + "/beesensordatas");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertThat("GET response contains '99999'",
                responseContent, containsString("99999"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    private String getIdOfEditedBeeSensorData() throws IOException {

        HttpGet request = new HttpGet(helper.getUri() + "/beesensordatas");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");
        System.out.println(responseContent + " RESPONSE");
        Pattern pattern = Pattern.compile(".*id.:(\\d{1,}),.readDate.:.2021-01-19.02:11.,.temperatureIn.:.99999.*");
        Matcher matcher = pattern.matcher(responseContent);

        String result = null;
        if (matcher.find())
            result = matcher.group(1);

        return result;
    }

    @Test
    @Order(8)
    public void testDeleteMessage() throws IOException {

        HttpDelete request = new HttpDelete(helper.getUri() + "/beesensordata/" + getIdOfEditedBeeSensorData());
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(9)
    public void checkIfDeleteWasSuccessful() throws IOException {

        HttpGet request = new HttpGet(helper.getUri() + "/beesensordatas");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");
        System.out.println("RESPONSE " + responseContent);

        assertTrue("Response doesn't contains data from further tests",
                !responseContent.contains("99999")
                        && !responseContent.contains("88888"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(10)
    public void cleanUp() throws IOException {

        HttpDelete request = new HttpDelete(helper.getUri() + "/beehive/" + getIdOfNewlyCreatedBeehive());
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

}

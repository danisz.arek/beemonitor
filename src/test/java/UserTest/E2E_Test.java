package UserTest;

import Helpers.Helpers;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class E2E_Test {

    private Helpers helper = new Helpers();
    private String authToken = "Bearer " + helper.createToken("test2", "test");

    @Test
    @Order(1)
    public void signUp() throws IOException {

        HttpPost request = new HttpPost(helper.getUri() + "/user");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        request.setEntity(new StringEntity(helper.createSignUpBody().toString()));

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        assertThat("Response contains new created username",
                responseContent, containsString("{\"username\":\"arkadiusz\"}"));

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    private String getIdOfNewUser() throws IOException {

        HttpGet request = new HttpGet(helper.getUri() + "/user");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        Pattern pattern = Pattern.compile(".*id.:(\\d{1,}),.username.:.arkadiusz.*");
        Matcher matcher = pattern.matcher(responseContent);

        String result = null;
        if (matcher.find())
            result = matcher.group(1);

        return result;
    }

    @Test
    @Order(2)
    public void saveNewUserDetails() throws IOException {

        String strJson = "{\"beekeperName\":\"test_name\",\"beekeperSurname\":\"test_surname\",\"email\":\"test.mail@gmail.com\",\"postCode\":\"99-999\",\"city\":\"test_city\",\"road\":\"test_road\",\"roadNr\":\"99\",\"user\":{\"id\":" + getIdOfNewUser() + ",\"username\":\"arkadiusz\"}}";

        HttpPost request = new HttpPost(helper.getUri() + "/userdetails");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        request.setEntity(new StringEntity(strJson));

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(3)
    public void createNewBeegarden1() throws IOException {

        String beehiveCreateJson = "{\"beegardenName\":\"testowa_pasieka1\",\"location\":\"Gliwice\",\"type\":\"wedrowna\",\"userId\":" + getIdOfNewUser() + "}";

        HttpPost request = new HttpPost(helper.getUri() + "/beegardens");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        request.setEntity(new StringEntity(beehiveCreateJson));

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    @Test
    @Order(4)
    public void createNewBeegarden2() throws IOException {

        String beehiveCreateJson = "{\"beegardenName\":\"testowa_pasieka2\",\"location\":\"Gliwice\",\"type\":\"wedrowna\",\"userId\":" + getIdOfNewUser() + "}";

        HttpPost request = new HttpPost(helper.getUri() + "/beegardens");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        request.setEntity(new StringEntity(beehiveCreateJson));

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(200, response.getStatusLine().getStatusCode());
    }

    private String getIdOfBeegarden(String beegardenName) throws IOException {

        HttpGet request = new HttpGet(helper.getUri() + "/beegardens");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        Pattern pattern = Pattern.compile(".*id.:(\\d{1,}),.beegardenName.:." + beegardenName + ".*");
        Matcher matcher = pattern.matcher(responseContent);

        String result = null;
        if (matcher.find())
            result = matcher.group(1);

        return result;
    }


    @Test
    @Order(5)
    public void createLotOfBeehives() throws IOException {

        for (int i = 1; i < 4; i++) {

            String beehiveCreateJson = "{\"beegardenId\":" + getIdOfBeegarden("testowa_pasieka1") + ",\"beehiveName\":\"testowy_ul1_" + i + "\",\"type\":\"wielkopolski\",\"dateOfCreation\":\"2020.11.21 11:50\",\"framesAmount\":10,\"trunksAmount\":1,\"bottom\":\"higieniczna\",\"feeder\":true,\"poolenTrapper\":false,\"propolisSinker\":true,\"queenCage\":false,\"barrier\":true,\"isolator\":true,\"heater\":false,\"inletWidth\":10,\"workFramesAmount\":1,\"force\":\"silna\",\"temperament\":\"waleczny\",\"queenCell\":false,\"swarmMood\":false,\"lackOfFood\":false,\"grubs\":3,\"propolis\":30,\"honey\":30}";

            HttpPost request = new HttpPost(helper.getUri() + "/beehives");
            request.setHeader("Accept", "application/json");
            request.setHeader("Content-type", "application/json");
            request.setHeader(HttpHeaders.AUTHORIZATION, authToken);
            request.setEntity(new StringEntity(beehiveCreateJson));

            HttpResponse response = HttpClientBuilder.create().build().execute(request);

            assertEquals(200, response.getStatusLine().getStatusCode());
        }

        for (int i = 1; i < 4; i++) {

            String beehiveCreateJson = "{\"beegardenId\":" + getIdOfBeegarden("testowa_pasieka2") + ",\"beehiveName\":\"testowy_ul2_" + i + "\",\"type\":\"wielkopolski\",\"dateOfCreation\":\"2020.11.21 11:50\",\"framesAmount\":10,\"trunksAmount\":1,\"bottom\":\"higieniczna\",\"feeder\":true,\"poolenTrapper\":false,\"propolisSinker\":true,\"queenCage\":false,\"barrier\":true,\"isolator\":true,\"heater\":false,\"inletWidth\":10,\"workFramesAmount\":1,\"force\":\"silna\",\"temperament\":\"waleczny\",\"queenCell\":false,\"swarmMood\":false,\"lackOfFood\":false,\"grubs\":3,\"propolis\":30,\"honey\":30}";

            HttpPost request = new HttpPost(helper.getUri() + "/beehives");
            request.setHeader("Accept", "application/json");
            request.setHeader("Content-type", "application/json");
            request.setHeader(HttpHeaders.AUTHORIZATION, authToken);
            request.setEntity(new StringEntity(beehiveCreateJson));

            HttpResponse response = HttpClientBuilder.create().build().execute(request);

            assertEquals(200, response.getStatusLine().getStatusCode());
        }
    }

    private String getIdOfNewlyCreatedBeehive() throws IOException {

        HttpGet request = new HttpGet(helper.getUri() + "/beehives");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        String responseContent = EntityUtils.toString(response.getEntity(), "UTF-8");

        Pattern pattern = Pattern.compile(".*id.:(\\d{1,}),.beegardenId\":" + getIdOfBeegarden("testowa_pasieka2") + ",.beehiveName\":.testowy_ul2_1.,.type.:.wielkopolski.*");
        Matcher matcher = pattern.matcher(responseContent);

        String result = null;
        if (matcher.find())
            result = matcher.group(1);

        return result;
    }

    @Test
    @Order(6)
    public void addALotOfBeeQueens() throws IOException {

        for (int i = 1; i < 4; i++) {

            String json = "{\"queenNo\":123,\"colour\":\"red\",\"fertile\":true,\"variety\":\"testowa_krolowa" + i + "\",\"dateOfApplication\":\"2020-11-20\",\"beehiveId\":" + getIdOfNewlyCreatedBeehive() + "}";

            HttpPost request = new HttpPost(helper.getUri() + "/beequeens");
            request.setHeader("Accept", "application/json");
            request.setHeader("Content-type", "application/json");
            request.setHeader(HttpHeaders.AUTHORIZATION, authToken);
            request.setEntity(new StringEntity(json));

            HttpResponse response = HttpClientBuilder.create().build().execute(request);

            assertEquals(200, response.getStatusLine().getStatusCode());
        }
    }

    @Test
    @Order(7)
    public void addABeeSensor() throws IOException {

        String json = "{\"phoneNo\":\"+4812345678\",\"beehive\":{\"id\":" + getIdOfNewlyCreatedBeehive() + "}}";

        HttpPost request = new HttpPost(helper.getUri() + "/beesensors");
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");
        request.setHeader(HttpHeaders.AUTHORIZATION, authToken);
        request.setEntity(new StringEntity(json));

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        assertEquals(200, response.getStatusLine().getStatusCode());

    }

    @Test
    @Order(8)
    public void addALotOfHoneyCollections() throws IOException {

        for (int i = 1; i < 5; i++) {

            String json = "{\"collectionDate\":\"2020-11-30 11:35\",\"honeyType\":\"testowy_przeglad" + i + "\",\"amount\":null,\"beehiveId\":" + getIdOfNewlyCreatedBeehive() + "}";

            HttpPost request = new HttpPost(helper.getUri() + "/honeycollections");
            request.setHeader("Accept", "application/json");
            request.setHeader("Content-type", "application/json");
            request.setHeader(HttpHeaders.AUTHORIZATION, authToken);
            request.setEntity(new StringEntity(json));

            HttpResponse response = HttpClientBuilder.create().build().execute(request);

            assertEquals(200, response.getStatusLine().getStatusCode());
        }
    }

    @Test
    @Order(9)
    public void createNewInspectionToNewBeehive() throws IOException {

        for (int i = 1; i < 5; i++) {

            String json = "{\"inspectionDate\":\"2020-11-01 11:30\",\"feeding\":false,\"queen\":true,\"treatment\":false,\"notes\":\"testowy_inspection" + i + "\",\"beehiveId\":" + getIdOfNewlyCreatedBeehive() + "}";

            HttpPost request = new HttpPost(helper.getUri() + "/inspections");
            request.setHeader("Accept", "application/json");
            request.setHeader("Content-type", "application/json");
            request.setHeader(HttpHeaders.AUTHORIZATION, authToken);
            request.setEntity(new StringEntity(json));

            HttpResponse response = HttpClientBuilder.create().build().execute(request);

            assertEquals(200, response.getStatusLine().getStatusCode());
        }
    }

    @Test
    @Order(10)
    public void addLotOfDataToNewBeeSensor() throws IOException {

        for (int i = 0; i < 10; i++) {

            String json = "{\"readDate\":\"2021-01-18 01:0" + i + "\",\"temperatureIn\":\"28." + i  + "\",\"humidityIn\":\"80\",\"temperatureOut\":\"23\",\"humidityOut\":\"87\",\"latitude\":\"22.33\",\"logitude\":\"50.22\",\"phoneNo\":\"68442916\"}";
            HttpPost request = new HttpPost(helper.getUri() + "/beesensordatas");
            request.setHeader("Accept", "application/json");
            request.setHeader("Content-type", "application/json");
            request.setHeader(HttpHeaders.AUTHORIZATION, authToken);
            request.setEntity(new StringEntity(json));

            HttpResponse response = HttpClientBuilder.create().build().execute(request);

            assertEquals(200, response.getStatusLine().getStatusCode());
        }
    }

    @Test
    @Order(11)
    public void cleanUp() throws IOException {

            HttpDelete request = new HttpDelete(helper.getUri() + "/user/" + getIdOfNewUser());
            request.setHeader("Accept", "application/json");
            request.setHeader("Content-type", "application/json");
            request.setHeader(HttpHeaders.AUTHORIZATION, authToken);

            HttpResponse response = HttpClientBuilder.create().build().execute(request);

            assertEquals(200, response.getStatusLine().getStatusCode());
    }


}
